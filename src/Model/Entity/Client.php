<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $title
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $gender
 * @property \Cake\I18n\FrozenDate $DOB
 * @property string $extraInfo
 * @property string $comment
 * @property string $occupation
 * @property string $emergency
 * @property int $medicareNum
 * @property int $referenceNum
 * @property string $doctor
 * @property string $note
 * @property string $reminder
 * @property string $smsMarketing
 * @property string $confirmation
 * @property string $concessionType
 * @property string $additionalEmail
 * @property string $invoiceInfo
 * @property string $city
 * @property string $state
 * @property string $country
 * @property int $postalCode
 * @property string $street
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
