<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Archive Entity
 *
 * @property int $id
 * @property string $fromName
 * @property string $fromEmail
 * @property string $phoneNumber
 * @property string $toName
 * @property string $message
 * @property \Cake\I18n\FrozenTime $datePurchase
 * @property \Cake\I18n\FrozenDate $expiry
 * @property int $paymentStatus
 * @property int $status_id
 * @property int $prices_id
 * @property string $UUID
 *
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Price $price
 */
class Archive extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
