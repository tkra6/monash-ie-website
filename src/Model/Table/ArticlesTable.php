<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'categories_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
             ->add ("title", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
            ->add("title", [
                      "custom" => [
                      "rule" => [$this, "checkAlpha"], //add the new rule 'checkAlpha' to name field
                      "message" => "Please do not enter number/special character except -. Always start with letter."
                        ]])
            ->notEmpty('title');

        $validator
            ->requirePresence('body', 'create')
             ->add ("body", ["notEmpty" => [
                      "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                      "message" => "Please Enter some Text, don't leave it blank."
                        ]])
            ->notEmpty('body');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['categories_id'], 'Categories'));
        $rules->add($rules->existsIn(['users_id'], 'Users'));

        return $rules;
    }
  
  public function checkAlpha($title, array $context) {
                     // special characters
        if (!preg_match("/^[a-z \-]+$/i", $title)) {
            return false;
        }
                //Check first letter isit alphabet
                if (!preg_match('/^[a-zA-Z]/', $title)) {
                  return false;
                }
                return true;
           }

}
