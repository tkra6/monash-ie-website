<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Prices Model
 *
 * @property \App\Model\Table\PricetypesTable|\Cake\ORM\Association\BelongsTo $Pricetypes
 *
 * @method \App\Model\Entity\Price get($primaryKey, $options = [])
 * @method \App\Model\Entity\Price newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Price[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Price|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Price patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Price[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Price findOrCreate($search, callable $callback = null, $options = [])
 */
class PricesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Pricetypes', [
            'foreignKey' => 'pricetypes_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('minutes', 'create')
          ->add ("minutes", ["notEmpty" => [
                        "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                        "message" => "Please Enter some text, don't leave it blank."
                          ]])

              ->add("minutes", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter special character."
                      ]])
            ->notEmpty('minutes');

        $validator
            ->decimal('value')
            ->requirePresence('value', 'create')
            ->add ("value", ["notEmpty" => [
                        "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                        "message" => "Please Enter the price, don't leave it blank."
                          ]])
          ->add('value', 'money', array('rule' =>
                                    array('money', 'left'),
                                    'message' => 'Please supply a valid monetary amount.'))
          ->notEmpty('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pricetypes_id'], 'Pricetypes'));

        return $rules;
    }
  
  public function checkAlpha($minutes, array $context)
    {
     if (!preg_match('/^[a-z0-9 ]*$/i', $minutes) ) {
            return false;
        }
              
        return true;
    }
  
}
