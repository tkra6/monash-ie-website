<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->requirePresence('username', 'create')
            ->add ("username", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                  "message" => "Please Enter Some Text, don't leave it blank."
                    ]])
            ->add("username", [
                    "custom" => [
                        "rule" => [$this, "checkSpecial"], //add the new rule 'checkSpecial' to username field
                        "message" => "Please enter AlphaNumeric only."
                    ]])
            ->notEmpty('username');
 

        $validator
            ->requirePresence('password', 'create')
            ->add("password", [
                    "custom" => [
                        "rule" => [$this, "checkCharacters"], 
                        "message" => "Minimum 8 Character with at least one Upper, Lower, Number and Special Character"
                    ]])
            ->add('password', ['length' => ['rule' => ['minLength', 8], 'message' => 'Passwords must be at least 8 characters long.']])
            ->add ("password", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'notBlank' to password field
                  "message" => "Please Enter Some Text, don't leave it blank."
                    ]])
            ->notEmpty('password');

        $validator
            ->requirePresence('first_name', 'create')
            ->add("first_name", [
                    "custom" => [
                        "rule" => [$this, "checkAlpha"], //add the new rule 'checkAlpha' to first_name field
                        "message" => "Please enter Alphabet only."
                    ]])
            ->add ("first_name", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'notBlank' to first_name field
                  "message" => "Please Enter Some Text, don't leave it blank."
                    ]])
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->add("last_name", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha1"], //add the new rule 'checkAlpha' to last_name field
                          "message" => "Please enter Alphabet only."
                      ]])
          ->add ("last_name", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'notBlank' to last_name field
                  "message" => "Please Enter Some Text, don't leave it blank."
                    ]])
            ->notEmpty('last_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
  
   public function checkCharacters($password, array $context)
    {
        // number
        if (!preg_match("#[0-9]#", $password)) {
            return false;  
        }
        // Uppercase
        if (!preg_match("#[A-Z]#", $password)) {
            return false;
        }
        // lowercase
        if (!preg_match("#[a-z]#", $password)) {
            return false;
        }
        // special characters
        if (!preg_match("#\W+#", $password) ) {
            return false;
        }
        // space
        if (preg_match("# #", $password)) {
            return false;
        }
      
        return true;
    }
  
     public function checkSpecial($username, array $context)
    {

        // No space
        if (preg_match("# #", $username)) {
            return false;
        }
        // No special characters
        if (preg_match("#\W+#", $username) ) {
            return false;
        }
        return true;
    }
  
       public function checkAlpha($firstName, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $firstName)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $firstName)) {
            return false;
        }
        
        return true;
    }
  
         public function checkAlpha1($lastName, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $lastName)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $lastName)) {
            return false;
        }
        
        return true;
    }
  
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}