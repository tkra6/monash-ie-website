<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Archives Model
 *
 * @property \App\Model\Table\StatusesTable|\Cake\ORM\Association\BelongsTo $Statuses
 * @property \App\Model\Table\PricesTable|\Cake\ORM\Association\BelongsTo $Prices
 *
 * @method \App\Model\Entity\Archive get($primaryKey, $options = [])
 * @method \App\Model\Entity\Archive newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Archive[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Archive|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Archive patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Archive[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Archive findOrCreate($search, callable $callback = null, $options = [])
 */
class ArchivesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('archives');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Prices', [
            'foreignKey' => 'prices_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('fromName', 'create')
          ->add ("fromName", ["notEmpty" => [
                      "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                      "message" => "Please Enter some text, don't leave it blank."
                        ]])
          
            ->add("fromName", [
                    "custom" => [
                        "rule" => [$this, "checkAlpha"], //add the new rule 'checkSpecial' to username field
                        "message" => "Please do not enter number/special character. Always start with letter"
                    ]])
            ->notEmpty('fromName');

        $validator
            ->requirePresence('fromEmail', 'create')
            ->notEmpty('fromEmail')
            ->email('fromEmail');

        $validator
            ->requirePresence('phoneNumber', 'create')
              ->add ("phoneNumber", ["notEmpty" => [
                      "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                      "message" => "Please Enter phone number, don't leave it blank."
                        ]])
          
            ->add("phoneNumber", [
                    "custom" => [
                        "rule" => [$this, "validate_phone"], //add the new rule 'checkSpecial' to username field
                        "message" => "Please enter number only (Australia format)"
                    ]])
            ->notEmpty('phoneNumber');

        $validator
            ->requirePresence('toName', 'create')
          ->add ("toName", ["notEmpty" => [
                      "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                      "message" => "Please Enter some text, don't leave it blank."
                        ]])
          
            ->add("toName", [
                    "custom" => [
                        "rule" => [$this, "checkAlpha"], //add the new rule 'checkSpecial' to username field
                        "message" => "Please do not enter number/special character. Always start with letter"
                    ]])
            ->notEmpty('toName');

        $validator
            ->requirePresence('message', 'create')
          ->add ("message", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
            ->add("message", [
                      "custom" => [
                      "rule" => [$this, "checkAlpha2"], //add the new rule 'checkAlpha' to name field
                      "message" => "Please do not enter number/special character except (-.,!). Always start with letter."
                        ]])
            ->notEmpty('message');

        $validator
            ->dateTime('datePurchase')
            ->allowEmpty('datePurchase');

        $validator
            ->allowEmpty('expiry', 'create');
              
        $validator
            ->allowEmpty('paymentStatus');

        $validator
            ->requirePresence('UUID', 'create')
            ->notEmpty('UUID');

        return $validator;
    }
  
   function validate_phone($phoneNumber) {
  $number = preg_replace('/[^\d]/', '', $phoneNumber);
  return preg_match('/^(0(2|3|4|7|8))?\d{8}$/', $phoneNumber)
    || preg_match('/^1(3|8)00\d{6}$/', $phoneNumber)
    || preg_match('/^13\d{4}$/', $phoneNumber);
}
  
   public function checkAlpha($fromName, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $fromName)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $fromName)) {
            return false;
        }
        
        return true;
    }
  
  public function checkAlpha1($toName, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $toName)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $toName)) {
            return false;
        }
        
        return true;
    }
  
  public function checkAlpha2($message, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $message)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z .,!\-]+$/i", $message)) {
            return false;
        }
        
        return true;
    }
  
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        $rules->add($rules->existsIn(['prices_id'], 'Prices'));
        return $rules;
    }
}
