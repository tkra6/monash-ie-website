<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pagecontents Model
 *
 * @method \App\Model\Entity\Pagecontent get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pagecontent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pagecontent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pagecontent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pagecontent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pagecontent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pagecontent findOrCreate($search, callable $callback = null, $options = [])
 */
class PagecontentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pagecontents');
        $this->setDisplayField('page');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('page', 'create')
            ->add ("page", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
          ->add("page", [
                    "custom" => [
                    "rule" => [$this, "checkAlpha"], //add the new rule 'checkAlpha' to name field
                    "message" => "Please do not enter number/special character except -. Always start with letter."
                      ]])
            ->notEmpty('page');

        $validator
            ->requirePresence('section', 'create')
            ->add ("section", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
           ->add("section", [
                    "custom" => [
                    "rule" => [$this, "checkAlpha1"], //add the new rule 'checkAlpha' to name field
                    "message" => "Please do not enter number/special character except -. Always start with letter."
                      ]])
            ->notEmpty('section');

        $validator
            ->requirePresence('content', 'create')
            ->add ("content", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
            ->notEmpty('content');
      
      $validator
            ->allowEmpty('block');
      
       $validator
            ->allowEmpty('heading');
      
      $validator
            ->allowEmpty('metatype');

        return $validator;
    }

           public function checkAlpha($page, array $context) {
                    if (!preg_match('/^[a-z \-]+$/i', $page)) {
                      return false;
                    }
                //Check first letter isit alphabet
                if (!preg_match('/^[a-zA-Z]/', $page)) {
                  return false;
                }
                return true;
           }

          public function checkAlpha1($section, array $context) {
                    if (!preg_match('/^[a-z \-]+$/i', $section)) {
                      return false;
                    }
                //Check first letter isit alphabet
                if (!preg_match('/^[a-zA-Z]/', $section)) {
                  return false;
                }
                return true;
           }
  
          public function checkHere($content, array $context) {
                        //Check first letter isit alphabet
                        if (!preg_match('/^[a-zA-Z]/', $content)) {
                          return false;
                        }
                        return true;
                   }
}
