<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clients Model
 *
 * @method \App\Model\Entity\Client get($primaryKey, $options = [])
 * @method \App\Model\Entity\Client newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Client[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Client|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Client[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Client findOrCreate($search, callable $callback = null, $options = [])
 */
class ClientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('firstName', 'create')
            ->add ("firstName", ["notEmpty" => [
                        "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                        "message" => "Please Enter some text, don't leave it blank."
                          ]])

              ->add("firstName", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->notEmpty('firstName');

        $validator
            ->requirePresence('lastName', 'create')
          ->add ("lastName", ["notEmpty" => [
                        "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                        "message" => "Please Enter some text, don't leave it blank."
                          ]])

              ->add("lastName", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha1"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->notEmpty('lastName');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('gender', 'create')
            ->notEmpty('gender');

        $validator
            ->date('DOB')
            ->requirePresence('DOB', 'create')
            ->notEmpty('DOB');

        $validator
           ->add("extraInfo", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha2"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character except (.,!-). Always start with letter"
                      ]])
            ->allowEmpty('extraInfo');

        $validator
          ->add("comment", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha3"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character except (.,!-). Always start with letter"
                      ]])
            ->allowEmpty('comment');

        $validator
            ->requirePresence('occupation', 'create')
           ->add ("occupation", ["notEmpty" => [
                        "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                        "message" => "Please Enter some text, don't leave it blank."
                          ]])

              ->add("occupation", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha4"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->notEmpty('occupation');

        $validator
            ->requirePresence('emergency', 'create')
            ->add ("emergency", ["notEmpty" => [
                          "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                          "message" => "Please Enter some text, don't leave it blank."
                            ]])
            ->notEmpty('emergency');

        $validator
            ->integer('medicareNum')
            ->add('medicareNum', ['length' => ['rule' => ['minLength', 10], 'message' => 'The Minimum number of digit is 10, please try again.']])
            ->add('medicareNum', ['length' => ['rule' => ['maxLength', 10], 'message' => 'The Maximum number of digit is 10, please try again.']])

          ->add("medicareNum", [
                      "custom" => [
                          "rule" => [$this, "checkMedicare"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please enter your 1o digit medicare number."
                      ]])
            ->allowEmpty('medicareNum');

        $validator
            ->integer('referenceNum')
          ->add('referenceNum', ['length' => ['rule' => ['minLength', 1], 'message' => 'The Minimum number of digit is 1, please try again.']])
          ->add('referenceNum', ['length' => ['rule' => ['maxLength', 1], 'message' => 'The Maximum number of digit is 1, please try again.']])
          ->add("referenceNum", [
                      "custom" => [
                          "rule" => [$this, "checkRef"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please enter your one digit reference number"
                      ]])
            ->allowEmpty('referenceNum');

        $validator
          ->add("doctor", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha5"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->allowEmpty('doctor');

        $validator
          ->add("note", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha6"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character except (.,!-). Always start with letter"
                      ]])
            ->allowEmpty('note');

        $validator
            ->requirePresence('reminder', 'create')
            ->notEmpty('reminder');

        $validator
            ->allowEmpty('smsMarketing');

        $validator
            ->allowEmpty('confirmatione');

        $validator
            ->requirePresence('concessionType', 'create')
            ->notEmpty('concessionType');

        $validator
            ->email('additionalEmail')
            ->allowEmpty('additionalEmail');

        $validator
          ->add("invoiceInfo", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha7"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character except (.,!-). Always start with letter"
                      ]])
            ->allowEmpty('invoiceInfo');

        $validator
            ->requirePresence('city', 'create')
          ->add ("city", ["notEmpty" => [
                          "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                          "message" => "Please Enter some text, don't leave it blank."
                            ]])
          ->add("city", [
                      "custom" => [
                          "rule" => [$this, "checkCity"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->notEmpty('city');

        $validator
            ->requirePresence('state', 'create')
          ->add ("state", ["notEmpty" => [
                          "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                          "message" => "Please Enter some text, don't leave it blank."
                            ]])
          ->add("state", [
                      "custom" => [
                          "rule" => [$this, "checkState"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->notEmpty('state');

        $validator
            ->requirePresence('country', 'create')
          ->add ("country", ["notEmpty" => [
                          "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                          "message" => "Please Enter some text, don't leave it blank."
                            ]])
          ->add("country", [
                      "custom" => [
                          "rule" => [$this, "checkCountry"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character. Always start with letter"
                      ]])
            ->notEmpty('country');

        $validator
            ->integer('postalCode')
            ->add('postalCode', ['length' => ['rule' => ['minLength', 4], 'message' => 'The Minimum number of digit is 4, please try again.']])
            ->add('postalCode', ['length' => ['rule' => ['maxLength', 4], 'message' => 'The Maximum number of digit is 4, please try again.']])

            ->add ("postalCode", ["notEmpty" => [
                          "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                          "message" => "Please Enter some text, don't leave it blank."
                            ]])
          ->add("postalCode", [
                      "custom" => [
                          "rule" => [$this, "checkPC"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter special character"
                      ]])
            ->requirePresence('postalCode', 'create')
            ->notEmpty('postalCode');

        $validator
            ->requirePresence('street', 'create')
          ->add ("street", ["notEmpty" => [
                          "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                          "message" => "Please Enter some text, don't leave it blank."
                            ]])
          ->add("street", [
                      "custom" => [
                          "rule" => [$this, "checkStreet"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character."
                      ]])
            ->notEmpty('street');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
  
     public function checkAlpha($firstName, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $firstName)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $firstName)) {
            return false;
        }
        
        return true;
    }
  
       public function checkAlpha1($lastName, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $lastName)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $lastName)) {
            return false;
        }
        
        return true;
    }
  
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
  
   public function checkAlpha2($extraInfo, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $extraInfo)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z .,!\-]+$/i", $extraInfo)) {
            return false;
        }
        
        return true;
    }
  
     public function checkAlpha3($comment, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $comment)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z .,!\-]+$/i", $comment)) {
            return false;
        }
        
        return true;
    }
  
       public function checkAlpha4($occupation, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $occupation)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $occupation)) {
            return false;
        }
        
        return true;
    }
  
         public function checkAlpha5($doctor, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $doctor)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $doctor)) {
            return false;
        }
        
        return true;
    }
  
       public function checkAlpha6($note, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $note)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z .,!\-]+$/i", $note)) {
            return false;
        }
        
        return true;
    }
  
         public function checkAlpha7($invoiceInfo, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $invoiceInfo)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z .,!\-]+$/i", $invoiceInfo)) {
            return false;
        }
        
        return true;
    }
  
         public function checkMedicare($medicareNum, array $context)
    {
      // space
        if (preg_match("# #", $medicareNum)) {
            return false;
        }
      
      // number
        if (!preg_match("#[0-9]#", $medicareNum)) {
            return false;  
        }
      
      if (preg_match("#\W+#", $medicareNum) ) {
            return false;
        }
        // Uppercase
        if (preg_match("#[A-Z]#", $medicareNum)) {
            return false;
        }
      
        return true;
    }
  
           public function checkRef($referenceNum, array $context)
    {
        if (preg_match("# #", $referenceNum)) {
            return false;
        }
      
      // number
        if (!preg_match("#[0-9]#", $referenceNum)) {
            return false;  
        }
      
      if (preg_match("#\W+#", $referenceNum) ) {
            return false;
        }

        // Uppercase
        if (preg_match("#[A-Z]#", $referenceNum)) {
            return false;
        }
      
        return true;
    }
  
    public function checkStreet($street, array $context)
    {
     if (!preg_match('/^[a-z0-9 ]*$/i', $street) ) {
            return false;
        }
              
        return true;
    }
  public function checkPC($postalCode, array $context)
    {

      if (preg_match("#\W+#", $postalCode) ) {
            return false;
        }
              
        return true;
    }
  
      public function checkCity($city, array $context)
    {
     if (!preg_match('/^[a-z ]*$/i', $city) ) {
            return false;
        }
              
        return true;
    }
  
        public function checkState($state, array $context)
    {
     if (!preg_match('/^[a-z ]*$/i', $state) ) {
            return false;
        }
              
        return true;
    }
  
          public function checkCountry($country, array $context)
    {
     if (!preg_match('/^[a-z ]*$/i', $country) ) {
            return false;
        }
              
        return true;
    }
}
