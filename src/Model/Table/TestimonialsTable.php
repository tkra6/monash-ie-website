<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Testimonials Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Services
 *
 * @method \App\Model\Entity\Testimonial get($primaryKey, $options = [])
 * @method \App\Model\Entity\Testimonial newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Testimonial[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Testimonial|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Testimonial patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Testimonial[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Testimonial findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TestimonialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('testimonials');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Services', [
            'foreignKey' => 'services_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->add("name", [
                  "custom" => [
                  "rule" => [$this, "checkAlpha"], //add the new rule 'checkAlpha' to name field
                  "message" => "Please do not enter number/special character."
                    ]])
          
            ->add ("name", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                  "message" => "Please Enter some Text, don't leave it blank."
                    ]])
          
            ->notEmpty('name');

        $validator
            ->requirePresence('content', 'create')
            ->add ("content", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                  "message" => "Please Enter Some Text, don't leave it blank."
                    ]])
          
                  ->add("content", [
                  "custom" => [
                  "rule" => [$this, "checkAlphaDot"], //add the new rule 'alphaNumeric' to content field
                  "message" => "Please do not enter number/special character other than (.,!)."
                    ]])
      
            ->notEmpty('content');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

      

        return $validator;
    }
    
      public function checkAlphaDot($content, array $context) {
            if (!preg_match('/^[a-z .,!\-]+$/i', $content)) {
              return false;
            }
        //Check first letter isit alphabet
        if (!preg_match('/^[a-zA-Z]/', $content)) {
          return false;
        }
        return true;
   }
  
       public function checkAlpha($name, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $name)) {
          return false;
        }
      
        // special characters
        if (!preg_match("/^[a-z \-]+$/i", $name)) {
            return false;
        }
      
        // Up
        return true;
    }
  

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
  
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['services_id'], 'Services'));

        return $rules;
    }
}
