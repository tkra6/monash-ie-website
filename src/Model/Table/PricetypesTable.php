<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pricetypes Model
 *
 * @method \App\Model\Entity\Pricetype get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pricetype newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pricetype[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pricetype|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pricetype patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pricetype[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pricetype findOrCreate($search, callable $callback = null, $options = [])
 */
class PricetypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pricetypes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
          ->add ("name", ["notEmpty" => [
                        "rule" => ["notBlank"], //add the new rule 'notBlank' to username field
                        "message" => "Please Enter some text, don't leave it blank."
                          ]])

              ->add("name", [
                      "custom" => [
                          "rule" => [$this, "checkAlpha"], //add the new rule 'checkSpecial' to username field
                          "message" => "Please do not enter number/special character except -. Always start with letter"
                      ]])
            ->notEmpty('name');

        return $validator;
    }
       public function checkAlpha($name, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $name)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $name)) {
            return false;
        }
        
        return true;
    }
}
