<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contents Model
 *
 * @method \App\Model\Entity\Content get($primaryKey, $options = [])
 * @method \App\Model\Entity\Content newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Content[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Content|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Content[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Content findOrCreate($search, callable $callback = null, $options = [])
 */
class ContentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contents');
        $this->setDisplayField('type');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->add("type", [
                        "custom" => [
                            "rule" => [$this, "checkAlpha"], //add the new rule 'checkAlpha' to last_name field
                            "message" => "Please start with Alphabet only. Only Alphabet & space are allowed."
                        ]])
          ->add ("type", ["notEmpty" => [
                  "rule" => ["notBlank"], //add the new rule 'notBlank' to last_name field
                  "message" => "Please Enter Some Text, don't leave it blank."
                    ]])
            ->notEmpty('type');

        $validator
            ->requirePresence('detail', 'create')
            ->requirePresence('detail', 'create')
            ->add ("detail", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'notBlank' to last_name field
                    "message" => "Please Enter Some Text, don't leave it blank."
                      ]])
            ->notEmpty('detail');

        $validator
            ->allowEmpty('image');
      
          $validator
                ->requirePresence('main', 'create')
                ->notEmpty('main');

        return $validator;
    }
  
         public function checkAlpha($type, array $context)
    {
      //Check first letter isit alphabet
      if (!preg_match('/^[a-zA-Z]/', $type)) {
          return false;
        }

        // space, alphabet
        if (!preg_match("/^[a-z \-]+$/i", $type)) {
            return false;
        }
        
        return true;
    }
  
  
}
