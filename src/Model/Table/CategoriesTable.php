<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentCategories
 * @property \Cake\ORM\Association\HasMany $ChildCategories
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class CategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
      

        $this->belongsTo('ParentCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id',
          'dependent' => true,
    'cascadeCallbacks' => true
        ]);
      
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->add ("name", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
            ->add("name", [
                    "custom" => [
                    "rule" => [$this, "checkAlpha"], //add the new rule 'checkAlpha' to name field
                    "message" => "Please do not enter number/special character except for - and &. Always start with letter."
                      ]])
            ->notEmpty('name');

        $validator
            ->requirePresence('description', 'create')
            ->add ("description", ["notEmpty" => [
                    "rule" => ["notBlank"], //add the new rule 'alphaNumeric' to content field
                    "message" => "Please Enter some Text, don't leave it blank."
                      ]])
          ->add("description", [
                    "custom" => [
                    "rule" => [$this, "checkAlpha1"], //add the new rule 'checkAlpha' to name field
                    "message" => "Please do not enter number/special character except for - and &. Always start with letter."
                      ]])
            ->notEmpty('description');
      
        $validator
          ->add('lft', 'valid', ['rule' => 'numeric'])
      //    ->requirePresence('lft', 'create')
          ->notEmpty('lft');

        $validator
            ->add('rght', 'valid', ['rule' => 'numeric'])
        //    ->requirePresence('rght', 'create')
            ->notEmpty('rght');

        return $validator;
    }
  
       public function checkAlpha($name, array $context) {
            if (!preg_match('/^[a-z &\-]+$/i', $name)) {
              return false;
            }
        //Check first letter isit alphabet
        if (!preg_match('/^[a-zA-Z]/', $name)) {
          return false;
        }
        return true;
   }
  
  public function checkAlpha1($description, array $context) {
            if (!preg_match('/^[a-z &\-]+$/i', $description)) {
              return false;
            }
        //Check first letter isit alphabet
        if (!preg_match('/^[a-zA-Z]/', $description)) {
          return false;
        }
        return true;
   }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentCategories'));

        return $rules;
    }
}
