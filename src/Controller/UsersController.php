<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Controller\Component\CookieComponent;
use Cake\Auth\DefaultPasswordHasher;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */

class UsersController extends AppController
{
  

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('default1');
       
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
  
  public function forgetPw()
{         
      if ($this->request->is('post')) {
        $test = $this->request->getData();
        $theEmail = $test['email'];

        $checkEmail = $this->Users->find('list')->select(['email'])->where(['email' => $theEmail])->toArray();
        $count = sizeof($checkEmail);  
        
        
        
      if($count >=1){
                  $theuser = $this->Users->find('list')->select(['id'])->where(['email' => $theEmail]);
        foreach ($theuser as $theusers):		
				$user = $this->Users->get($theusers, [
            'contain' => []
        ]);
        endforeach;
                  
         $lowerCase = 'abcdefghijklmnopqrstuvwxyz';
         $number = '0123456789'; 
         $upperCase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
         $special = '!@#$%^&*-_'; 
         $string = '';
         $max1 = (strlen($lowerCase)) - 1;
         $max2 = (strlen($number)) - 1;
         $max3 = (strlen($upperCase)) - 1;
         $max4 = (strlen($special)) -1 ;
         for ($i = 0; $i < 4; $i++) {
              $string .= (str_shuffle(($lowerCase[mt_rand(0, $max1)]).($number[mt_rand(0, $max2)]).($upperCase[mt_rand(0, $max3)]).($special[mt_rand(0, $max4)])));
         }
        
          $password = $string;
          $hasher = new DefaultPasswordHasher();
          $updatePassword = $hasher->hash($string);

        $this->Users->updateAll(
        array('password' => $updatePassword),
        array('Users.id' => $theusers ));
        
        $this->set(compact('checkEmail'));
        $this->set('theuser', $theuser);
        $this->set('user', $user);
        $this->set('_serialize', ['checkEmail', 'theuser', 'user']);
				
                if (!empty($theEmail))
                {
                    $email = new Email();
                    $email->template('reset');
                    $email->emailFormat('both');
                    $email->setFrom('ieteam16@gmail.com');
                    $email->setTo($theEmail);
                    $email->subject('Password Reset - Musclework Massage');
                  $email->viewVars([
                        'email' => $theEmail,
                        'first' => $user['first_name'],
                        'last' => $user['last_name'],
                        'password' => $password,
                                    ]);
                    $email->send();                   
                }

          $this->Flash->success('Password Reset Request Successfully!.');
        
        } elseif ($count < 1) {
            $this->Flash->error(__('Invalid email in the server.'));   
        
      }
       
    }
      return $this->redirect(['action' => 'login']);

}

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $user = $this->Users->get($id,[]
        );

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->viewBuilder()->setLayout('default1');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
              
              
              
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
          
          
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                   
              
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
                 
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
  // In src/Controller/UsersController.php
public function login()
{
        $this->viewBuilder()->setLayout('defaultlogin');
        $this->loadComponent('Cookie');

    if ($this->request->is('post')) {
        $user = $this->Auth->identify();
//       if (($this->request->data['rememberMe']) == 1) {
//            unset($this->request->data['rememberMe']); 
       
//     $cookie = array();
//     $cookie['username'] = $this->request->data['username'];
//     $cookie['password'] = $this->request->data['password']; 
//     $this->Cookie->write('rememberMe', $cookie, true, '1 week');

      
//    /*    if (empty($this->data)) {
//             $cookie = $this->Cookie->read('rememberMe');
//          if (!is_null($cookie)) {
//               $this->request->data = $cookie;
//                 $user = $this->Auth->identify();
//                 }
//             } */
      
//   //           
//       $this->Cookie->read('username', 'password');

        if ($user) {

            $this->Auth->setUser($user);
             parent::initialize();
             $this->Auth->allow(['controller' =>'users']);
            return $this->redirect($this->Auth->redirectUrl(['controller' => 'users']));
             
        }
		//}


                         echo "<script>
                          window.location.href='../users/login';
                          alert('Your username or password is incorrect.');
                          redirect('../users/login');
                          </script>";
        //$this->Flash->error('Your username or password is incorrect.');
    }
}
  
  public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout', 'testlogin', 'forgetPw']);
}

public function logout()
{

             echo "<script>
                   window.location.href='../users/login';
                   alert('You are now logged out.');
                   redirect('../users/login');
                   </script>";
  
        return $this->redirect($this->Auth->logout());
   // $this->Flash->success('You are now logged out.');
}
  
  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete', 'email','testlogin'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
  
  public function email()
{
        $this->viewBuilder()->setLayout('default1');

  if ($this->request->is('post')) {
            $emailAddress = $this->request->data['email'];
            $url = Router::Url(['controller' => 'Users', 'action' => 'add'], true);
            //$encoded_url = str_replace('%2F', '/', urlencode($url));
            $query = $this->Users->findByEmail($emailAddress);
            $user = $query->first();
            if (is_null($user)) {
                    $email = new Email();
                    $email->template('themail');
                    $email->emailFormat('both');
                    $email->setFrom('ieteam16@gmail.com');
                    $email->setTo($emailAddress);
                    //$email->setTo('hellosupergirl95@gmail.com');
                    $email->subject('Gift Voucher - Musclework Massage');
                    $email->viewVars(['url' => $url]);
                    if ($email->send()) {
                        $this->Flash->success(__('Email Sent'));
                    } else {
                        $this->Flash->error(__('Error sending email: ') . $email->smtpError);
                    }                
            } else {
              $this->Flash->error('Bad');
            }
          }
}
}