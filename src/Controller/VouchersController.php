<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;





/**
 * Vouchers Controller
 *
 * @property \App\Model\Table\VouchersTable $Vouchers
 */
class VouchersController extends AppController
{

	public function archive($id=null) {
		

	Time::setToStringFormat('dd-MM-yyyy');
	$time = new Time('now,','Australia/Sydney');
	$newTime = $time->format('Y/m/d');
  $test_ar = explode('-', $newTime);
	$count = 0;
	
	$this->loadModel('Archives');
		
        $this->viewBuilder()->setLayout('default1');

        $this->paginate = [
            'contain' => ['Status', 'Prices'],
					'order' => ['id' => 'asc']
        ];
        $vouchers = $this->paginate($this->Vouchers);
		
		//Use query to find expiry voucher
		$vvv = $this->Vouchers->find('list')->select(['id'])
		 ->where(['expiry <' => $test_ar[0]]);
		
				
		    $this->set(compact('vouchers', 'vvv'));
        $this->set('_serialize', ['vouchers' , 'vvv']);
				
				//In this loop, it only have the id of expiry voucher
				foreach ($vvv as $vvvs):		
				$voucher = $this->Vouchers->get($vvvs)->toArray();

			//Trying to save the vouchers to archive table
				$hello = $this->Archives->newEntity();

		 		$hello = $this->Archives->patchEntity($hello, $voucher);
				 if ($this->Archives->save($hello)) {	
								}				
		endforeach; 
		
		//Trying to find how many vouchers are processed
		foreach ($vvv as $aaa):		
				$delVoucher = $this->Vouchers->get($aaa);
				$this->Vouchers->delete($delVoucher);	
 				$count+= count($aaa);
		endforeach; 
		
		//After everything is processed, you return to the index
		//if there is nth to process, return to the index page with UPTODATE
if (($count) == (sizeof($vvv))) {
	//return $this->redirect(['action' => 'index']);
		echo "<script>
        window.location.href='/vouchers/index';
        alert('Expired vouchers have been Archived.');
        redirect('/vouchers/index');
        </script>";
	//$this->Flash->success(__('Expired vouchers have been Archived.'));
} else {
	//return $this->redirect(['action' => 'index']);
	//echo "Your table is UP-TO-DATE";
	echo "<script>
        window.location.href='/vouchers/index';
        alert('Your table is UP-TO-DATE.');
        redirect('/vouchers/index');
        </script>";
}
		    
}
	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
public function index()
    {
        $this->viewBuilder()->setLayout('default1');

        $this->paginate = [
            'contain' => ['Status', 'Prices'],
					'order' => ['id' => 'asc']
        ];
        $vouchers = $this->paginate($this->Vouchers);

        $this->set(compact('vouchers'));
        $this->set('_serialize', ['vouchers']);
	
    }

    /**
     * View method
     *
     * @param string|null $id Voucher id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
public function view($id = null)
    {
              $this->viewBuilder()->setLayout('default1');

        $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);

        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
 public function add()
    {
		
			Time::setToStringFormat('dd-MM-yyyy');
		$time = new Time('now,','Australia/Sydney');
		$newTime = $time->format('Y/m/d');
  	$test_ar = explode('-', $newTime);
        $this->viewBuilder()->setLayout('default1');
    
        $voucher = $this->Vouchers->newEntity();
        if ($this->request->is('post')) {
            $voucher = $this->Vouchers->patchEntity($voucher, $this->request->getData());
           // set a UUID value
          $uuid = Text::uuid();
          $voucher->UUID = $uuid;
					
					 $start = $voucher->datePurchase;
					

					$purchase_date = $voucher->datePurchase;
				
					 $year = $purchase_date->year; // 2014
					 $month = $purchase_date->month; // 5
					 $day = $purchase_date->day; // 10
					
					 $start = ($year . "/" . $month . "/" . $day);

 					$expiry = strtotime("+3 months", strtotime($start));
					$expiry = strftime ( '%Y/%m/%d' , $expiry );

          $voucher->expiry = $expiry;
					
          if ($this->Vouchers->save($voucher)) {
                $this->Flash->success(__('The voucher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The voucher could not be saved. Please, try again.'));
        }
        $status = $this->Vouchers->Status->find('list', ['limit' => 200]);
        $prices = $this->Vouchers->Prices->find('list', ['limit' => 200]);
        $this->set(compact('voucher', 'status', 'prices'));
        $this->set('_serialize', ['voucher']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Voucher id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
public function edit($id = null)
    {
              $this->viewBuilder()->setLayout('default1');

        $voucher = $this->Vouchers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $voucher = $this->Vouchers->patchEntity($voucher,$this->request->getData());
            if ($this->Vouchers->save($voucher)) {
                $this->Flash->success(__('The voucher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The voucher could not be saved. Please, try again.'));
        }
      //$this->loadModel('Status');      
        $status = $this->Vouchers->Status->find('list', ['limit' => 200]);
        $prices = $this->Vouchers->Prices->find('list', ['limit' => 200]);
        $this->set(compact('voucher', 'status', 'prices'));
        $this->set('_serialize', ['voucher']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Voucher id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
              $this->viewBuilder()->setLayout('default1');

        $this->request->allowMethod(['post', 'delete']);
        $voucher = $this->Vouchers->get($id);
        if ($this->Vouchers->delete($voucher)) {
            $this->Flash->success(__('The voucher has been deleted.'));
        } else {
            $this->Flash->error(__('The voucher could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
  
   public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout', 'prices', 'details', 'paymentOptions', 'success', 'phonePayment', 'newClient', 'unsuccessful', 'viewPdfDownload']);
}
  
  
   public function newClient()
  {
          $firstName = "null";
          $lastName = "null";
          $email = "not@real.email";
          $gender = "";
          $number = "00000";
          $address1 = "null";
          $address2 = "null";
          $country = "Australia";
          $state = "Victoria ";
          $city = "Melbourne";
          $postcode = "4444";
      
//        $dataStruct = $this->Vouchers->newEntity();
        if ($this->request->is('post')) {
          
          $firstName = $this->request->getdata("Firstname");
          $lastName = $this->request->getdata("Lastname");
          $email = $this->request->getdata("email");
          $gender = $this->request->getdata("id_gender");
          $number = $this->request->getdata("number");
          $address1 = $this->request->getdata("Address1");
          $address2 = $this->request->getdata("Address2");
          $country = $this->request->getdata("Country");
          $state = $this->request->getdata("State");
          $city = $this->request->getdata("City");
          $postcode = $this->request->getdata("postcode");
          $now = Time::now();
          echo $now;
          
            if ($gender = 1) {
              $gender = "Male";
          } else if ($gender = 2){
              $gender = "Female";
          } else {$gender = "Not Applicable";
                 }
          
        }
      
      
         
   
      
   $http = new Client([
           'headers' => ['Accept' => 'application/json', 'User-Agent' => 'test(rmgil5@student.monash.edu)'],
              'auth' => ['username' => '1ad78e84f83355e1856cd02447b901c7', 'password' => '']]);
      
      
      
    
$response = $http->post('https://api.cliniko.com/v1/patients',[

  "address_1" => "$address1",
  "address_2" => "$address2",
  "address_3" => "",
  "archived_at" => "",
  "city" => "$city",
  "country" => "",
  "created_at" => "",
  "date_of_birth" => "",
  "deleted_at" => "",
  "email" => "$email",
  "emergency_contact" => "",
  "first_name" => "$firstName",
  "invoice_default_to" => "",
  "invoice_email" => "",
  "invoice_extra_information" => "",
  "gender" => "$gender",
  "id" => 1,
  "last_name" => "$lastName",
  "notes" => "",
  "occupation" => "",
  "old_reference_id" => "",
  "post_code" => "$postcode",
  "referral_source" => "",
  "reminder_type" => "SMS & Email",
  "state" => "",
  "title" => "",
  "updated_at" => "2013-03-26T14:00:00Z",
  "patient_phone_numbers" => []
  
   
  
  
]);
      
    }

  
  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete', 'viewPdf','email', 'archive', 'viewPdfDownload'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}

      function viewPdf($id = null)
        {
          
        
         $this->loadModel('Contents');
        $theAddress = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Address']);
				$this->set('theAddress',$theAddress);
    
        $thePhone = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Phone Number']);
				$this->set('thePhone',$thePhone);
				
          
        $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);
				
				$modality_id = $voucher['price']['pricetypes_id'];
				$this->loadModel('pricetypes');
        $modality = $this->pricetypes->find('all')->select(['name'])
        ->where(['id' => $modality_id]);
				$this->set('modality',$modality);
				
			


        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);
        } 
	
	function viewPdfDownload() {  
		 		$voucherData = $this->request->session()->read('voucherSession');
						$this->Vouchers->save($voucherData);

				$id = $voucherData->id;
		
				$this->loadModel('Contents');
        $theAddress = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Address']);
				$this->set('theAddress',$theAddress);
    
        $thePhone = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Phone Number']);
				$this->set('thePhone',$thePhone);
				
          
        $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);
				
				$modality_id = $voucher['price']['pricetypes_id'];
				$this->loadModel('pricetypes');
        $modality = $this->pricetypes->find('all')->select(['name'])
        ->where(['id' => $modality_id]);
				$this->set('modality',$modality);
				
        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);
		
		



		 
		
        }
  
  public function prices() 
  {
    
        $this->loadModel('Pricetypes');
		    $thePricetype = $this->Pricetypes->find('all');
				$this->set('thePricetype',$thePricetype);
		
		    $this->loadModel('Prices');
		    $thePrice = $this->Prices->find('all')->contain(['Pricetypes']);
				$this->set('thePrice',$thePrice);
    
  }
  
     public function prices2() 
  {
    
    
        $this->loadModel('Prices');
        $prices = $this->paginate($this->Prices);

        $this->set(compact('prices'));
        $this->set('_serialize', ['prices']);
    
  }
  
  public function details($id = null) {
     		$voucher = $this->Vouchers->newEntity();
  
        if ($this->request->is('post')) {
         	$voucher = $this->Vouchers->patchEntity($voucher, $this->request->getData());
					
          $time = new Time('now,','Australia/Sydney');
          $voucher->datePurchase = $time;
					
					
          $voucher->prices_id = $id;
					$voucher->status_id = 2;  // "Unredeemed by default"
					
          $uuid = Text::uuid();
          $voucher->UUID = $uuid;
					
					$expiry = date('Y-m-d', strtotime('+3 months'));
          $voucher->expiry = $expiry;
          
          $session = $this->request->session()->write('voucherSession', $voucher);
					
					// create session to send through price amount securely
					$this->request->session()->write(
							'priceSession', $id);

					return $this->redirect(['action' => 'paymentOptions']);
					
				}
					
		
        $this->set(compact('voucher', 'status', 'prices', 'price'));
        $this->set('_serialize', ['voucher', 'price']);
  }
  
  public function paymentOptions($id = null)
  {
          // read and store price amount from session
          $amount = $this->request->session()->read('priceSession');

		
		     

          //get the price amount from prices_id
          $this->loadModel('prices');
          $price = $this->prices->get($amount);
          $value = $price->value;
						
    
         $this->set('value', $value);
         $this->set('_serialize', ['value']);
    
    //$this->viewBuilder()->setLayout(false); // temporarily disabled Layout render (so I can test form)    
  }
  
   public function success($id = null)
  {
		 			$voucherData = $this->request->session()->read('voucherSession');
						$this->Vouchers->save($voucherData);

				$id = $voucherData->id;
		
				$this->loadModel('Contents');
        $theAddress = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Address']);
				$this->set('theAddress',$theAddress);
    
        $thePhone = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Phone Number']);
				$this->set('thePhone',$thePhone);
				
          
        $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);
				
				$modality_id = $voucher['price']['pricetypes_id'];
				$this->loadModel('pricetypes');
        $modality = $this->pricetypes->find('all')->select(['name'])
        ->where(['id' => $modality_id]);
				$this->set('modality',$modality);
				
        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);

		 
         $voucherData = $this->request->session()->read('voucherSession');
		 		 $voucherData->paymentStatus = 1; //Paid - Paypal
		 
           if ($this->Vouchers->save($voucherData)) {
						 						 
						  $voucher = $this->Vouchers->get($voucherData->id, [
            'contain' => ['Status', 'Prices']
        ]);
						 
							$UUID = $voucher['UUID'];


							$email = new Email();

							$email->template('hero');
							$email->emailFormat('html');
							$mAttach =  ("../src/Template/Vouchers/PDFVouchers/Voucher-".$UUID.".pdf");
							$email->attachments($mAttach); 

							$email->setFrom('ieteam16@gmail.com');
							$email->setTo($voucherData->fromEmail);

							$email->subject('Gift Voucher Purchase - Muscleworks Massage');
							$email->viewVars([
									'uuid' => $voucher['UUID'],
									'toName' => $voucher['toName'],
									'fromName' => $voucher['fromName'],
									'price' => $voucher['price']['minutes'],
									'expiry' => $voucher['price']['value'],
								 'message' => $voucher['message'],
															]);
							$email->send();
						 
           } else {
                         $this->Flash->error(__('The voucher could not be saved. Please, try again.'));

           }
          
  }
	
	public function unsuccessful() {
		 if ($this->Vouchers->save($voucherData)) {
           } else {
                         $this->Flash->error(__('The voucher could not be saved. Please, try again.'));

           }
		
	}
  
    public function phonePayment($id = null) {
			 $voucherData = $this->request->session()->read('voucherSession');
			 $voucherData->paymentStatus = 0; //Paid - Paypal

			$this->loadModel('contents');
		    $phone = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Phone Number']);
				$this->set('phone',$phone);
			
       $voucherData = $this->request->session()->read('voucherSession');
           if ($this->Vouchers->save($voucherData)) {
           } else {
                         $this->Flash->error(__('The voucher could not be saved. Please, try again.'));

           }
    }
  
    public function email($id = null)
{
        $this->viewBuilder()->setLayout('default1'); 
       $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);

        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);
        
        
  if ($this->request->is('post')) {
		
           $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);

        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);
    
            $emailAddress = $this->request->data['email'];
            $UUID = $voucher['UUID'];
            
     
                    $email = new Email();

                    $email->template('hero');
                    $email->emailFormat('html');
                    $mAttach =  ("../src/Template/Vouchers/PDFVouchers/Voucher-".$UUID.".pdf");
                    $email->attachments($mAttach); 
    
                    $email->setFrom('ieteam16@gmail.com');
                    $email->setTo($emailAddress);

                    $email->subject('Gift Voucher Purchase - Muscleworks Massage');
                    $email->viewVars([
                        'uuid' => $voucher['UUID'],
                        'toName' => $voucher['toName'],
                        'fromName' => $voucher['fromName'],
                        'price' => $voucher['price']['minutes'],
                        'expiry' => $voucher['price']['value'],
                       'message' => $voucher['message'],
                                    ]);
                    if ($email->send()) {		
											
										$this->Flash->Success('Email has been sent!');
										return $this->redirect(['action' => 'index']);


                      
                    } else {
                        $this->Flash->error(__('Error sending email: ') . $email->smtpError);
                    }                

          }
}
  

}