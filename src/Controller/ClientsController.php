<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\ContactForm;
use App\Form\LoginForm;
use Cake\Routing\Router;
use Cake\Mailer\Email;
use Cake\Http\Client;
use Cake\I18n\Time;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 *
 * @method \App\Model\Entity\Client[] paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{

    public function index()
    {
        $this->viewBuilder()->setLayout('default1');


        $clients = $this->paginate($this->Clients);

        $this->set(compact('clients'));
        $this->set('_serialize', ['clients']);
    }
  
  
      public function test1()
    {
            $this->viewBuilder()->setLayout('try');

        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
              $this->request->session()->write('client.title',$this->request->getData('title'));
              $this->request->session()->write('client.firstName',$this->request->getData('firstName'));
              $this->request->session()->write('client.lastName',$this->request->getData('lastName'));
              $this->request->session()->write('client.email',$this->request->getData('email'));
              $this->request->session()->write('client.gender',$this->request->getData('gender'));
              $this->request->session()->write('client.DOB',$this->request->getData('DOB'));
              $this->request->session()->write('client.extraInfo',$this->request->getData('extraInfo'));
              $this->request->session()->write('client.comment',$this->request->getData('comment'));
              $this->request->session()->write('client.occupation',$this->request->getData('occupation'));
              $this->request->session()->write('client.emergency',$this->request->getData('emergency'));
              $this->request->session()->write('client.medicareNum',$this->request->getData('medicareNum'));
              $this->request->session()->write('client.referenceNum',$this->request->getData('referenceNum'));
              $this->request->session()->write('client.doctor',$this->request->getData('doctor'));
              $this->request->session()->write('client.note',$this->request->getData('note'));
              $this->request->session()->write('client.reminder',$this->request->getData('reminder'));
              $this->request->session()->write('client.smsMarketing',$this->request->getData('smsMarketing'));
              $this->request->session()->write('client.confirmation',$this->request->getData('confirmation'));
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
        //$address = $this->Clients->Address->find('list', ['limit' => 200]);
       // $this->set(compact('client', 'address'));
        $this->set('_serialize', ['client']);
    }
  
    public function clientform()
    {
          $this->viewBuilder()->setLayout('default');

        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
       // $address = $this->Clients->Address->find('list', ['limit' => 200]);
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);
    }
    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       $this->viewBuilder()->setLayout('default1');

        $client = $this->Clients->get($id, [
            'contain' => []
        ]);

        $this->set('client', $client);
        $this->set('_serialize', ['client']);
    
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
            $this->viewBuilder()->setLayout('default1');

        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
       // $address = $this->Clients->Address->find('list', ['limit' => 200]);
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
            $this->viewBuilder()->setLayout('default1');

        $client = $this->Clients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
       // $address = $this->Clients->Address->find('list', ['limit' => 200]);
        $this->set(compact('client'));
        $this->set('_serialize', ['client']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        if ($this->Clients->delete($client)) {
            $this->Flash->success(__('The client has been deleted.'));
        } else {
            $this->Flash->error(__('The client could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
     public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout', 'clientform', 'test1', 'waitlist', 'test', 'booklogin', 'viewbookings', 'cancelbooking']);
}

  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
  
  
   public function waitList()
  {
     
     $http1 = new Client([
              'headers' => ['Accept' => 'application/json', 'User-Agent' => 'test(rmgil5@student.monash.edu)'],
              'auth' => ['username' => '1ad78e84f83355e1856cd02447b901c7', 'password' => '']]);
    $response1 = $http1->get('https://api.cliniko.com/v1/appointment_types', [], ['type' => 'json']);
    $jsonAppTypes = $response1->json;
     
     
    $http2 = new Client([
              'headers' => ['Accept' => 'application/json', 'User-Agent' => 'test(rmgil5@student.monash.edu)'],
              'auth' => ['username' => '1ad78e84f83355e1856cd02447b901c7', 'password' => '']]);
    $response2 = $http2->get('https://api.cliniko.com/v1/practitioners', [], ['type' => 'json']);
    $jsonPractitioners = $response2->json;

      $this->viewBuilder()->setLayout('default');
        $waitForm = new ContactForm();
        if ($this->request->is('post')) {
            if ($waitForm->execute($this->request->getData())) {
                     $url = Router::Url(['controller' => 'Articles', 'action' => 'list'], true);
              
                     foreach($jsonAppTypes['appointment_types'] as $question)
                      {
                          for ($i = 0; $i < 1; $i++)
                          {
                              $appresults[] = $question['name'];

                          }
                      }
              
                    $counter = 0;
                    foreach($jsonPractitioners['practitioners'] as $question)
                    {
                        for ($i = 0; $i < 1; $i++)
                        {
                            $counter = $counter + 1;

                        }
                    }
                    $practitioners = array();
                    for ($i = 0; $i < count($counter) - 1; $i++)
                        {
                            $practitioners[] = $this->request->data[i];

                        }
                    $email = new Email();
                    $email->template('waitlist');
                    $email->emailFormat('html');
                    $email->setFrom('ieteam16@gmail.com');
                    $email->setTo('adenehh@gmail.com');
                    $email->subject('New Cliniko Wait List Recipient (automated)');
                    $email->viewVars([
                        'firstName' => $this->request->data['firstName'],
                        'lastName' => $this->request->data['lastName'],
                        'email' => $this->request->data['email'],
                        'appType' => $appresults[$this->request->data['appointmentType']],
                        'prac' => $this->request->data['prac'],
                       'Monday' => $this->request->data['Monday'],
                       'Tuesday' => $this->request->data['Tuesday'],
                       'Wednesday' => $this->request->data['Wednesday'],
                       'Thursday' => $this->request->data['Thursday'],
                       'Friday' => $this->request->data['Friday'],
                       'Saturday' => $this->request->data['Saturday'],
                       'urgentAppointment' => $this->request->data['urgentAppointment'],
                       'onlyOutsideBusinessHours' => $this->request->data['onlyOutsideBusinessHours'],
                       'additionalRequests' => $this->request->data['additionalRequests']
                    ]);
                    
                    if ($email->send()) {
                    $url = array('controller' => 'pages', 'action' => 'book');
                    $second = 5;              
                    $this->response->header("refresh:$second; url='" . Router::url($url) . "'");
                    $this->Flash->Success('You have been added to the wait list! We will notify you of any open appointments to your preferences by email. You will be redirected in ' . $second . ' seconds.');

                    } else {
                        $this->Flash->error(__('Error sending email: ') . $email->smtpError);
                    }
              
              
            } else {
                $this->Flash->error('There was a problem submitting your form.');
            }
        }
     
    
     
    $this->set(compact('jsonAppTypes', 'waitForm', 'jsonPractitioners'));
  }

  
  public function test()
  {
    
    
    $http = new Client([
              'headers' => ['Accept' => 'application/json', 'User-Agent' => 'test(rmgil5@student.monash.edu)'],
              'auth' => ['username' => '1ad78e84f83355e1856cd02447b901c7', 'password' => '']]);
    
    
    $response = $http->get('https://api.cliniko.com/v1/appointment_types', [], ['type' => 'json']);
    $json = $response->json;
    
    

    $this->set(compact('json'));




    //debug($response);
    
  }
  
  public function booklogin()
  {
    $loginForm = new LoginForm();
    $this->set(compact('loginForm'));
    if ($this->request->is('post')) {
            if ($loginForm->execute($this->request->getData())) {
              
              $email = $this->request->getdata("email");
              $phone = $this->request->getdata("phone");
             
              // HTTP Client authentication with Cliniko API
              $http = new Client([
              'headers' => ['Accept' => 'application/json', 'User-Agent' => 'test(rmgil5@student.monash.edu)'],
              'auth' => ['username' => '1ad78e84f83355e1856cd02447b901c7', 'password' => '']]);
    
              // Get Patients from POST Email
              $response = $http->get('https://api.cliniko.com/v1/patients?q=email:='.$email, [], ['type' => 'json']);
              $clientjson = $response->json;
              $this->request->session()->write('clientjson', $clientjson);
              
              // Get Patient IDs from Patients
              $clientid = null;
              foreach($clientjson['patients'] as $patient)
              {
                  foreach ($patient['patient_phone_numbers'] as $patient_phones) {
                      if ($patient_phones['number'] == $phone) {
                        $clientid = $patient['id'];
                        $this->request->session()->write('patient_name', $patient['first_name']. ' '.$patient['last_name']);
                      }
                  }
              }
              
              if (empty($clientid)) {
                    return $this->Flash->error("Your phone and/or email did not match up with our records. Please try again.", [
                           'key' => 'invalidlogin']);
                  }
              
              // View Appointments for patient ID
              $response = $http->get('https://api.cliniko.com/v1/patients/'.$clientid.'/appointments', [], ['type' => 'json']);
              $appjson = $response->json;

              $bookingDetails = $this->getAppointmentDetails($appjson, $http);
              $this->request->session()->write('bookingDetails', $bookingDetails);

              
              return $this->redirect(['action' => 'viewbookings']);
              
            } else {
                
                }
    } 
    
    
    
  }
  
  public function viewbookings() {
        $bookingDetails = $this->request->session()->read('bookingDetails');
        $patientName = $this->request->session()->read('patient_name');
       // var_dump($this->request->session()->read('bookingDetails'));
          
        $this->set(compact('bookingDetails', 'patientName'));
  }
  
  public function cancelbooking($key = null) {
      $bookingDetails = $this->request->session()->read('bookingDetails');
       //var_dump($this->request->session()->read('bookingDetails'));
    
      $cancelForm = new ContactForm();
      if ($this->request->is('post')) {
        
              if ($cancelForm->execute($this->request->getData())) {
                
                  $http = new Client([
                  'headers' => ['Accept' => 'application/json', 'User-Agent' => 'test(rmgil5@student.monash.edu)'],
                  'auth' => ['username' => '1ad78e84f83355e1856cd02447b901c7', 'password' => '']]);
        
                  $response = $http->patch('https://api.cliniko.com/v1/appointments/'.$bookingDetails[$key]['appointment_id'].'/cancel',[

                  "cancellation_reason" => $this->request->data['cancellationReason'],
                  "cancellation_note" => ""
                  ]);
                  
                  array_splice($bookingDetails, $key, 1);
                  $this->request->session()->write('bookingDetails', $bookingDetails);
                  return $this->redirect(['action' => 'viewbookings']);


              }
        
      }
    
      $this->set(compact('bookingDetails', 'key', 'cancelForm'));
  }
  
  
  
  private function getAppointmentDetails($appjson, $http) {
    // Gets the following data from a Cliniko JSON "get appointments" response
    // Paramters: JSON response, http client instance
    // Returns: Array of filtered and formatted data
    
    $totalEntries = $appjson['total_entries'];
    
    if ($totalEntries == 0) {
        return null;
    } else {
    
      for ($i = 0; $i < $totalEntries; $i++) {
        
          $response = $http->get($appjson['appointments'][$i]['appointment_type']['links']['self'], [], ['type' => 'json']);
              $apptypejson = $response->json;
        
          $response = $http->get($appjson['appointments'][$i]['practitioner']['links']['self'], [], ['type' => 'json']);
              $practitionerjson = $response->json;
        
        
          $bookingDetails[$i] = ['start' => $appjson['appointments'][$i]['appointment_start'],
                            'end' => $appjson['appointments'][$i]['appointment_end'],
                            'madeon' => $appjson['appointments'][$i]['created_at'],
                            'patient_name' => $appjson['appointments'][$i]['patient_name'],
                            'practitioner_name' => $practitionerjson['first_name']. ' '.$practitionerjson['last_name'],
                            'appointment_id' => $appjson['appointments'][$i]['id'],
                            'appointment_type' => $apptypejson['name']];
        
      }
       return $bookingDetails;
    
    }
  }
  
}
