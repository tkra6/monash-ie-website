<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Prices Controller
 *
 * @property \App\Model\Table\PricesTable $Prices
 *
 * @method \App\Model\Entity\Price[] paginate($object = null, array $settings = [])
 */
class PricesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
                          $this->viewBuilder()->setLayout('default1');

        $this->paginate = [
            'contain' => ['Pricetypes']
        ];
        $prices = $this->paginate($this->Prices);

        $this->set(compact('prices'));
        $this->set('_serialize', ['prices']);
    }

    /**
     * View method
     *
     * @param string|null $id Price id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
                          $this->viewBuilder()->setLayout('default1');

        $price = $this->Prices->get($id, [
            'contain' => ['Pricetypes']
        ]);

        $this->set('price', $price);
        $this->set('_serialize', ['price']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
                          $this->viewBuilder()->setLayout('default1');

        $price = $this->Prices->newEntity();
        if ($this->request->is('post')) {
            $price = $this->Prices->patchEntity($price, $this->request->getData());
            if ($this->Prices->save($price)) {
                 $this->Flash->success(__('The price has been saved.'));
                 return $this->redirect(['action' => 'index']);
              
            }
            $this->Flash->error(__('The price could not be saved. Please, try again.'));
          
        }
        $pricetypes = $this->Prices->Pricetypes->find('list', ['limit' => 200]);
        $this->set(compact('price', 'pricetypes'));
        $this->set('_serialize', ['price']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Price id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
                          $this->viewBuilder()->setLayout('default1');

        $price = $this->Prices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $price = $this->Prices->patchEntity($price, $this->request->getData());
            if ($this->Prices->save($price)) {
                 $this->Flash->success(__('The price has been saved.'));
                 return $this->redirect(['action' => 'index']);
              
            }
            $this->Flash->error(__('The price could not be saved. Please, try again.'));
         
        }
        $pricetypes = $this->Prices->Pricetypes->find('list', ['limit' => 200]);
        $this->set(compact('price', 'pricetypes'));
        $this->set('_serialize', ['price']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Price id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $price = $this->Prices->get($id);
        if ($this->Prices->delete($price)) {
            $this->Flash->success(__('The price has been deleted.'));
        } else {
            $this->Flash->error(__('The price could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout']);
}

  
  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
}
