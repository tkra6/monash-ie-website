<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('default1');
        
        $this->paginate = [
            'contain' => ['Categories'],
          'order' => ['title' => 'desc']
        ];
        $articles = $this->paginate($this->Articles);
      
      
      
        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }
  
//       public function blog()
//     {
//        $this->paginate = [
//             'contain' => ['Categories', 'Users'],
//             'order' => ['title' => 'desc']
//         ];
//         $articles = $this->paginate($this->Articles);
        
        
//         $this->loadModel('Categories');
//         $categories = $this->Categories->find('list')->toArray();

//         $this->set(compact('categories', 'articles' ));
//         $this->set('_serialize', ['categories', 'articles']);

//     }
  
//         public function theCat($name = null)
//     {
//        $this->paginate = [
//             'contain' => ['Categories', 'Users'],
//             'order' => ['title' => 'desc']
//         ];
          
//         $articles = $this->paginate($this->Articles);
          
//           $this->loadModel('Categories');
//            $catArticles = $this->Categories->get($id, [
//             'contain' => []
//         ]);

//         $categories = $this->Categories->find('list')->toArray();

//         $this->set(compact('categories', 'articles' , 'catArticles'));
//         $this->set('_serialize', ['categories', 'articles', 'catArticles']);

//     }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
  
    public function tags($name=null)
  {
//    $tags= $this->request->params['pass'];
//     $articles = $this->Articles->find('categories',['categories'=>$tags]);
//     $this->set(compact('articles', 'tags'));
      
      $this->viewBuilder()->setLayout('default'); //later change to default
      
      $this->loadModel('Categories');
     
        $articles = $this->Articles->find('all')->contain(['Categories','Users'])          
        ->where(['name' => $name]);
      
         $categories = $this->Articles->find('all')->group('Categories_id')->contain(['Categories']);
            
        $this->set(compact('articles', 'categories'));
        $this->set('_serialize', ['articles', 'categories']);

  }
 
  
    public function view($id = null)
    {
            $this->viewBuilder()->setLayout('default1');

        $article = $this->Articles->get($id, [
            'contain' => []
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->viewBuilder()->setLayout('default1');
      $user = $this->request->session()->read('Auth.User');

      $article = $this->Articles->newEntity();    
      
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);

            }
          
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
          
          
        }
            $this->loadModel('Categories');
       // Just added the categories list to be able to choose
        // one category for an article
        $category = $this->Articles->Categories->find('treeList');
        $this->set(compact('categories'));
            //$category = $this->Categories->find('list')->toArray();
            $this->set(compact('article', 'user', 'category')); //to show the user, can see at dubug variable
            $this->set('_serialize', ['article']);
    }
  
  
    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('default1');

        $article = $this->Articles->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
              
            }
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
          
        }
      
            $this->loadModel('Categories');
            $category = $this->Categories->find('list')->toArray();
        $this->set(compact('article', 'user', 'category'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
            $this->viewBuilder()->setLayout('default1');

        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
  public function initialize()
        {
            parent::initialize();
            $this->Auth->allow(['logout', 'tags']);
        }

  public function isAuthorized($user)
      {
      $action = $this->request->getParam('action');

      // The add and index actions are always allowed.
      if (in_array($action, ['index', 'add', 'view', 'edit', 'delete'])) {
          return true;
      }
      // All other actions require an id.
      if (!$this->request->getParam('pass.0')) {
          return false;
      }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
}
