<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
      
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => $this->referer() // If unauthorized, return them to page they were just on
        ]);

        // Allow the display action so our pages controller
        // continues to work.
        $this->Auth->allow(['display']);
    }
      
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
  
  public function beforeFilter(Event $event) {
    //for example, you want to read messages
    //import the Model
    $this->loadModel('Pagecontents');
    $menupage = $this->Pagecontents->find('all')->select(['page'])
        ->where(['block' => '0'])
				->group('page');
				$this->set('menupage',$menupage);
		
		$this->loadModel('Testimonials');
    $theTestimonial = $this->Testimonials->find('all')
        ->where(['status' => 'Published'])->contain(['Services']);
				$this->set('theTestimonial',$theTestimonial);
		
    $this->loadModel('Contents');
		$thePromotion = $this->Contents->find('all')->select(['main','detail', 'image'])
        ->where(['type' => 'Promotion'])->toArray();
				$this->set('thePromotion',$thePromotion);
		
    $theAddress = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Address']);
				$this->set('theAddress',$theAddress);
    
    $thePhone = $this->Contents->find('all')->select(['detail'])
        ->where(['type' => 'Phone Number']);
				$this->set('thePhone',$thePhone);
		
		$this->loadModel('Pricetypes');
		 $thePricetype = $this->Pricetypes->find('all')->order('id');
				$this->set('thePricetype',$thePricetype);
		
		    $this->loadModel('Prices');
		 $thePrice = $this->Prices->find('all')->contain(['Pricetypes'])->order('Pricetypes_id')->order('value');
				$this->set('thePrice',$thePrice);
		
		
		$this->loadModel('Articles');
		$articles = $this->Articles->find('all')->toArray();
		//$blogPost = $this->Articles->find('all')->toArray();
		$blogPost = $this->Articles->find('all', array(
   'limit' => 3,
   'order' => array('Articles.modified' => 'desc'),
   'contain' => 'Categories',));
		
    $categories = $this->Articles->find('all')->group('Categories_id')->contain(['Categories']);
        
        //$this->loadModel('Categories');
        //$categories = $this->Categories->find('list')->toArray();

        $this->set(compact('categories', 'articles', 'blogPost'));
        $this->set('_serialize', ['categories', 'articles', 'blogPost']);

		
}
  
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
  
  public function isAuthorized($user)
{
    return false;
}
  
}