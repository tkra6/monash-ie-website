<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
  
  // $uses is where you specify which models this controller uses
  public $uses = array('Pagecontents');
  
  
    /**
     * Displays a view
     *
     * @param string ...$path Path segments.
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
      
        $this->loadModel('Pagecontents');
        
        $pagecontents = $this->Pagecontents->find('all')->select(['section', 'content', 'heading'])
        ->where(['page' => $page])
        ->where(['block !=' => '1']);       
         $this->set('pagecontents',$pagecontents);
         $this->set(compact('page', 'subpage', 'pagecontents'));
         $this->set('_serialize', ['page', 'subpage', 'pagecontents' ]);

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
      
    } 
  
  
       
  
public function beforeRender(Event $event) {
		$IDENTIFIER = "";
    
        $theTeam = $this->Contents->find('all')->select(['type', 'main' ,'detail', 'image'])
        ->where(['type' => 'Team']);
				$this->set('theTeam',$theTeam);
		
// 	 $this->set('testimonial', 'services');
    
        $theService = $this->Contents->find('all')->select(['type', 'main' ,'detail', 'image'])
        ->where(['type' => 'Service']);
				$this->set('theService',$theService);
	
				$theMain = $this->Contents->find('all')->select(['type', 'main' ,'detail', 'image'])
							->where(['type' => 'Main Slideshow']);
							$this->set('theMain',$theMain);
	
	      $theReady = $this->Contents->find('all')->select(['type', 'main' ,'detail', 'image'])
							->where(['type' => 'ready']);
							$this->set('theReady',$theReady);
	
		$test = $this->request->here('pages');
		//debug($test);
		

		if ($test == "/")  { 
			$IDENTIFIER = "home";
		}
		else {
		$test_ar = explode('/', $test);
		$IDENTIFIER = $test_ar[2];
		//debug($test_ar);
		}
		
		$this->loadModel('Pagecontents');
    $metatype = $this->Pagecontents->find('all')->select(['metatype'])
        ->where(['Pagecontents.page' => $IDENTIFIER ])
				->group('page');
				$this->set('metatype',$metatype);
}
  
  
  public function add()
    {
        $page = $this->Pages->newEntity();
        if ($this->request->is('post')) {
            $page = $this->Pages->patchEntity($page, $this->request->data);
            if ($this->Pages->save($page)) {
              $this->Page->create();
                 $this->_saveFile($this->data);
                $this->Flash->success('The page has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The page could not be saved. Please, try again.');
            }
        }
        $this->set(compact('page'));
        $this->set('_serialize', ['page']);
    }
  
}


  