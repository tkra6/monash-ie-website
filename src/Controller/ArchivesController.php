<?php
namespace App\Controller;


use Cake\I18n\Time;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Http\Client;


use App\Controller\AppController;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\ArchivesTable $Archives
 *
 * @method \App\Model\Entity\Archive[] paginate($object = null, array $settings = [])
 */
class ArchivesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      
        $this->viewBuilder()->setLayout('default1');
        $this->paginate = [
            'contain' => ['Status', 'Prices']
        ];
        $archives = $this->paginate($this->Archives);

        $this->set(compact('archives'));
        $this->set('_serialize', ['archives']);
    }

    /**
     * View method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $archive = $this->Archives->get($id, [
            'contain' => ['Status', 'Prices']
        ]);

        $this->set('archive', $archive);
        $this->set('_serialize', ['archive']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->viewBuilder()->setLayout('default1');
        $archive = $this->Archives->newEntity();
        if ($this->request->is('post')) {
            $archive = $this->Archives->patchEntity($archive, $this->request->getData());
                     // set a UUID value
                    $uuid = Text::uuid();
                    $archive->UUID = $uuid;
          
          // set expiry date to 1 year later
        	$expiry = date('Y-m-d', strtotime('+3 month'));
          $archive->expiry = $expiry;
          
          
            if ($this->Archives->save($archive)) {
                $this->Flash->success(__('The archive has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The archive could not be saved. Please, try again.'));
        }
        $status = $this->Archives->Status->find('list', ['limit' => 200]);
        $prices = $this->Archives->Prices->find('list', ['limit' => 200]);
        $this->set(compact('archive', 'status', 'prices'));
        $this->set('_serialize', ['archive']);
    }
  
  
  
  
//       {
//         $this->viewBuilder()->setLayout('default1');
    
//         $voucher = $this->Vouchers->newEntity();
//         if ($this->request->is('post')) {
//             $voucher = $this->Vouchers->patchEntity($voucher, $this->request->getData());
//            // set a UUID value
//           $uuid = Text::uuid();
//           $voucher->UUID = $uuid;
					
// 					// set expiry date to 1 year later
// 					$expiry = date('Y-m-d', strtotime('+3 month'));
//           $voucher->expiry = $expiry;
					
//           if ($this->Vouchers->save($voucher)) {
//                 $this->Flash->success(__('The voucher has been saved.'));

//                 return $this->redirect(['action' => 'index']);
//             }
//             $this->Flash->error(__('The voucher could not be saved. Please, try again.'));
//         }
//         $status = $this->Vouchers->Status->find('list', ['limit' => 200]);
//         $prices = $this->Vouchers->Prices->find('list', ['limit' => 200]);
//         $this->set(compact('voucher', 'status', 'prices'));
//         $this->set('_serialize', ['voucher']);
//     }

    /**
     * Edit method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $archive = $this->Archives->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $archive = $this->Archives->patchEntity($archive, $this->request->getData());
            if ($this->Archives->save($archive)) {
                $this->Flash->success(__('The archive has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The archive could not be saved. Please, try again.'));
        }
        $status = $this->Archives->Status->find('list', ['limit' => 200]);
        $prices = $this->Archives->Prices->find('list', ['limit' => 200]);
        $this->set(compact('archive', 'status', 'prices'));
        $this->set('_serialize', ['archive']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $archive = $this->Archives->get($id);
        if ($this->Archives->delete($archive)) {
            $this->Flash->success(__('The archive has been deleted.'));
        } else {
            $this->Flash->error(__('The archive could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
}
