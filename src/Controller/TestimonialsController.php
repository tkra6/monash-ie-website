<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Testimonials Controller
 *
 * @property \App\Model\Table\TestimonialsTable $Testimonials
 */
class TestimonialsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
      $this->viewBuilder()->setLayout('default1');
        $this->paginate = [
            'contain' => ['Services']
        ];
        $testimonials = $this->paginate($this->Testimonials);

        $this->set(compact('testimonials'));
        $this->set('_serialize', ['testimonials']);
    }

    /**
     * View method
     *
     * @param string|null $id Testimonial id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $testimonial = $this->Testimonials->get($id, [
            'contain' => ['Services']
        ]);

        $this->set('testimonial', $testimonial);
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->viewBuilder()->setLayout('default1');
        $testimonial = $this->Testimonials->newEntity();
        if ($this->request->is('post')) {

            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->getData());
                    //$try = $this->request->input('json_decode');

            if ($this->Testimonials->save($testimonial)) {
                  
                 $this->Flash->success(__('The testimonial has been saved.'));

                return $this->redirect(['controller' => 'testimonials', 'action' => 'index']);
            }
          
            $this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
        }
        $services = $this->Testimonials->Services->find('list', ['limit' => 200]);
        $this->set(compact('testimonial', 'services'));
        $this->set('_serialize', ['testimonial']);
      // echo $try;
    }
    
  public function add1()
    {
        $testimonial = $this->Testimonials->newEntity();
        if ($this->request->is('post')) {
            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->getData());
            if ($this->Testimonials->save($testimonial)) {
              
                 $this->Flash->success(__('The testimonial has been successfully submitted. Thank you so much for the feedback.'));
                 return $this->redirect(['controller' => 'pages', 'action' => 'home']);
              
            }
            $this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
                    
        }
        $services = $this->Testimonials->Services->find('list', ['limit' => 200]);
        $this->set(compact('testimonial', 'services'));
        $this->set('_serialize', ['testimonial']);
    }
    /**
     * Edit method
     *
     * @param string|null $id Testimonial id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $testimonial = $this->Testimonials->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $testimonial = $this->Testimonials->patchEntity($testimonial, $this->request->getData());
            if ($this->Testimonials->save($testimonial)) {
                 $this->Flash->success(__('The testimonial has been saved.'));
                 return $this->redirect(['action' => 'index']);
              
            }
            $this->Flash->error(__('The testimonial could not be saved. Please, try again.'));
         
        }
        $services = $this->Testimonials->Services->find('list', ['limit' => 200]);
        $this->set(compact('testimonial', 'services'));
        $this->set('_serialize', ['testimonial']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Testimonial id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->viewBuilder()->setLayout('default1');
        $this->request->allowMethod(['post', 'delete']);
        $testimonial = $this->Testimonials->get($id);
        if ($this->Testimonials->delete($testimonial)) {
            $this->Flash->success(__('The testimonial has been deleted.'));
        } else {
            $this->Flash->error(__('The testimonial could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
 public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout', 'add1']);
}

  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
}
