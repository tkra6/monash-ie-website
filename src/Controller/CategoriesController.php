<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{
  

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
       $this->viewBuilder()->setLayout('default1');

        $this->paginate = [
            'contain' => ['ParentCategories']
        ];
        $categories = $this->paginate($this->Categories);
//         $relatedArticle = $this->Articles->find('list')->select(['name'])
//         ->where(['parent_id !=' => $category['id']])
//         ->where(['rght <' => $category['rght']]);
    
        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    
//     public function index()
//     {   $this->viewBuilder()->setLayout('default1');
     
//         $categories = $this->Categories->find()
//             ->order(['lft' => 'ASC']);
//         $this->set(compact('categories'));
//         $this->set('_serialize', ['categories']);
//     }

    public function moveUp($id = null)
    {
       $this->viewBuilder()->setLayout('default1');

        $this->request->allowMethod(['post', 'put']);
        $category = $this->Categories->get($id);
        if ($this->Categories->moveUp($category)) {
            $this->Flash->success('The category has been moved Up.');

          
        } else {
           $this->Flash->error('The category could not be moved up. Please, try again.');

        }
        return $this->redirect($this->referer(['action' => 'index']));
    }

    public function moveDown($id = null)
    {
        $this->viewBuilder()->setLayout('default1');

        $this->request->allowMethod(['post', 'put']);
        $category = $this->Categories->get($id);
        if ($this->Categories->moveDown($category)) {
            $this->Flash->success('The category has been moved down.');

        } else {
            $this->Flash->error('The category could not be moved down. Please, try again.');
          

        }
        return $this->redirect($this->referer(['action' => 'index']));
    }
  
    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('default1');

        $category = $this->Categories->get($id, [
            'contain' => ['ParentCategories', 'ChildCategories']
        ]);

        $this->set('category', $category);
        $this->set('_serialize', ['category']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
            $this->viewBuilder()->setLayout('default1');

        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));
                 return $this->redirect(['action' => 'index']);
              
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
          
        }
        $parentCategories = $this->Categories->ParentCategories->find('list', ['limit' => 200]);
//       $parentCategories = $this->Categories->find('list', array(
//   'fields'=>'parent_id',
//    'order'=>'Categories.parent_id ASC',
//    'conditions'=> array('Categories.id'=>'1'),
//    'group' => 'parent_id'));
        $this->set(compact('category', 'parentCategories'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
            $this->viewBuilder()->setLayout('default1');

        $category = $this->Categories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));
              return $this->redirect(['action' => 'index']);
               
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
          
        }

      //$parent_id = $this->Categories->ParentCategories->find()->select(['parent_id'])->where(['id' => $id]);
      //$ancestor = $this->Categories->ParentCategories->find('list')->select(['id'])->where([$category['parent_id']])   
      $parentCategories = $this->Categories->ParentCategories->find('list')->select(['name'])
        //->where(['id !=' => $id])
        ->where(['lft <' => $category['lft']]);
        //->where(['parent_id <' => $category['parent_id']])
        //->where(['parent_id !=' => 1]);
      $parentSmall = $this->Categories->ParentCategories->find('list')->select(['name'])
        ->where(['parent_id !=' => $category['id']])
        ->where(['rght <' => $category['rght']]);
        $this->set(compact('category', 'parentCategories', 'parentSmall','relatedArticle'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
  
  
//     public function delete($id = null)
//     {
//          $this->viewBuilder()->setLayout('default1');

//         $this->request->allowMethod(['post', 'delete']);
      
//         $category = $this->Categories->get($id);
// //         $relatedArticle = $this->Categories->ParentCategories->find()
// //         ->where([ $id => $category['parent_id']]);
// //       if ($relatedArticle==null) {
//              $aCategory = $categoriesTable->get(10);
//               $categoriesTable->delete($aCategory);
//         if ($this->Categories->delete($category)) {
           
//             $this->Flash->success(__('The category has been deleted.'));
//         } else {
//             $this->Flash->error(__('The category could not be deleted. Please, try again.'));
//         }

//         return $this->redirect(['action' => 'index']); 
//     }
  
function delete($id=null) {
  if($id==null)
  die("No ID received");
  $this->Categories->id=$id;
  if($this->Categories->removeFromTree($id,true)==false)
     $this->Session->setFlash('The Category could not be deleted.');
   $this->Session->setFlash('Category has been deleted.');
   $this->redirect(array('action'=>'index'));
}
  
 public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout']);
}

  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete','moveDown','moveUp'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }
    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }

    return parent::isAuthorized($user);
}

}
