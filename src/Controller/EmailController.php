<?php
namespace App\Controller;

use App\Controller\AppController;


class EmailController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function instruction($id = null)
    {
             $this->loadModel('Vouchers');

             $voucher = $this->Vouchers->get($id, [
            'contain' => ['Status', 'Prices']
        ]);

        $this->set('voucher', $voucher);
        $this->set('_serialize', ['voucher']);
    }

}
