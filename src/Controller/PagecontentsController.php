<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Pagecontents Controller
 *
 * @property \App\Model\Table\PagecontentsTable $Pagecontents
 */
class PagecontentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
	public function choosePage() {
		 $this->viewBuilder()->setLayout('default1');
	
			  $pagecontents = $this->Pagecontents->find('all');
			
				$pagetables = $this->Pagecontents->find('all')->select(['page'])
				->group('page')->order('id');
				$this->set('pagetable',$pagetables);
			
				$pagelist=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page')->where(['block !=' => '1'])->order('id');
		
				if($this->request->is('post')) {
				 $selected_value = $this->request->data['page'];
		
				$this->Pagecontents->updateAll(
				array('block' => 1),
				array('Pagecontents.page' => $selected_value ));
					      $this->Flash->success('The page has been blocked.');
                return $this->redirect(['action' => 'choose_page']);
			}
		
			
			$thelist=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page')->where(['block !=' => '0'])->order('id');
        $this->set(compact('pagecontents', 'pagetables', 'pagelist', 'thelist'));
        $this->set('_serialize', ['pagecontents', 'pagetables', 'pagelist', 'thelist']);
	}
	
	public function pageconfig($page=null)
  {
      
      $this->viewBuilder()->setLayout('default1'); 
      $this->loadModel('Contents');
    	 
		//	$pageItems = $this->Pagecontents->find('all')->where(['page'=>$page])->order('id');
		        $pageItems = $this->Pagecontents->get(1, [
            'contain' => []
        ]);
// 			$contentItems = $this->Contents->find('all')->where();
      $pageName = $this->Pagecontents->find('list')->select(['page'])->where(['page'=>$page])->first();
        $this->set(compact('pageItems', 'contentItems', 'pageName'));
        $this->set('_serialize', ['pageItems', 'contentItems', 'pageName']);

  }
	
    public function index()
    {
			  $this->viewBuilder()->setLayout('default1');
	
			  $pagecontents = $this->Pagecontents->find('all');
			
				$pagetables = $this->Pagecontents->find('all')->select(['page'])
				->group('page');
				$this->set('pagetable',$pagetables);
			
				$pagelist=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page')->where(['block !=' => '1']);
		
				if($this->request->is('post')) {
				 $selected_value = $this->request->data['page'];
		
				$this->Pagecontents->updateAll(
				array('block' => 1),
				array('Pagecontents.page' => $selected_value ));
					      $this->Flash->success('The page has been blocked.');
                return $this->redirect(['action' => 'index']);
			}
			
			$thelist=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page')->where(['block !=' => '0']); ;
        $this->set(compact('pagecontents', 'pagetables', 'pagelist', 'thelist'));
        $this->set('_serialize', ['pagecontents', 'pagetables', 'pagelist', 'thelist']);
    }
	
	public function unblock() {
		$this->loadModel('Pagecontents');   
		
		$thelist=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page')->where(['block !=' => '0']); ;
		
				if($this->request->is('post'))
			{
				 $selected_value1 = $this->request->data['page'];
					
						
		$this->Pagecontents->updateAll(
    array('block' => 0),
    array('Pagecontents.page' => $selected_value1 ));
					$this->Flash->success('The page has been unblocked.');
					return $this->redirect(['action' => 'index']);

			}
		
        $this->set(compact('thelist'));
        $this->set('_serialize', ['thelist']);
	}

    /**
     * View method
     *
     * @param string|null $id Pagecontent id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
			        $this->viewBuilder()->setLayout('default1');

        $pagecontent = $this->Pagecontents->get($id, [
            'contain' => []
        ]);

        $this->set('pagecontent', $pagecontent);
        $this->set('_serialize', ['pagecontent']);
			
			//mkdir("../pages/hello", 0755);
		}
    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
			  $this->viewBuilder()->setLayout('default1');
			
				$pageOptions=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page');
 			
        $pagecontent = $this->Pagecontents->newEntity();
        if ($this->request->is('post')) {

            $pagecontent = $this->Pagecontents->patchEntity($pagecontent, $this->request->getData());

            if ($this->Pagecontents->save($pagecontent)) {
							$pageName = $pagecontent->page;
							$filename = '../src/Template/Pages/'.$pageName.'.ctp';

							if (file_exists($filename)) {
											
							} else {
											$source = '../src/Template/Pages/CTPtemplate.ctp';
											$destination = '../src/Template/Pages/'.$pageName.'.ctp';

											$data = file_get_contents($source);

											$handle = fopen($destination, "w+");
											fwrite($handle, $data);
											fclose($handle);
							}
							
                 $this->Flash->success(__('The pagecontent has been saved.'));
                 return $this->redirect(['action' => 'index']);
							
            }
            $this->Flash->error(__('The pagecontent could not be saved. Please, try again.'));
					
        }
        $this->set(compact('pagecontent', 'pageOptions'));
        $this->set('_serialize', ['pagecontent', 'pageOptions']);
			
    }
	
    /**
     * Edit method
     *
     * @param string|null $id Pagecontent id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
			
    {
			  $this->viewBuilder()->setLayout('default1');

        $pagecontent = $this->Pagecontents->get($id, [
            'contain' => []
        ]);
			$pageOptions=$this->Pagecontents->find('list',array('fields'=>'page'))->distinct('page');
			
			$contents = $this->Contents->find('all');
				$this->set('contents',$contents);
			
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pagecontent = $this->Pagecontents->patchEntity($pagecontent, $this->request->getData());
            if ($this->Pagecontents->save($pagecontent)) {
                 $this->Flash->success(__('The pagecontent has been saved.'));
                 return $this->redirect(['action' => 'index']);
							
            }
            $this->Flash->error(__('The pagecontent could not be saved. Please, try again.'));
					
        }
        $this->set(compact('pagecontent', 'pageOptions'));
        $this->set('_serialize', ['pagecontent', 'pageOptions']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pagecontent id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
			  $this->viewBuilder()->setLayout('default1');

        $this->request->allowMethod(['post', 'delete']);
        $pagecontent = $this->Pagecontents->get($id);
			  $delPageName = $pagecontent->page;
				$delSection = $pagecontent->section;
				$filename = '../src/Template/Pages/'.$delPageName.'.ctp';
				$thePage = $this->Pagecontents->find('list')->select(['section'])->where(['section !=' => $delSection])->where(['page =' => $delPageName])->toArray();;
				
				$numPage = sizeof($thePage);
				
				$this->set(compact('thePage'));
        $this->set('_serialize', ['thePage']);
				
        if ($this->Pagecontents->delete($pagecontent)) {
					
					if($numPage >= 1) {
						
						$this->Flash->success(__('The pagecontent has been deleted.'));
					}
					elseif ($numPage < 1) {
					unlink($filename);
					$this->Flash->success(__('The pagecontent has been deleted.'));
					}
					
        } else {
            $this->Flash->error(__('The pagecontent could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
	
 public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout']);
}

  public function isAuthorized($user)
{
    $action = $this->request->getParam('action');

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'edit', 'delete', 'unblock', 'choosePage', 'pageconfig'])) {
        return true;
    }
    // All other actions require an id.
    if (!$this->request->getParam('pass.0')) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->getParam('pass.0');
    $user = $this->Users->get($id);
    if ($user->user_id == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}

}
