<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Musclework Massage';
?>
	<!DOCTYPE HTML>
	<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>
			<?= $cakeDescription ?>:
				<?= $this->fetch('title') ?>
					<?php echo $this->Html->meta ( 'favicon.ico', 'http://cdn.sstatic.net/stackoverflow/img/favicon.ico?v=038622610830', array ('type' => 'icon') ); ?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=0.8">
		<i>	
				<?php foreach ($metatype as $metatypes): ?> 
				<meta name = "keywords" content="	<?php echo ($metatypes->metatype);?>">
		    <?php endforeach; ?>
    </i>

		<style>
			.input-group-addon.primary {
				color: rgb(255, 255, 255);
				background-color: rgb(50, 118, 177);
				border-color: rgb(40, 94, 142);
			}

			.input-group-addon.success {
				color: rgb(255, 255, 255);
				background-color: rgb(255, 255, 255);
				border-color: rgb(255, 255, 255);
			}

			.input-group-addon.info {
				color: rgb(255, 255, 255);
				background-color: rgb(57, 179, 215);
				border-color: rgb(38, 154, 188);
			}

			.input-group-addon.warning {
				color: rgb(255, 255, 255);
				background-color: rgb(240, 173, 78);
				border-color: rgb(238, 162, 54);
			}

			.input-group-addon.danger {
				color: rgb(255, 255, 255);
				background-color: rgb(255, 255, 255);
				border-color: rgb(255, 255, 255);
			}
		</style>


		<?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800'); ?>
		<?php echo $this->Html->css('aanimate.css'); ?>
		<?php echo $this->Html->css('aicomoon.css'); ?>
		<?php echo $this->Html->css('abootstrap.css'); ?>
		<?php echo $this->Html->css('amagnific-popup.css'); ?>
		<?php echo $this->Html->css('aowl.carousel.min.css'); ?>
		<?php echo $this->Html->css('aowl.theme.default.min.css'); ?>
		<?php echo $this->Html->css('astyle.css'); ?>
		<?php echo $this->Html->css('menuBar.css'); ?>

		<?php echo $this->Html->script('amodernizr-2.6.2.min.js'); ?>
		<?php echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); ?>
		<!-- Modernizr JS -->
		<!-- FOR IE9 below -->
		<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>

	<body>

		<div class="fh5co-loader"></div>

		<div id="page">
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
						<div id="fh5co-logo"><a class="navbar-brand navbar-brand-left" href="/pages/home"><img src="/img/logoNEW.jpg"</img></a></div>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'home')) ?>">Home</a></li>
							<li><a href="#fh5co-contact">Contact</a></li>
							<?php foreach ($menupage as $menupages): ?>
							<?php if (($menupages->page != "book") && (($menupages->page != "home")) && (($menupages->page != "buyvoucher"))) { ?>

							<li>
								<a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => ($menupages->page))) ?>">
									<?php $pageName = ($menupages->page); 
											echo ucfirst($pageName); ?>
								</a>
							</li>
							
							<?php	} ?>
								<?php endforeach; ?>
							<?php echo $this->Html->link($this->Html->tag('i','',['class' => 'icon-calendar']).' Book Now',['controller' => 'Pages', 'action' => 'book'],
																				 ['class' => 'btn btn-primary btn-outline btn-sm', 'role' => 'button' , 'escape' => false]);?>
								
									<?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-shopping-cart']).' Gift Voucher',['controller' => 'Pages', 'action' => 'buyvoucher'],
																				 ['class' => 'btn btn-primary btn-outline btn-sm', 'role' => 'button' , 'escape' => false]);?>
						</ul>
					</div>
				</div>
			</nav>

			<div id="content">
				<?= $this->Flash->render() ?>
					<div class="row">
						<?= $this->fetch('content') ?>
					</div>


					<div id="fh5co-contact">
						<div class="container">
							<div class="row animate-box">
								<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
									<h2>Contact Us</h2>
								</div>
							</div>
							<div class="row">
								<div class="pricing">
									<div class="col-md-12 text-center animate-box">
										<P>

										<i class="fa fa-phone" style="color:black"> </i>
							<?php foreach ($thePhone as $thePhones): ?>
							<a style="color:black;" href="tel://<?php echo ($thePhones->detail);?>"><?php echo ($thePhones->detail);?></a>
							<?php endforeach; ?><br/>

							<i class="fa fa-map-marker" style="color:black"></i>
							<?php foreach ($theAddress as $theAddresss): ?>
							<?php echo ($theAddresss->detail);?>
							<?php endforeach; ?><br/>
										</P>
										<div class="templatemo-contact-map" id="map"> </div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="thefooter">
						<p>
							<a href="http://www.facebook.com/MuscleworksMassage"><i class="icon-facebook" style="color:white"></i></a>
							<a href="https://www.instagram.com/muscleworks_massage/"><i class="icon-instagram" style="color:white"></i></a>
							<a href="https://plus.google.com/+MuscleworksMassageElsternwick"><i class="icon-google" style="color:white"></i></a> &nbsp;  |  &nbsp;
							<i class="fa fa-phone" style="color:white"> </i>
							<?php foreach ($thePhone as $thePhones): ?>
							<a style="color:white;" href="tel://<?php echo ($thePhones->detail);?>"><?php echo ($thePhones->detail);?></a>
							<?php endforeach; ?><br/>

							<i class="fa fa-map-marker" style="color:white"></i>
							<?php foreach ($theAddress as $theAddresss): ?>
							<?php echo ($theAddresss->detail);?>
							<?php endforeach; ?><br/>
						
						<small class="block"><a style = "color:white;">&copy; 2017 MuscleworksMassage.</a></small>
						<small class="block"><a style = "color:white;" href="/Pages/ts">Terms & Conditions</a><a style = "color:white;" href="<?php echo $this->url->build(array('controller' => 'Users', 'action' => 'login')); ?>" target="_blank"> | Admin</a></small>
						</p>

					</div>

			</div>

			<div class="gototop js-top">
				<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
			</div>

			<!-- google maps integration - *note scripts need to be put into .js file* -->
			<script>
				function initMap() {
					var map = new google.maps.Map(document.getElementById('map'), {
						center: {
							lat: -37.8978565,
							lng: 145.0063526
						},
						zoom: 19
					});

					var infowindow = new google.maps.InfoWindow();
					var service = new google.maps.places.PlacesService(map);

					service.getDetails({
						placeId: 'ChIJ8_N8-_5o1moRlTuJPaZoo_Y'
					}, function(place, status) {
						if (status === google.maps.places.PlacesServiceStatus.OK) {
							var marker = new google.maps.Marker({
								map: map,
								position: place.geometry.location
							});
							google.maps.event.addListener(marker, 'click', function() {
								infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
									'Place ID: ' + place.place_id + '<br>' +
									place.formatted_address + '</div>');
								infowindow.open(map, this);
							});
						}
					});
				}
			</script>

			<!-- google review outer badge* -->
			<!-- 	<div id="wpac-google-review"></div>

	<script type="text/javascript">
		wpac_init = window.wpac_init || [];
		wpac_init.push({
			widget: 'GoogleReview',
			id: 5319,
			place_id: 'ChIJ8_N8-_5o1moRlTuJPaZoo_Y',
			view_mode: 'badge'
		});
		(function() {
			if ('WIDGETPACK_LOADED' in window) return;
			WIDGETPACK_LOADED = true;
			var mc = document.createElement('script');
			mc.type = 'text/javascript';
			mc.async = true;
			mc.src = 'https://embed.widgetpack.com/widget.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(mc, s.nextSibling);
		})();
	</script> -->


			<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV_vd2mthcUArrqn37sgqKrkKL9C18nc8&libraries=places&callback=initMap"></script>
			<!-- jQuery -->
			<?php echo $this->Html->script('ajquery.min.js'); ?>
			<!-- jQuery Easing -->
			<?php echo $this->Html->script('ajquery.easing.1.3.js'); ?>
			<!-- Bootstrap -->
			<?php echo $this->Html->script('abootstrap.min.js'); ?>
			<!-- Waypoints -->
			<?php echo $this->Html->script('ajquery.waypoints.min.js'); ?>
			<!-- Stellar Parallax -->
			<?php echo $this->Html->script('ajquery.stellar.min.js'); ?>
			<!-- Carousel -->
			<?php echo $this->Html->script('aowl.carousel.min.js'); ?>
			<!-- countTo -->
			<?php echo $this->Html->script('ajquery.countTo.js'); ?>
			<!-- Magnific Popup -->
			<?php echo $this->Html->script('ajquery.magnific-popup.min.js'); ?>

			<!-- Main -->
			<?php echo $this->Html->script('amain.js'); ?>


	</body>

	</html>