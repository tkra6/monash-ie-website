<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Musclework Massage';
?>


  <!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>
      <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <!-- BOOTSTRAP STYLES-->
    <!-- FONTAWESOME STYLES-->
    <!-- MORRIS CHART STYLES-->
    <!-- CUSTOM STYLES-->
    <!-- GOOGLE FONTS-->
    <?php echo $this->Html->css('/bs-binary-admin/assets/css/bootstrap.css') ?>
    <?php echo $this->Html->css('/bs-binary-admin/assets/css/font-awesome.css') ?>
    <?php echo $this->Html->css('/bs-binary-admin/assets/js/morris/morris-0.4.3.min.css') ?>
    <?php echo $this->Html->css('/bs-binary-admin/assets/css/custom.css') ?>
    <?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans') ?>
    
  </head>

  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
          <a class="navbar-brand" href="<?php echo $this->url->build(array('controller' => 'users', 'action' => 'index')) ?>"></a>
        </div>
        <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">
          <a class="btn btn-danger square-btn-adjust" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>">Back</a>
        </div>
      </nav>





      <div id="page-wrapper">
        <?= $this->Flash->render() ?>

          <div class="row">
            <?= $this->fetch('content') ?>
          </div>
      </div>

    </div>


    <!-- /. NAV SIDE  -->


    <!-- BOOTSTRAP SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    <!-- CUSTOM SCRIPTS -->

    <?php echo $this->Html->script(array('/bs-binary-admin/assets/js/jquery-1.10.2.js', '/bs-binary-admin/assets/js/bootstrap.min.js', '/bs-binary-admin/assets/js/jquery.metisMenu.js', '/bs-binary-admin/assets/js/custom.js')); ?>

  </body>

  </html>