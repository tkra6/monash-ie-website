<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Musclework Masasge';
?>


<!DOCTYPE html>
<html lang="en" <!-- <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--<link rel="shortcut icdd" href="PUT YOUR FAVICON HERE">-->
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
<!-- Google Web Font Embed -->

<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'); ?>
<!-- Bootstrap core CSS <link href="css/bootstrap.css" rel='stylesheet' type='text/css'> -->
<?php echo $this->Html->css('bootstrap.css'); ?>


<!-- Custom styles for this template -->

<?php echo $this->Html->css('templatemo_style.css'); ?>


</head>

<body>

	<div class="templatemo-top-bar" id="templatemo-top">
		<div class="container">
			<div class="subheader">
				<div id="phone" class="pull-left">
					<img alt="icon 1" <?php echo $this->Html->image('location.png'); ?> 135 Gardenvale Road, Gardenvale Victoria 3185 <img/>
				</div>
				
				<div id="phone" class="pull-right">
					<a href="http://www.facebook.com/MuscleworksMassage" target="_blank"><img alt="facebook" <?php echo $this->Html->image('004-facebook-logo-button.png'); ?> <img/></a>
					<a href="https://www.instagram.com/explore/locations/1017106370/" target="_blank"><img alt="instagram" <?php echo $this->Html->image('005-instagram-logo.png'); ?> <img/></a>
					<a href="https://twitter.com/muscleworksm" target="_blank"><img alt="twitter" <?php echo $this->Html->image('003-twitter-logo-button.png'); ?> <img/></a>
				</div>
			</div>
		</div>
	</div>
	<div class="templatemo-top-menu">
		<div class="container">
			<!-- Static navbar -->
			<div class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
				  <a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'home1')) ?>" class="navbar-brand"><img alt="Musclework Massage" title="Musclework Massage" height="100px" height="400px" <?php echo $this->Html->image('muscleWlogo.jpg'); ?><img/></a>
					</div>
					<div class="navbar-collapse collapse" >
						<ul class="nav nav-justified" style="margin-top: 40px;">
							<li><a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'home1')) ?>">HOME</a></li>
							<li><a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'services')) ?>">SERVICE</a></li>
							<li><a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'home1#templatemo-teaminfo')) ?>">TEAM</a></li>
							<li><a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'home1#templatemo-portfolio')) ?>">PORTFOLIO</a></li>
							<li><a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'articles', 'action' => 'blog')) ?>">BLOG</a></li>
							<li><a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'testimonials', 'action' => 'add1')) ?>">TESTIMONAL</a></li>
							<li><a style= "color:black;" href="#templatemo-contact">CONTACT</a></li>
							
			<li>	<a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>" class="btn btn-sm btn-green" role="button">Book Now</a> </li>
			<li>	<a style= "color:black;" href="<?php echo $this->url->build(array('controller' => 'vouchers', 'action' => 'prices')) ?>" class="btn btn-sm btn-grey" role="button">Buy Now</a> </li>

						</ul>
						 
						
					</div>
					<!--/.nav-collapse -->
				</div>
				<!--/.container-fluid -->
			</div>
			<!--/.navbar -->
		</div>
		<!-- /container -->
	</div>

	

  
  <div id="content">
            <?= $this->Flash->render() ?>

            <div class="row">
                <?= $this->fetch('content') ?>
            </div>
    	</div>
	

	<div id="templatemo-contact">
		<div class="container">
			<div class="row">
				<div class="templatemo-line-header head_contact">
					<div class="text-center">
						<hr class="team_hr team_hr_left hr_gray" /><span class="txt_darkgrey">CONTACT US</span>
						<hr class="team_hr team_hr_right hr_gray" />
					</div>
				</div>

				<div class="col-md-8">
					<div class="templatemo-contact-map" id="map"> </div>
					<div class="clearfix"></div>
					<i>You can find us on 135 Gardenvale Road, Gardenvale, Victoria 3185</i>
				</div>
				<div class="col-md-4 contact_right">

					<p>We are here to help you:</p>
					<p><img alt="icon 1" <?php echo $this->Html->image('location.png'); ?> <img/> 135 Gardenvale Road, Gardenvale Victoria 3185</p>

					<p><img alt="icon 2" <?php echo $this->Html->image('phone1.png'); ?> <img/> (03) 9530 6400</p>

				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /#templatemo-contact -->


	<div class="templatemo-tweets">
		<div class="container">
			<div class="row" style="margin-top:20px;">
				<div class="col-md-2">
				</div>
				<div class="col-md-1">
					<img alt="icon" <?php echo $this->Html->image('quote.png'); ?> <img/>
				</div>
				<div class="col-md-7 tweet_txt">
					<span>I take a massage each week. This isn't an indulgence, it's an investment in your full creative expression/productivity/passion and sustained good health.</span>
					<br/>
					<span class="twitter_user">Robin S. Sharma</span>
				</div>
				<div class="col-md-2">
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</div>

	<div class="templatemo-partners">
		<div class="container">
			<div class="row">


				<div class="templatemo-line-header">
					<div class="text-center">
						<hr class="team_hr team_hr_left hr_gray" /><span class="txt_darkgrey">OUR PARTNERS</span>
						<hr class="team_hr team_hr_right hr_gray" />
					</div>
				</div>
				<div class="clearfix"></div>


				<div class="text-center">

					<div style="margin-top:60px;">
						<ul class="list-inline">
							<li class="col-sm-2 col-md-2 templatemo-partner-item">
								<a href="#"><img class="img-responsive" alt="partner 1" <?php echo $this->Html->image('p1.gif'); ?> <img/></a>
							</li>
							<li class="col-sm-2 col-md-2 templatemo-partner-item">
								<a href="#"><img class="img-responsive" alt="partner 2" <?php echo $this->Html->image('p2.jpg'); ?> <img/></a>
							</li>
							<li class="col-sm-2 col-md-2 templatemo-partner-item">
								<a href="#"><img class="img-responsive" alt="partner 3" <?php echo $this->Html->image('p3.jpg'); ?> <img/></a>
							</li>
							<li class="col-sm-2 col-md-2 templatemo-partner-item">
								<a href="#"><img class="img-responsive" alt="partner 4" <?php echo $this->Html->image('partner4.jpg'); ?> <img/></a>
							</li>
							<li class="col-sm-2 col-md-2 templatemo-partner-item">
								<a href="#"><img class="img-responsive" alt="partner 5" <?php echo $this->Html->image('partner5.jpg'); ?> <img/></a>
							</li>
							<li class="col-sm-2 col-md-2 templatemo-partner-item">
								<a href="#"><img class="img-responsive" alt="partner 6" <?php echo $this->Html->image('partner6.jpg'); ?> <img/></a>
							</li>
						</ul>

					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="templatemo-footer">
		<div class="container">
			<div class="row">
				<div class="text-center">

					<div class="footer_container">
						<ul class="list-inline">
							<li>
								<a rel="nofollow" href="http://www.facebook.com/MuscleworksMassage"><span class="social-icon-fb"></span></a>
							</li>
							<li>
								<a rel="nofollow" href="https://www.instagram.com/explore/locations/1017106370/"><span class="social-icon-rss"></span></a	
                                </li>
                                <li>
																	<a rel="nofollow" href="https://twitter.com/muscleworksm"><span class="social-icon-twitter"></span></a>
								</a>
							</li>
						</ul>
						<div class="height30"></div>
						<a class="btn btn-lg btn-orange" href="#" role="button" id="btn-back-to-top">Back To Top</a>
						<div class="height30"></div>
					</div>
					<div class="footer_bottom_content">
						<span id="footer-line">Copyright © 2017 <a href="#">Muscleworks Massage | </a> <a href=<?php echo $this->url->build(array('controller' => 'users', 'action' => 'login')); ?>> Admin</a></span>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- google maps integration - *note scripts need to be put into .js file* -->
	<script>
		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				center: {
					lat: -37.8978565,
					lng: 145.0063526
				},
				zoom: 19
			});

			var infowindow = new google.maps.InfoWindow();
			var service = new google.maps.places.PlacesService(map);

			service.getDetails({
				placeId: 'ChIJ8_N8-_5o1moRlTuJPaZoo_Y'
			}, function(place, status) {
				if (status === google.maps.places.PlacesServiceStatus.OK) {
					var marker = new google.maps.Marker({
						map: map,
						position: place.geometry.location
					});
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
							'Place ID: ' + place.place_id + '<br>' +
							place.formatted_address + '</div>');
						infowindow.open(map, this);
					});
				}
			});
		}
	</script>

	<!-- google review outer badge* -->
	<div id="wpac-google-review"></div>

	<script type="text/javascript">
		wpac_init = window.wpac_init || [];
		wpac_init.push({
			widget: 'GoogleReview',
			id: 5319,
			place_id: 'ChIJ8_N8-_5o1moRlTuJPaZoo_Y',
			view_mode: 'badge'
		});
		(function() {
			if ('WIDGETPACK_LOADED' in window) return;
			WIDGETPACK_LOADED = true;
			var mc = document.createElement('script');
			mc.type = 'text/javascript';
			mc.async = true;
			mc.src = 'https://embed.widgetpack.com/widget.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(mc, s.nextSibling);
		})();
	</script>

<div id="wpac-comment"></div>
<script type="text/javascript">
wpac_init = window.wpac_init || [];
wpac_init.push({widget: 'Comment', id: 5319, view_mode: 'list'});
(function() {
    if ('WIDGETPACK_LOADED' in window) return;
    WIDGETPACK_LOADED = true;
    var mc = document.createElement('script');
    mc.type = 'text/javascript';
    mc.async = true;
    mc.src = 'https://embed.widgetpack.com/widget.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
})();
</script>


	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV_vd2mthcUArrqn37sgqKrkKL9C18nc8&libraries=places&callback=initMap"></script>

	<?php echo $this->Html->script(array('jquery.min.js','bootstrap.min.js', 'stickUp.min.js', 'colorbox/jquery.colorbox-min.js', 'templatemo_script.js')); ?>

	<!-- templatemo 395 urbanic -->
</body>

</html>
