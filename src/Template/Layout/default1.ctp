<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Musclework Massage - Admin';
?>


  <!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>
      <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <!-- BOOTSTRAP STYLES-->
    <!-- FONTAWESOME STYLES-->
    <!-- MORRIS CHART STYLES-->
    <!-- CUSTOM STYLES-->
    <!-- GOOGLE FONTS-->
    <?php echo $this->Html->css('/bs-binary-admin/assets/css/bootstrap.css') ?>
    <?php echo $this->Html->css('/bs-binary-admin/assets/css/font-awesome.css') ?>
    <?php echo $this->Html->css('/bs-binary-admin/assets/js/morris/morris-0.4.3.min.css') ?>
    <?php echo $this->Html->css('/bs-binary-admin/assets/css/custom.css') ?>
    <?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans') ?>
    
    
  </head>

  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
          <a class="navbar-brand" href="<?php echo $this->url->build(array('controller' => 'users', 'action' => 'index')) ?>">Admin</a>
        </div>
        <div style="color: white;
        padding: 15px 50px 5px 50px;
        float: right;
        font-size: 16px;"> User Access : <?php echo ($this->request->session()->read('Auth.User.first_name')) . " " . ($this->request->session()->read('Auth.User.last_name')); ?> &nbsp; 
          <a href="<?php echo $this->url->build(array('controller' => 'users', 'action' => 'logout')) ?>" class="btn btn-danger square-btn-adjust">Logout</a> </div>

      </nav>


      <!-- /. NAV TOP  -->
      <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
          <ul class="nav" id="main-menu">
            <li>
              <a href="<?php echo $this->url->build(array('controller' => 'clients', 'action' => 'index')) ?>"><i class="fa fa-star fa-3x"></i> Clients</a>
            </li>  
            
             <li>
              <a href="<?php echo $this->url->build(array('controller' => 'users', 'action' => 'index')) ?>"><i class="fa fa-users fa-3x"></i> Users </a>
            </li>
            
            <li>
              <a href="<?php echo $this->url->build(array('controller' => 'pagecontents', 'action' => 'index')) ?>"><i class="fa fa-pencil fa-3x"></i> Manage Page & Content <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'pagecontents', 'action' => 'index')) ?>">Pages</a>
                </li>
                <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'contents', 'action' => 'index')) ?>">Content</a>
                </li>
              </ul>
            </li>
            
            <li>
              <a><i class="fa fa-laptop fa-3x"></i> Manage Blog <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'articles', 'action' => 'index')) ?>">Articles</a>
                </li>
                  <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'categories', 'action' => 'index')) ?>">Categories</a>
                </li>
              </ul>
            </li>           

            <li>
              <a href="<?php echo $this->url->build(array('controller' => 'services', 'action' => 'index')) ?>"><i class="fa fa-sitemap fa-3x"></i> Services & Prices <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'services', 'action' => 'index')) ?>">Services</a>
                </li>
              </ul>
               <ul class="nav nav-second-level">
                <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'prices', 'action' => 'index')) ?>">Prices</a>
                </li>
              </ul>
                <ul class="nav nav-second-level">
                <li>
                  <a href="<?php echo $this->url->build(array('controller' => 'pricetypes', 'action' => 'index')) ?>">Prices Type (Package)</a>
                </li>
              </ul>
            </li>
            



            <li>
              <a href="<?php echo $this->url->build(array('controller' => 'testimonials', 'action' => 'index')) ?>"><i class="fa fa-edit fa-3x"></i> Testimonials </a>
            </li>


            <li>
              <a href="<?php echo $this->url->build(array('controller' => 'vouchers', 'action' => 'index')) ?>"><i class="fa fa-gift fa-3x"></i> Vouchers </a>
            </li>
            
            <li>
              <a href="<?php echo $this->url->build(array('controller' => 'archives', 'action' => 'index')) ?>"><i class="fa fa-gift fa-3x"></i> Archives </a>
            </li>

          </ul>
        </div>
      </nav>


      <div id="page-wrapper">
        <?= $this->Flash->render() ?>

          <div class="row">
            <?= $this->fetch('content') ?>
          </div>
      </div>

    </div>


    <!-- /. NAV SIDE  -->


    <!-- BOOTSTRAP SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    <!-- CUSTOM SCRIPTS -->

    <?php echo $this->Html->script(array('/bs-binary-admin/assets/js/jquery-1.10.2.js', '/bs-binary-admin/assets/js/bootstrap.min.js', '/bs-binary-admin/assets/js/jquery.metisMenu.js', '/bs-binary-admin/assets/js/custom.js')); ?>


  </body>

  </html>