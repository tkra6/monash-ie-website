<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
include_once("analyticstracking.php") ?>
		<?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800'); ?>
		<?php echo $this->Html->css('aanimate.css'); ?>
		<?php echo $this->Html->css('aicomoon.css'); ?>
		<?php echo $this->Html->css('abootstrap.css'); ?>
		<?php echo $this->Html->css('amagnific-popup.css'); ?>
		<?php echo $this->Html->css('aowl.carousel.min.css'); ?>
		<?php echo $this->Html->css('aowl.theme.default.min.css'); ?>
		<?php echo $this->Html->css('astyle.css'); ?>

		<?php echo $this->Html->script('amodernizr-2.6.2.min.js'); ?>


<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body> <div id="fh5co-schedule" class="fh5co-bg" style="background-image: url(/img/Orange-Wallpaper.png);">
   
  
   <div class="col align-self-center">
     <center><h1> ERROR 404</h1></center>
    </div>
     <div class="col align-self-center">
     <center><h2> Oops... this page doesn't exist. Please go back</h2></center>
    </div>
    <center><div><img style="width:800px;height:200px" src="/img/banner.jpg"</img></div></center>
        <div id="footer"><center>
            <h1>
              
          <?= $this->Html->link(__('Back'), 'javascript:history.back()') ?></h1></center>
        </div>
    </div>
	</div>
</body>
</html>
