  <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
          <h3><?= __('Services') ?></h3>
          </div>
        </div>
        <hr />
  <div class="panel panel-default">
        <div class="panel-heading">
    <?= h($service->name) ?>
        </div>

    <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($service->name) ?></td>
        </tr>
    </table>
</div>
