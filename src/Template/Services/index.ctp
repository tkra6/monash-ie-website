 <div id="page-inner">
  <div class="row">
    <div class="col-md-12">
     <h3><?= __('Services') ?></h3>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Services
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Services', 'action' => 'add']); ?></a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                  
                  <thead>
                    <tr>

                      <th>
                        <?php echo "Name";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                   <tbody>
            <?php foreach ($services as $service): ?>
            <tr>
                <td><?= h($service->name) ?></td>
                <td class="actions">
                          <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Services', 'action' => 'edit', $service->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>

                          <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                          array('controller' => 'Services', 'action' => 'delete',$service->id), 
                          array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete Service: '. $service->name)));
                          ?>

                </td>
            </tr>
            <?php endforeach; ?>
            <?php echo $this->fetch('postLink');?>

        </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
        </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>

