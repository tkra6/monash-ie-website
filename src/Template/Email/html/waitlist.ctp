This message is automated. The following customer has requested a position in the wait list:<br><br>
CONTACT DETAILS:<br>
First Name: <?php echo $firstName ?><br>

Last Name: <?php echo $lastName ?><br>

Email Address: <?php echo $email ?><br><br>
APPOINTMENT PREFERENCES:<br>

Appointment Type: <?php echo $appType ?><br>

Practitioner: <?php echo $prac ?><br>

Monday: <?php echo $Monday ?><br>

Tuesday: <?php echo $Tuesday ?><br>

Wednesday: <?php echo $Wednesday ?><br>

Thursday: <?php echo $Thursday ?><br>

Friday: <?php echo $Friday ?><br>

Saturday: <?php echo $Saturday ?><br>

Urgent Appointment: <?php echo $urgentAppointment ?><br>

Only Outside Business Hours: <?php echo $onlyOutsideBusinessHours ?><br>

Additional Requests: <?php echo $additionalRequests ?><br>