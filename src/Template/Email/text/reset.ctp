<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Email</title>
	
	    <?php echo $this->Html->css('email.css'); ?>
<?php echo $this->Html->css('email/email.css'); ?>

</head>
 
<body bgcolor="#FFFFFF">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#999999">
	<tr>
		<td></td>
		<td class="header container">
			
				<div class="content">
					<table bgcolor="#999999">
					<tr>
						<td><img <?php echo $this->Html->image('banner.jpg'); ?></img></td>
						<td align="right"><h6 class="collapse">MuscleworksMassage</h6></td>
					</tr>
				</table>
				</div>
				
		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<div class="content">
			<table>
				<tr>
					<td>
						
						<h3>Dear Customer</h3>
						<p class="lead">Thanks for your recent booking with Muscleworks Massage, your purchase has been processed, here are your voucher details:<br>Please bring this voucher in store to redeem.</p>

						
						<h3>Your Voucher Details:</h3>
						<p>
							<b>Voucher Unique Code: </b><?php echo $uuid?><br/>
							<b>Recipient: </b><?php echo $toName?><br/>
							<b>From: </b><?php echo $fromName?><br/>
							<b>Voucher Price Value: </b><?php echo $price?><br/>
							<b>Expiry Date: </b><?php echo $expiry?><br/>
							<b>Message: </b><?php echo $message?><br/>
						</p>

												
						<br/>
						<br/>							
												
						<!-- social & contact -->
						<table class="social" width="100%">
							<tr>
								<td>
									
									<!--- column 1 -->
									<table align="left" class="column">
										<tr>
											<td>				
												
												<h5 class="">Connect with Us:</h5>
												<p class=""><a href="http://www.facebook.com/MuscleworksMassage" class="soc-btn fb">Facebook</a> <a href="https://twitter.com/muscleworksm" class="soc-btn tw">Twitter</a> <a href="https://www.instagram.com/explore/locations/1017106370/" class="soc-btn gp">Instagram</a></p>
						
												
											</td>
										</tr>
									</table><!-- /column 1 -->	
									
									<!--- column 2 -->
									<table align="left" class="column">
										<tr>
											<td>				
																			
												<h5 class="">Contact Info:</h5>												
												<p>Phone: <strong>(03) 9530 6400</strong><br/>
                Email: <strong><a href="emailto:info@muscleworksmassage.com.au">info@muscleworksmassage.com.au</a></strong></p>
                
											</td>
										</tr>
									</table><!-- /column 2 -->
									
									<span class="clear"></span>	
									
								</td>
							</tr>
						</table><!-- /social & contact -->
					
					
					</td>
				</tr>
			</table>
			</div>
									
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
			
				<!-- content -->
				<div class="content">
				<table>
				<tr>
					<td align="center">
						<p>
							<a href="#">Terms</a> |
							<a href="#">Privacy</a> |
							
						</p>
					</td>
				</tr>
			</table>
				</div><!-- /content -->
				
		</td>
		<td></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>