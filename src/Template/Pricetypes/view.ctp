<div id="page-inner">
          <div class="row">
          <div class="col-md-12">
           <h2><?= __('Price Types') ?></h2>
          </div>
        </div>
        <hr />
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
    <h3><?= h($pricetype->name) ?></h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
             
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($pricetype->name) ?></td>
        </tr>
    </table>
</div>