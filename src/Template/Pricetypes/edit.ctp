<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div id="page-inner">
  <div class="form-group">
<div class="pricetypes form large-9 medium-8 columns content">
    <?= $this->Form->create($pricetype) ?>
    <fieldset>
        <legend><?= __('Edit Pricetype') ?></legend>
        <?php
            echo $this->Form->control('name', array('class'=>'form-control'));
        ?> <br/>
    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
