
<div id="page-inner">
<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>

<div class="articles form large-9 medium-8 columns content">
    <?= $this->Form->create($article) ?>
    <fieldset>
        <legend><?= __('Edit Article') ?></legend>
          <?php  echo $this->Form->control('title', array('class'=>'form-control'));  ?> <br/>
          <?php  echo $this->Form->control('body', array('class'=>'ckeditor','type'=>'textarea', 'name'=> 'body'));  ?> <br/>
          <?php  echo $this->Form->control('categories_id', array('class'=>'form-control','options' => $category));  ?> <br/>
          <?php  echo $this->Form->control('users_id', array('type'=>'text', 'disabled' => 'disabled','class'=>'form-control', 'value'=> $article->user->first_name));  ?> <br/>
    </fieldset>
   
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>

    <?= $this->Form->end() ?>
</div>
<!--   <script>
    CKEDITOR.replace( 'body', {
   config.filebrowserBrowseUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload.php?type=flash';
} );
  
    
  </script> -->
