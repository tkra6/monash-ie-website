<div id="page-inner">
          <div class="row">
          <div class="col-md-12">
            <h3><?= __('Article') ?></h3> 
          </div>
        </div>
  <hr/>
  <div class="panel panel-default">
        <div class="panel-heading">
    <?= h($article->title) ?>
        </div>

    <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($article->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categories Id') ?></th>
            <td><?= $this->Number->format($article->categories_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Users Id') ?></th>
            <td><?= $this->Number->format($article->users_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($article->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($article->modified) ?></td>
        </tr>
    </table>
    <div class="row">
            <div class="col-md-12">
        <h4><?= __('Body') ?></h4>
        <?= $this->Text->autoParagraph(h($article->body)); ?>
      </div>
    </div>
</div>
