
<div id="page-inner">
<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>

<div class="articles form large-9 medium-8 columns content">
    <?= $this->Form->create($article) ?>
        <legend><?= __('Add Article') ?></legend>
          <?php  echo $this->Form->control('title', array('class'=>'form-control'));  ?> <br/>
          <?php  echo $this->Form->control('body', array('class'=>'ckeditor','type'=>'textarea', 'name'=> 'body'));  ?> <br/>
          <?php  echo $this->Form->control('categories_id', array('class'=>'form-control','options' => $category));  ?> <br/>
          <?php  echo $this->Form->control('users_id', array('type'=>'text', 'class'=>'form-control', 'value'=> $user['id'], 'type' => 'hidden'));  ?> <br/>    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>


</div>
  </div>
