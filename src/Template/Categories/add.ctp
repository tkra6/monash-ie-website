<div id="page-inner">
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category) ?>
    <fieldset>
        <legend><?= __('Add Category') ?></legend>

          <?php echo $this->Form->control('parent_id', array('class'=>'form-control','options' => $parentCategories, 'empty'=>'No parent category')); ?> <br/>
          <?php  echo $this->Form->control('name', array('class'=>'form-control')); ?> <br/>
          <?php echo $this->Form->control('description', array('class'=>'form-control')); ?> <br/>

    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
</div>
