<div id="page-inner">
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category) ?>
    <fieldset>
        <legend><?= __('Edit Category') ?></legend>
      
                <?php if (($category['parent_id'] == 0 ) && ($category['lft'] == 1)) {
                       echo $this->Form->control('parent_id', array('class'=>'form-control','options' => $parentCategories));
                       echo "You can't change its parent because this is the ancestor"; } elseif (($category['parent_id'] == 0) && ($category['lft'] != 1)) { ?> <br/>
      
          <?php echo $this->Form->control('parent_id', array('class'=>'form-control','options' => $parentSmall)); } else {
                echo $this->Form->control('parent_id', array('class'=>'form-control','options' => $parentCategories)); } ?>
                       
      
          <br/><?php echo $this->Form->control('name', array('class'=>'form-control')); ?> <br/>
          <?php echo $this->Form->control('description', array('class'=>'form-control')); ?> <br/>

    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
</div>