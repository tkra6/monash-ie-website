<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3><?= __('Article') ?></h3>
    </div>
  </div>
  <hr/>
  <div class="panel panel-default">
    <div class="panel-heading">
      <?= h($category->name) ?>
    </div>

    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <tr>
            <th scope="row">
              <?= __('Parent Category') ?>
            </th>
            <td>
              <?= $category->has('parent_category') ? $this->Html->link($category->parent_category->name, ['controller' => 'Categories', 'action' => 'view', $category->parent_category->id]) : '' ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Name') ?>
            </th>
            <td>
              <?= h($category->name) ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Description') ?>
            </th>
            <td>
              <?= h($category->description) ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Id') ?>
            </th>
            <td>
              <?= $this->Number->format($category->id) ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Lft') ?>
            </th>
            <td>
              <?= $this->Number->format($category->lft) ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Rght') ?>
            </th>
            <td>
              <?= $this->Number->format($category->rght) ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Created') ?>
            </th>
            <td>
              <?= h($category->created) ?>
            </td>
          </tr>
          <tr>
            <th scope="row">
              <?= __('Modified') ?>
            </th>
            <td>
              <?= h($category->modified) ?>
            </td>
          </tr>
        </table>
      
        <div class="row">
          <div class="col-md-12">
            <h3><?= __('Related') ?></h3> 
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading">
            <?= __('Related Categories') ?>
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              <?php if (!empty($category->child_categories)): ?>
              <table class="table table-striped table-bordered table-hover">

                <tr>
                  <th scope="col">
                    <?= __('Id') ?>
                  </th>
                  <th scope="col">
                    <?= __('Parent Id') ?>
                  </th>
                  <th scope="col">
                    <?= __('Lft') ?>
                  </th>
                  <th scope="col">
                    <?= __('Rght') ?>
                  </th>
                  <th scope="col">
                    <?= __('Name') ?>
                  </th>
                  <th scope="col">
                    <?= __('Description') ?>
                  </th>
                  <th scope="col">
                    <?= __('Created') ?>
                  </th>
                  <th scope="col">
                    <?= __('Modified') ?>
                  </th>
                  <th scope="col" class="actions">
                    <?= __('Actions') ?>
                  </th>
                </tr>
                <?php foreach ($category->child_categories as $childCategories): ?>
                <tr>
                  <td>
                    <?= h($childCategories->id) ?>
                  </td>
                  <td>
                    <?= h($childCategories->parent_id) ?>
                  </td>
                  <td>
                    <?= h($childCategories->lft) ?>
                  </td>
                  <td>
                    <?= h($childCategories->rght) ?>
                  </td>
                  <td>
                    <?= h($childCategories->name) ?>
                  </td>
                  <td>
                    <?= h($childCategories->description) ?>
                  </td>
                  <td>
                    <?= h($childCategories->created) ?>
                  </td>
                  <td>
                    <?= h($childCategories->modified) ?>
                  </td>
                  <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Categories', 'action' => 'view', $childCategories->id]) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Categories', 'action' => 'edit', $childCategories->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Categories', 'action' => 'delete', $childCategories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCategories->id)]) ?>
                  </td>
                </tr>
                <?php endforeach; ?>
              </table>
              <?php endif; ?>
            </div>
          </div>