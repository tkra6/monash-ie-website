      <div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Categories') ?>
      </h3>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Categories
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Categories', 'action' => 'add']); ?></a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                  
                  <thead>
                    <tr>

                      <th>
                        <?php echo "Parent_id";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Lft"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Rght"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Name"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Description"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
<!--                       <th>
                        <?php //echo "Created"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php //echo "Modified"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th> -->
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= $category->has('parent_category') ? $this->Html->link($category->parent_category->name, ['controller' => 'Categories', 'action' => 'view', $category->parent_category->id]) : '' ?></td>
                <td><?= $category->lft ?></td>
                <td><?= $category->rght ?></td>  
                <td><?= h($category->name) ?></td>
                <td><?= h($category->description) ?></td>
                <td class="actions">
                 <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Categories', 'action' => 'edit', $category->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>

                    <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                    array('controller' => 'Categories', 'action' => 'delete',$category->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete Category Name: '. $category->name)));
                    ?>
                  
                    <?php echo $this->Form->postLink('<i class="glyphicon glyphicon-chevron-up"></i> ' . __('Move Up'), 
                    array('controller' => 'Categories', 'action' => 'moveUp',$category->id), 
                    array('class' => 'btn btn-primary', 'escape'=>false, 'confirm' =>('Are you sure you want to move up category name:{0}?'. $category->name)));
                    ?>
                  
                    <?php echo $this->Form->postLink('<i class="glyphicon glyphicon-chevron-down"></i> ' . __('Move Down'), 
                    array('controller' => 'Categories', 'action' => 'moveDown',$category->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to move down category name:{0}?'. $category->name)));
                    ?>
                  
                </td>    

                    </tr>
                    <?php endforeach; ?>
                    <?php echo $this->fetch('postLink');?>

                  </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
        </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>
