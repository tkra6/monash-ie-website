<div id="page-inner">
        <div class="row">
          <div class="col-md-12">
          <h3><?= __('Status') ?></h3>
          </div>
        </div>
        <hr />
<div class="panel panel-default">
        <div class="panel-heading">
    <h3><?= h($status->name) ?></h3>
  </div>
    
    <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
<!--         <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?php //echo ($status->name) ?></td>
        </tr> -->
    </table>
            
    <div class="related">
        <h4><?= __('Related Vouchers') ?></h4>
        <?php if (!empty($status->vouchers)): ?>
      <table class="table table-striped table-bordered table-hover">
       
            <tr>
                <th scope="col"><?= __('From Name') ?></th>
                <th scope="col"><?= __('From Email') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('To Name') ?></th>
                <th scope="col"><?= __('Date Purchase') ?></th>
                <th scope="col"><?= __('Expiry') ?></th>
                <th scope="col"><?= __('Payment Status') ?></th>
                <th scope="col"><?= __('Status') ?></th>
            </tr>
            <?php foreach ($status->vouchers as $vouchers): ?>
            <tr>
              <td><?= h($vouchers->fromName) ?></td>
                        <td><?= h($vouchers->fromEmail) ?></td>
                        <td><?= h($vouchers->phoneNumber) ?></td>
                        <td><?= h($vouchers->toName) ?></td>
                        <td><?= h($vouchers->datePurchase) ?></td>
                        <td><?= h($vouchers->expiry) ?></td>
                        <td><?php if(($vouchers->paymentStatus) == 0) { echo "Upaid"; } else { echo "Paid"; } ?> </td>              
                        <td><?= h($status->name)?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>