<?php echo $this->Html->script(array('/bs-binary-admin/assets/js/jquery-1.10.2.js', '/bs-binary-admin/assets/js/bootstrap.min.js', '/bs-binary-admin/assets/js/jquery.metisMenu.js', '/bs-binary-admin/assets/js/custom.js')); ?>

<?php echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js'); ?>

<div class="container">
  <h2>Carousel Example</h2>  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
				<img alt="Los Angeles" style="width:1200px; height:400px;" <?php echo $this->Html->image('jogging1.jpg'); ?></img>
				<div class="carousel-caption">
				<h3>Los Angeles</h3>
          <p>LA is always so much fun!</p>
				</div>
      </div>
				
			<div class="item">
        <img alt="Chicago" style="width:1200px; height:400px;" <?php echo $this->Html->image('jogging1.jpg'); ?></img>
        <div class="carousel-caption">
          <h3>Chicago</h3>
          <p>Thank you, Chicago!</p>
        </div>
      </div>
				
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>



