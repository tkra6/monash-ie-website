<?php 
$type = array('Promotion' => 'Promotion', 'Team'=> 'Team', 'Partner'=> 'Partner' ,'Service'=> 'Service', 'Main Slideshow' => 'Main Slideshow');
?>
<div id="page-inner">
  <?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<div class="prices form large-9 medium-8 columns content">
    <?= $this->Form->create($content) ?>
    <fieldset>
        <legend><?= __('Add Content') ?></legend>
        
          <?php  echo $this->Form->control('type', array('class'=>'form-control', 'options' => $type, 'id'=>'notifications-dropdown'));?><br/>
          <?php  echo $this->Form->control('main', array('class'=>'form-control', 'type'=>'textarea'));?><br/>
          <?php  echo $this->Form->control('detail', array('class'=>'ckeditor', 'type'=>'textarea', 'name'=> 'detail'));?><br/>
          <?php  echo $this->Form->control('image', array('class'=>'ckeditor', 'type'=>'textarea', 'name'=> 'image'));?><br/>
        
    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
</div>

<script>
$('#notifications-dropdown').change(function() {
  if ($(this).val() == 'Promotion') {
    //alert($(this).val());
  }
    //alert($(this).val());
  });
</script>
        