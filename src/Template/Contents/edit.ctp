<?php
$thetype = $content->type;

$type = array('Promotion' => 'Promotion', 'Team'=> 'Team', 'Partner'=> 'Partner','Service'=> 'Service', 'Main Slideshow' => 'Main Slideshow');

?>
<div id="page-inner">
<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>

<div class="prices form large-9 medium-8 columns content">
    <?= $this->Form->create($content) ?>
    <fieldset>
        <legend><?= __('Edit Content') ?></legend>
      
          <?php if (($thetype == "Address") || ($thetype == "Phone Number") || ($thetype == "Quote")) { ?>
          <?php  echo $this->Form->control('type', array('class'=>'form-control','disabled' => 'disabled')); } 
          else { ?><br/>
          <?php  echo $this->Form->control('type', array('class'=>'form-control', 'options' => $type)); }?><br/>
          <?php  echo $this->Form->control('main', array('class'=>'form-control'));?><br/>
          <?php  echo $this->Form->control('detail', array('class'=>'ckeditor', 'type'=>'textarea', 'name'=> 'detail'));?><br/>
          <?php  echo $this->Form->control('image', array('class'=>'ckeditor', 'type'=>'textarea', 'name'=> 'image'));?><br/>
      
    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
</div>

