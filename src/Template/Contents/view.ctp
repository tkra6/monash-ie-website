<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Content $content
  */
?>

<div id="page-inner">
          <div class="row">
          <div class="col-md-12">
           <h2><?= __('Contents') ?></h2>
          </div>
        </div>
        <hr />
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
        <h3><?=  h($content->type)?></h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($content->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Detail') ?></th>
            <td><?= h($content->detail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($content->image) ?></td>
        </tr>
    </table>
</div>