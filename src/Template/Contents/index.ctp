
<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Contents') ?>
      </h3>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <?php foreach ($contentTables as $contentTable): ?>
      <div class="panel panel-default">
        <div class="panel-heading">
           <h2><?php echo($contentTable->type); ?></h2>
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Contents', 'action' => 'add']); ?></a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">


                  <thead>
                    <tr>

                      <th>
                        <?php echo "Type";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Detail"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($contents as $content): ?>
                    <?php if (($content->type) == ($contentTable->type)) { ?>
                    <tr>
                      <td>
                        <?= h($content->type) ?>
                      </td>
                      <td>
                        <?= h($content->detail) ?>
                      </td>


                      <td class="actions">

                        <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Contents', 'action' => 'edit', $content->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>

             <?php if ((($content->type) != ("Address")) && (($content->type) != ("Quote")) && (($content->type) != ("Phone Number"))) { ?>
                        <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                    array('controller' => 'Contents', 'action' => 'delete',$content->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete content type: '. $content->type)));
                        
                                                                                                                                       } ?>
                                              
                      </td>
                    </tr>
                    <?php } endforeach; ?>
                    <?php echo $this->fetch('postLink');?>

                  </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
       <?php endforeach; ?> 
    </div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>
    