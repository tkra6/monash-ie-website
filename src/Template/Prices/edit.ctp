<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div id="page-inner">

      <div class="form-group">
<div class="prices form large-9 medium-8 columns content">
    <?= $this->Form->create($price) ?>
    <fieldset>
        <legend><?= __('Edit Price') ?></legend>
        
          <?php  echo $this->Form->control('minutes', array('class'=>'form-control'));?><br/>
          <?php  echo $this->Form->control('value', array('class'=>'form-control', 'id'=>'numberbox'));?><br/>
          <?php  echo $this->Form->control('pricetypes_id', array('class'=>'form-control', 'options' => $pricetypes));?><br/>
        
    </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
        
<?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>

<script>
$('#numberbox').keyup(function(){
  if ($(this).val() > 1000){
    alert("Do not enter value more than $1000");
    $(this).val('1000');
  }
});
</script>