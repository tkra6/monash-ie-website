<div id="page-inner">
          <div class="row">
          <div class="col-md-12">
           <h2><?= __('Prices') ?></h2>
          </div>
        </div>
        <hr />
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
        <h3><?= h($price->minutes)." -- ".h($price->value) ?></h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
             
        <tr>
            <th scope="row"><?= __('Minutes') ?></th>
            <td><?= h($price->minutes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pricetype') ?></th>
            <td><?= $price->has('pricetype') ? $this->Html->link($price->pricetype->name, ['controller' => 'Pricetypes', 'action' => 'view', $price->pricetype->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value') ?></th>
            <td><?= h($price->value) ?></td>
        </tr>
    </table>
</div>