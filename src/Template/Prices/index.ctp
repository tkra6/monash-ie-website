    
     <div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Prices') ?>
      </h3>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Prices
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Prices', 'action' => 'add']); ?></a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">


                  <thead>
                    <tr>

                      <th>
                        <?php echo "Minutes";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Value"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Price types"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>

                    
                    
                           <tbody>
            <?php foreach ($prices as $price): ?>
            <tr>
                <td><?= h($price->minutes) ?></td>
                <td><?= h($price->value) ?></td>
                <td><?= $price->has('pricetype') ? $this->Html->link($price->pricetype->name, ['controller' => 'Pricetypes', 'action' => 'view', $price->pricetype->id]) : '' ?></td>

                  
                      <td class="actions">

                        <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Prices', 'action' => 'edit', $price->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>


                        <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                    array('controller' => 'Prices', 'action' => 'delete',$price->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete price : '. $price->minutes . '-- ' . $price->value )));
                    ?>


                      </td>
                    </tr>
                    <?php endforeach; ?>
                    <?php echo $this->fetch('postLink');?>

                  </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>

