
<div id="page-inner">
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
         <?= h($archive->fromName) ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
        <tr>
            <th scope="row"><?= __('From Name') ?></th>
            <td><?= h($archive->fromName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('From Email') ?></th>
            <td><?= h($archive->fromEmail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('To Name') ?></th>
            <td><?= h($archive->toName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Message') ?></th>
            <td><?= h($archive->message) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expiry') ?></th>
            <td><?= h($archive->expiry) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment Status') ?></th>
            <td><?= h($archive->paymentStatus) ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('Status') ?></th>
            <td><?= $archive->has('status') ? $this->Html->link($archive->status->name, ['controller' => 'Status', 'action' => 'view', $archive->status->id]) : '' ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('Price') ?></th>
            <td><?= $archive->has('price') ? $this->Html->link($archive->price->id, ['controller' => 'Prices', 'action' => 'view', $archive->price->minutes]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('UUID') ?></th>
            <td><?= h($archive->UUID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PhoneNumber') ?></th>
            <td><?= $this->Number->format($archive->phoneNumber) ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('VoucherStatus Id') ?></th>
            <td><?= $this->Number->format($archive->voucherStatus_id) ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('DatePurchase') ?></th>
            <td><?= h($archive->datePurchase) ?></td>
        </tr>
    </table>
</div>
