  <div id="page-inner">
  <div class="row">
    <div class="col-md-12">
     <h2>
                 Archived data

      </h2>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          Archived data
        </div>

        <div class="panel-body">

<!--           <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a href="/users/add" >Add</a>
            </li>
          </ul> -->
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                  
                                <thead>
                    <tr>

                      <th>
                        <?php echo "From Name";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "From Email";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Phone Number";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "To Name";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Date Purchase";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Expiry";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Payment Status";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Status";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Price";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        $clickMe = "PDF";
                        $urlPDF = array('controller' => 'Vouchers', 'action' => 'viewPdf');
                        foreach ($archives as $archive): ?>
                    <tr>
                        <td><?= h($archive->fromName) ?></td>
                        <td><?= h($archive->fromEmail) ?></td>
                        <td><?= h($archive->phoneNumber) ?></td>
                        <td><?= h($archive->toName) ?></td>
                        <td><?= h($archive->datePurchase) ?></td>
                        <td><?= h($archive->expiry) ?></td>
                        <td><?php if(($archive->paymentStatus) == 0) { echo "Unpaid"; } else if (($archive->paymentStatus) == 1) { echo "Paypal Payment";} else if (($archive->paymentStatus) == 2) {echo "Phone Payment";} else { echo "Cash"; } ?> </td>              
                        <td><?= $archive->has('status') ? $this->Html->link($archive->status->name, ['controller' => 'Status', 'action' => 'view', $archive->status->id]) : '' ?></td>
                        <td><?= $archive->has('price') ? $this->Html->link($archive->price->value, ['controller' => 'Prices', 'action' => 'view', $archive->price->id]) : '' ?></td>
                        <td class="actions">
                          
                         
                          <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-file']).' PDF',['controller' => 'Vouchers', 'action' => 'viewPdf', $archive->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>
                          
                          <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-envelope']).' Email',['controller' => 'Vouchers', 'action' => 'email', $archive->id],
                                                 ['class' => 'btn btn-danger', 'role' => 'button' , 'escape' => false]);?>


                        </td> 
                    </tr>
                    <?php endforeach; ?>
                  <?php echo $this->fetch('postLink');?>
                </tbody>

                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
        </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>






























































































<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Archive[]|\Cake\Collection\CollectionInterface $archives
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Archive'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Prices'), ['controller' => 'Prices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price'), ['controller' => 'Prices', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="archives index large-9 medium-8 columns content">
    <h3><?= __('Archives') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fromName') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fromEmail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phoneNumber') ?></th>
                <th scope="col"><?= $this->Paginator->sort('toName') ?></th>
                <th scope="col"><?= $this->Paginator->sort('message') ?></th>
                <th scope="col"><?= $this->Paginator->sort('datePurchase') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expiry') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paymentStatus') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prices_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('UUID') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($archives as $archive): ?>
            <tr>
                <td><?= $this->Number->format($archive->id) ?></td>
                <td><?= h($archive->fromName) ?></td>
                <td><?= h($archive->fromEmail) ?></td>
                <td><?= h($archive->phoneNumber) ?></td>
                <td><?= h($archive->toName) ?></td>
                <td><?= h($archive->message) ?></td>
                <td><?= h($archive->datePurchase) ?></td>
                <td><?= h($archive->expiry) ?></td>
                <td><?= $this->Number->format($archive->paymentStatus) ?></td>
                <td><?= $this->Number->format($archive->status_id) ?></td>
                <td><?= $archive->has('price') ? $this->Html->link($archive->price->id, ['controller' => 'Prices', 'action' => 'view', $archive->price->id]) : '' ?></td>
                <td><?= h($archive->UUID) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $archive->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $archive->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $archive->id], ['confirm' => __('Are you sure you want to delete # {0}?', $archive->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
