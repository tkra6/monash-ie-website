 <div id="page-inner">
  <div class="row">
    <div class="col-md-12">
     <h2>Admin Dashboard</h2>
     <h5><?php echo "Welcome " . ($this->request->session()->read('Auth.User.first_name')) ." ". ($this->request->session()->read('Auth.User.last_name')) . ", Love to see you back."; ?></h5>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Users
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Users', 'action' => 'add']); ?></a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                  
                  <thead>
                    <tr>

                      <th>
                        <?php echo "Username";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "First Name"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Last Name"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Email"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Created"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Modified"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                   <tbody>
                      <?php foreach ($users as $user): ?>
                      <tr>

                        <td>
                          <?= h($user->username) ?>
                        </td>
                        <td>
                          <?= h($user->first_name) ?>
                        </td>
                        <td>
                          <?= h($user->last_name) ?>
                        </td>
                        <td>
                          <?= h($user->email) ?>
                        </td>
                        <td>
                          <?= h($user->created) ?>
                        </td>
                        <td>
                          <?= h($user->modified) ?>
                        </td>
                        <td class="actions">
                          
                           <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Users', 'action' => 'edit', $user->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>

                          <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                          array('controller' => 'Users', 'action' => 'delete',$user->id), 
                          array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete User: '. $user->username)));
                          ?>
                         
                        </td>
                      </tr>
                      <?php endforeach; ?>
                      <?php echo $this->fetch('postLink');?>
                    </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
        </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>
