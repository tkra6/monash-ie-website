<?php echo $this->Html->css('/css/testlogin.css') ?>
<?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
<?php echo $this->Html->script('/js/testlogin.js') ?>

<head>
	<meta charset="UTF-8">
	<title>Login form</title>
</head>

<div style="color: white; padding: 15px 50px 5px 50px; float: left; font-size: 16px; position:relative; z-index:2;">
	<a class="btn btn-warning btn-lg" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'display')) ?>">Home</a>
</div>

<div class="container">
	<div id="login-box">
		<div class="logo">
			<img src="/img/muscleWlogo.jpg" height="200" width="200" class="img img-responsive img-circle center-block" />
			<h1 class="logo-caption"><span class="tweak">L</span>ogin</h1>
		</div>
		<!-- /.logo -->
		<div class="controls">
			<?php echo $this->Form->create(); ?>
			<?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Username', 'type' => 'username')); ?>
			<?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Password', 'type' => 'password')); ?>
			<?php echo $this->Form->input('rememberMe', array('class' => 'checkbox', 'label' => 'Remember Me', 'type' => 'checkbox')); ?>
			<?php echo $this->Form->button(__('Login'), array('type' => 'submit', 'class' => 'btn btn-default btn-block btn-custom','label' => false)); ?>
			<?php echo $this->Form->end(); ?> <br/>
			<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">Forget password? </button>

		</div>
		<!-- /.controls -->
	</div>
	<!-- /#login-box -->
</div>
<!-- /.container -->
<div style="position:relative; z-index:99999999;" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Reset your password</h4>
			</div>
			<?php echo $this->Form->create('forgetPw', array(
          'url'   => array('controller' => 'users','action' => 'forgetPw'))); ?>
			<div class="modal-body">
				Please enter your email to reset your password. Upon verify, we will send you an email with your new password.
				
				
				<div class="form-group input-group">
				<span class="input-group-addon">@</span>
					<?= $this->Form->control('email', array('class'=> 'form-control', 'placeholder' => 'Enter your email', 'type'=>'email', 'id'=> 'email', 'label'=>false, 'required' => true,)) ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?= $this->Form->button('submit', array('name'=> 'submit', 'class'=> 'btn btn-warning', 'onClick'=> 'Javascript:checkEmail();')) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>

<div id="particles-js"></div>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js"></script>-->
<script>
		
		function checkEmail() {

    var email = document.getElementById('email');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)){
    alert('Please provide a valid email address');
    email.focus;
    return false;
			
			
 }
	</script>