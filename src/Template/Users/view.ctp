
<div id="page-inner">
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
          <?= h($user->first_name) ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <tr>
                <th scope="row">
                  <?= __('Username') ?>
                </th>
                <td>
                  <?= h($user->username) ?>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <?= __('First Name') ?>
                </th>
                <td>
                  <?= h($user->first_name) ?>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <?= __('Last Name') ?>
                </th>
                <td>
                  <?= h($user->last_name) ?>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <?= __('Email') ?>
                </th>
                <td>
                  <?= h($user->email) ?>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <?= __('Created') ?>
                </th>
                <td>
                  <?= h($user->created) ?>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <?= __('Modified') ?>
                </th>
                <td>
                  <?= h($user->modified) ?>
                </td>
              </tr>



            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

</html>