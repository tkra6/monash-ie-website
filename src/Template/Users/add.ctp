<div id="page-inner">

      <div class="form-group">
        <?= $this->Form->create($user) ?>
          <fieldset>
           <h2>
             <legend><?= __('Add User') ?></legend>
            </h2> 

            
            <?php echo $this->Form->control('username', array('class'=>'form-control')); ?> </br>
            <?php echo $this->Form->control('password', array('class'=>'form-control')); ?> </br>
            <?php echo $this->Form->control('first_name', array('class'=>'form-control')); ?> </br>
            <?php echo $this->Form->control('last_name', array('class'=>'form-control')); ?> </br>
            <?php echo $this->Form->control('email', array('class'=>'form-control')); ?> </br>
        
          </fieldset>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
          <?= $this->Form->end() ?>
      </div>

</div>