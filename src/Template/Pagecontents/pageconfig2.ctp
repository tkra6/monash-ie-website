<?php echo $this->Html->css('choosePage.css'); ?>

<div class="container" style="position:absolute;">
    <div class="row">
		<div class="col-xs-12">
			
			<h1> <?php echo ($pageName); ?> </h1>
			 
			 
			<!-- tabs -->
			<div class="tabbable tabs-left">
				<ul class="nav nav-tabs">
					<?php $countPage = 0;?>
					<?php foreach ($pageItems as $pageItem): ?>
					<?php if ($countPage == 0) {?>
					<li class="active"><a href="<?php echo "#". $pageItem->section?>" data-toggle="tab"><?php echo $pageItem->section;?></a></li>
					<?php $countPage = $countPage +1; ?>
					<?php } else { ?>
					<li><a href="<?php echo $this->url->build(array('controller' => 'Pagecontents', 'action' => 'edit', $pageItem->id)) ?>" data-toggle="tab"><?php echo $pageItem->section;?></a></li>
					<?php } endforeach ?>
				</ul>
				
				<div class="tab-content">
					<?php $countSection = 0;?>
					<?php foreach ($pageItems as $pageItem): ?>
					<?php if ($countSection == 0) {?>
					<div class="tab-pane active" id="<?php echo $pageItem->section;?>">                
						<div class="">
							<h1><?php echo $pageItem->section;?></h1>
							<div class="pagecontents form large-9 medium-8 columns content">

    <?= $this->Form->create($pageItems) ?>
    <fieldset>
        <legend><?= __('Add Pagecontent') ?></legend>
             <?php echo $this->Form->control('heading', array('class'=>'form-control')); ?>   </br>
             <?php echo $this->Form->control('content', array('class'=>'ckeditor','type'=>'textarea', 'name'=> 'content')); ?>    </br>
						 <?php echo $this->Form->control('metatype', array('id' => 'testInput', 'class'=>'form-control','type'=>'text', 'name'=> 'metatype')); ?>    </br>
							<?php echo "Press TAB or COMMA to add metatype";?></br> </br>
             <?php echo $this->Form->control('block', array('type' => 'checkbox')); ?>   </br>
						
    </fieldset>
    </br>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>             
						</div>
					</div> 
					<?php $countSection = $countSection +1; ?>
					<?php } else { ?>
					
					<div class="tab-pane" id="<?php echo $pageItem->section;?>"> 
						<div class="">
							<h1><?php echo $pageItem->section;?></h1>
							<p>because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>                 
						</div>
					</div>
				<?php } endforeach ?>
				</div>
			</div>
			<!-- /tabs -->
		</div>
	</div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>