<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Page Content') ?>
      </h3>
    </div>
  </div>
  <hr/>

  <?php if (($pagelist != null) || ($thelist != null)) { ?>
<div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to block') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($pagelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pagelist = $accountValues; ?>
    
    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pagelist), 'empty'=>'', 'required'=>true)); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
       <div class="col-md-4 col-sm-4">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <?= __('Select a page to unblock') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($thelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $thelist = $accountValues; ?>
    

    <?= $this->Form->create('', array('url' => array('controller' => 'pagecontents', 'action' => 'unblock'))); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($thelist), 'empty'=>'', 'required'=>true)); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  </div>
      
   <?php } elseif (($pagelist == true) && ($thelist == false)) { ?>
    <div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to block') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($pagelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pagelist = $accountValues; ?>
    
    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pagelist), 'empty'=>'Choose')); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  
<?php } elseif (($thelist == true) && ($pagelist == false)) { ?>
  <div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <?= __('Select a page to unblock') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($thelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $thelist = $accountValues; ?>
    

    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($thelist), 'empty'=>'Choose')); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  </div>
      </div>
  <?php } else { echo "Something wrong"; } ?>

   

	<div class="row">

		<section class="content">
			<h1 style="padding-left:10px;">Manage Pages</h1>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="pull-right">
							<div class="btn-group">
                <?php foreach ($pagetable as $pagetables): ?>
								<button type="button" class="btn btn-success btn-filter" data-target="<?php echo($pagetables->page); ?>"><?php echo($pagetables->page); ?></button>
                 <?php endforeach; ?> 
							</div>
						</div>
						<div class="table-container">
							<table class="table table-filter">
								<tbody>
                  <?php foreach ($pagetable as $pagetables): ?>
                  <?php foreach ($pagecontents as $pagecontent): ?>
                  <?php if (($pagecontent->page) == ($pagetables->page)) { ?>
									<tr data-status="<?php echo($pagetables->page); ?>">

										<td>
											<div class="ckbox">
												<input type="checkbox" id="checkbox1">
												<label for="checkbox1"></label>
											</div>
										</td>
										<td>
											<div class="media">
                        <a href="#" class="pull-left">
													<img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
												</a>
												<div class="media-body">
													<span class="media-meta pull-right">Febrero 13, 2016</span>
													<h4 class="title">
														<?php echo "Heading: ". ($pagecontent->heading); ?>
														<span class="pull-right pagado"><?php echo "Page:".($pagecontent->page); ?></span>
													</h4>
													<p class="summary"><?php echo($pagecontent->metatype); ?></p>
												</div>
											</div>
										</td>
									</tr>
                  <?php } endforeach; ?> 
                  <?php endforeach; ?> 
                 
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		
	</div>
</div>



    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
      $(document).ready(function () {

	$('.star').on('click', function () {
      $(this).toggleClass('star-checked');
    });

    $('.ckbox label').on('click', function () {
      $(this).parents('tr').toggleClass('selected');
    });

    $('.btn-filter').on('click', function () {
      var $target = $(this).data('target');
      if ($target != 'all') {
        $('.table tr').css('display', 'none');
        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
      } else {
        $('.table tr').css('display', 'none').fadeIn('slow');
      }
    });

 });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>