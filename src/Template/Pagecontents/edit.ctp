<?php
foreach ($pageOptions as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pageOptions = $accountValues;
?>
<div id="page-inner">
<?php echo $this->Html->script(array('/bs-binary-admin/assets/js/jquery-1.10.2.js', '/bs-binary-admin/assets/js/bootstrap.min.js', '/bs-binary-admin/assets/js/jquery.metisMenu.js', '/bs-binary-admin/assets/js/custom.js')); ?>
<?php echo $this->Html->css('tags.css'); ?>
<?php echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js'); ?>
<?php echo $this->Html->script('autofill.js'); ?>
<?php echo $this->Html->script('tags.js'); ?>
<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>
<?php echo $this->Html->script('ckeditor/plugins/tableresize'); ?>
<?php echo $this->Html->script('ckeditor/plugins/image2'); ?>
			<script>
			$(function() {
				$("#testInput").tags({
					unique: true,
					maxTags: 15
				});
			});
		</script>

	
<div class="pagecontents form large-9 medium-8 columns content">
    <?= $this->Form->create($pagecontent) ?>
    <fieldset>
        <legend><?= __('Edit Pagecontent') ?></legend>
             <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pageOptions))); ?>     </br>
             <?php echo $this->Form->control('section', array('class'=>'form-control')); ?>   </br>
	           <?php echo $this->Form->control('heading', array('class'=>'form-control')); ?>   </br>
						 <?php echo $this->Form->control('metatype', array('id' => 'testInput', 'class'=>'form-control','type'=>'text', 'name'=> 'metatype')); ?>    </br>
             <?php echo $this->Form->control('block', array('type' => 'checkbox')); ?>   </br>
<?php $check = false; ?>
<?php foreach ($contents as $content): ?>
<?php if ($pagecontent['section'] == ($content->type)) { ?>
						  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Contents
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a href="/contents/add" >Add</a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">


                  <thead>
                    <tr>

                      <th>
                        <?php echo "Type";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Main"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?=__('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($contents as $content): ?>
										<?php if ($pagecontent['section'] == ($content->type)) { ?>
                    <tr>
                      <td>
                        <?= h($content->type) ?>
                      </td>
                      <td>
                        <?= h($content->main) ?>
                      </td>


                      <td class="actions">

                        <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Contents', 'action' => 'edit', $content->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>


                        <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                    array('controller' => 'Contents', 'action' => 'delete',$content->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete content type: '. $content->main)));
                        
                    ?>
                                              
                      </td>
                    </tr>
                    <?php } endforeach; ?>
                    <?php echo $this->fetch('postLink');?>

                  </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
				<?php $check = true; ?>
				<?php break; ?>
							<?php } else { ?>
							<?php } ?>
			<?php endforeach; ?>
				
				<?php if ($check == false) { ?>
				             <?php echo $this->Form->control('content', array('class'=>'ckeditor','type'=>'textarea', 'name'=> 'content')); ?>    </br>
				<?php } ?>


    </fieldset>
    </br>

 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>

<!-- <script>
		CKEDITOR.replace( 'content', {
			extraPlugins: ['colordialog,tableresize', 'tableresize'. 'image2'],
			height: 470
		});
	</script> -->
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>