<?php echo $this->Html->css('choosePage.css'); ?>
<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Page Content') ?>
      </h3>
    </div>
  </div>
  <hr/>

  <?php if (($pagelist != null) || ($thelist != null)) { ?>
<div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to block') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($pagelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pagelist = $accountValues; ?>
    
    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pagelist), 'empty'=>'', 'required'=>true)); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
       <div class="col-md-4 col-sm-4">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <?= __('Select a page to unblock') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($thelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $thelist = $accountValues; ?>
    

    <?= $this->Form->create('', array('url' => array('controller' => 'pagecontents', 'action' => 'unblock'))); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($thelist), 'empty'=>'', 'required'=>true)); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  </div>
      
   <?php } elseif (($pagelist == true) && ($thelist == false)) { ?>
    <div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to block') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($pagelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pagelist = $accountValues; ?>
    
    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pagelist), 'empty'=>'Choose')); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  
<?php } elseif (($thelist == true) && ($pagelist == false)) { ?>
  <div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <?= __('Select a page to unblock') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($thelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $thelist = $accountValues; ?>
    

    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($thelist), 'empty'=>'Choose')); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  </div>
      </div>
  <?php } else { echo "Something wrong"; } ?>

  
  <div class="container" style = "position: absolute;">
    <div class="space">	
    <div class="row">
	      <?php foreach ($pagetable as $pagetables): ?>
	    <div class="col-md-4">
	        <div class="widget widget-call-to-action well-default">
                            <h3 class="widget-title"><?php echo ($pagetables->page)?></h3>
                                <a href="<?php echo $this->url->build(array('controller' => 'pagecontents', 'action' => 'pageconfig', $pagetables->page,  $pagetables->section)) ?>" class="btn btn-white btn-lg"><?php echo "Manage ".($pagetables->page). " Page";?></a>
                    </div>
	        
	    </div>
      <?php endforeach ?>
	</div></div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>

    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>