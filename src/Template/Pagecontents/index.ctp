<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Page Content') ?>
      </h3>
    </div>
  </div>
  <hr/>

  <?php if (($pagelist != null) || ($thelist != null)) { ?>
<div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to block') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($pagelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pagelist = $accountValues; ?>
    
    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pagelist), 'empty'=>'', 'required'=>true)); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
       <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to unblock') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($thelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $thelist = $accountValues; ?>
    

    <?= $this->Form->create('', array('url' => array('controller' => 'pagecontents', 'action' => 'unblock'))); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($thelist), 'empty'=>'', 'required'=>true)); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  </div>
      
   <?php } elseif (($pagelist == true) && ($thelist == false)) { ?>
    <div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to block') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($pagelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pagelist = $accountValues; ?>
    
    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($pagelist), 'empty'=>'Choose')); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  
<?php } elseif (($thelist == true) && ($pagelist == false)) { ?>
  <div class="row">
  <div class="col-md-4 col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">
      <?= __('Select a page to unblock') ?><br/>
  </div>
<?php $accountValues = array();
foreach ($thelist as $key => $value) {
    $accountValues[$value] = $value;  } 
    $thelist = $accountValues; ?>
    

    <?= $this->Form->create(); ?><br/>
    <?php echo $this->Form->control('page', array('class'=>'form-control', 'options' => ($thelist), 'empty'=>'Choose')); ?> <br/>
    <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-left', 'type' => 'submit')); ?><br/><br/>
    <?php echo $this->Form->end(); ?>
  </div>
    
  </div>
  </div>
      </div>
  <?php } else { echo "Something wrong"; } ?>

                            <div class="alert alert-default"> Click here to add a page.
                             <a class="btn icon-btn btn-success" href="<?php echo $this->url->build(array('controller' => 'pagecontents', 'action' => 'add')) ?>">
                              <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-success"></span>Add</a>
                            </div> 
  <br/>
  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <?php foreach ($pagetable as $pagetables): ?>
      <div class="panel panel-warning">
        <div class="panel-heading">
          <h3>
            <?php echo($pagetables->page); ?>
          </h3>
        </div>
        
    <div class="panel-body">

          <br/>
              
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">


                  <thead>
                    <tr>

                      <th>
                        <?php echo "Page";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Section"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Metatype"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Block"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>                     
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                        
          <tbody>
            <?php foreach ($pagecontents as $pagecontent): ?>
            <?php if (($pagecontent->page) == ($pagetables->page)) { ?>
            <tr>

              <td>
                <?php echo $this->Form->control('page', array('type'=>'hidden','value'=>($pagecontent->page)));?>
                <?= h($pagecontent->page) ?>
              </td>
              <td>
                <?php echo $this->Form->control('section', array('type'=>'hidden','value'=>($pagecontent->section)));?>
                <?= h($pagecontent->section) ?>
              </td>
              <td>
                <?php echo $this->Form->control('metatype', array('type'=>'hidden','value'=>($pagecontent->metatype)));?>
                <?= h($pagecontent->metatype) ?>
              </td>
              <td>
                <?php if (($pagecontent->block) == 1) {

                echo  $this->Form->checkbox('block', array('type'=>'checkbox','checked'=>true,  'disabled' => true)); } 
                      else {  echo $this->Form->checkbox('block', array('type'=>'checkbox','checked'=>false, 'disabled' => true));
                      }?>   </br> 
            
                <td class="actions">
                  <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Pagecontents', 'action' => 'edit', $pagecontent->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>


                  <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                    array('controller' => 'Pagecontents', 'action' => 'delete',$pagecontent->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete Page: '. $pagecontent->page)));
                    ?>
                                    
                  <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-eye']).' View',['controller' => 'Pagecontents', 'action' => 'view', $pagecontent->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>
                  
                </td>
               
              </td>
            </tr>
            <?php } endforeach; ?>   
          </tbody>
                </table>

<!--               </div>
            </div> -->
          </div>
            
            <?php echo $this->fetch('postLink');?>
      </div>
  
    </div>
<?php endforeach; ?>
    </div>
  </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>