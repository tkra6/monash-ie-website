<?php
foreach ($pageOptions as $key => $value) {
    $accountValues[$value] = $value;  } 
    $pageOptions = $accountValues;
?>

<div id="page-inner">
<?php echo $this->Html->script(array('/bs-binary-admin/assets/js/jquery-1.10.2.js', '/bs-binary-admin/assets/js/bootstrap.min.js', '/bs-binary-admin/assets/js/jquery.metisMenu.js', '/bs-binary-admin/assets/js/custom.js')); ?>

<?php echo $this->Html->css('tags.css'); ?>
<?php echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js'); ?>
<?php echo $this->Html->script('autofill.js'); ?>
<?php echo $this->Html->script('tags.js'); ?>
<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>
	
			<script>
			$(function() {
				$("#testInput").tags({
					unique: true,
					maxTags: 15
				});
			});
		</script>

  
<div class="pagecontents form large-9 medium-8 columns content">

    <?= $this->Form->create($pagecontent) ?>
    <fieldset>
        <legend><?= __('Add Pagecontent') ?></legend>
             <?php echo $this->Form->control('page', array('class'=>'form-control')); ?>     </br>
             <?php echo $this->Form->control('section', array('class'=>'form-control')); ?>   </br>
	           <?php echo $this->Form->control('heading', array('class'=>'form-control')); ?>   </br>
             <?php echo $this->Form->control('content', array('class'=>'ckeditor','type'=>'textarea', 'name'=> 'content')); ?>    </br>
						 <?php echo $this->Form->control('metatype', array('id' => 'testInput', 'class'=>'form-control','type'=>'text', 'name'=> 'metatype')); ?>    </br>
						 <?php echo "Press TAB or COMMA to add metatype";?></br> </br>
             <?php echo $this->Form->control('block', array('type' => 'checkbox')); ?>   </br>
						
    </fieldset>
    </br>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
    <?= $this->Form->end() ?>
</div>
</div>
