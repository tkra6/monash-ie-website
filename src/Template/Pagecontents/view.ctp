<div id="page-inner">
          <div class="row">
          <div class="col-md-12">
            <h3><?= __('Page Content') ?></h3> 
          </div>
        </div>
  <hr/>
  <div class="panel panel-default">
        <div class="panel-heading">
    <?= h($pagecontent->page) ?>
        </div>

    <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
        <tr>
            <th scope="row"><?= __('Page') ?></th>
            <td><?= h($pagecontent->page) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Section') ?></th>
            <td><?= h($pagecontent->section) ?></td>
        </tr>
         <tr>
            <th scope="row"><?= __('Metatype') ?></th>
           <html>
            <td><?= h($pagecontent->section) ?></td>
        </tr>
    </table>
            
    <div class="row">
      <div class="col-md-12">
        <h4><?= __('Content') ?></h4>
        <?= (($pagecontent->content)); ?>
                    
    </div>
</div>
    </div>
  </div>
</div>
