<div class="templatemo-welcome" id="templatemo-service">
		<div class="container">
			
			
			<div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Selected Appointment'; ?>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Appointment Type</th>
                                            <th>Practitioner</th>
																						<th>Date</th>
																						<th>Time</th>
																						<th>Duration</th>
																						<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
																				echo '<tr>';
																						echo '<td>'.$bookingDetails[$key]['appointment_type'].'</td>';
																						echo '<td>'.$bookingDetails[$key]['practitioner_name'].'</td>';
	
																						$dateStart = explode("T", $bookingDetails[$key]['start']);
																						$newDate = date("d-m-Y", strtotime($dateStart[0]));
																						echo '<td>'.$newDate.'</td>';
	
																						$startTime = explode("Z", $dateStart[1]);
																						echo '<td>'.$startTime[0].'</td>';
	
																						$dateEnd = explode("T", $bookingDetails[$key]['end']);
																						$endTime = explode("Z", $dateEnd[1]);
																						$start_date = new DateTime('2007-09-01 '.$startTime[0]);
																						$since_start = $start_date->diff(new DateTime('2007-09-01 '.$endTime[0]));
																						echo '<td>'.$since_start->i.' minutes<br>'.'</td>';
	
																				echo '</tr>';
																			 ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
			
			<div class="row">
                <div class="col-md-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
    																	<?= $this->Form->create($cancelForm) ?>
																			<p class="help-block"><b>Cancellation Information and Fees:</b> The minimum cancellation time is 24hrs. Anything under this time length requires full 
																				payment of treatment as our practitioners need to be paid for their time that was being held by you.</br></br>

																			<b>If you need to reschedule/alter or cancel</b> an appointment please telephone (03) 9530 6400 as soon as possible so we 
																				can try to fill this gap in the massage timetable to allow another client in need of pain relief the booking time..</p>
																			<?php $cancelReasons = [10 => 'Feeling better', 20 => "Condition's worsened", 30 => 'Sick', 40 => 'Away', 60 => 'Work', 50 => 'Other']; ?>
																			<?php echo $this->Form->control('cancellationReason', array('class'=>'form-control', 'required'=>'required', 'options' => $cancelReasons, 'value' => $cancelReasons, 'empty'=>'Please select a reason')); ?> <br/>
																			<?php echo $this->Form->button(__('Confirm Cancellation'), array('class' => 'btn btn-default pull-left','div' => false, 'label' => false, 'type' => 'submit')); ?>
																			<?php echo $this->Html->link("Return", array('controller' => 'clients', 'action'=> 'viewbookings'), array( 'class' => 'btn btn-default pull-right','div' => false, 'label' => false)); ?>



																	</form>
															</div>
															<div class="col-md-6">
																
															</div>
													</div>
											</div>
									</div>
				</div>
			
			</div>
			
    
	</div>
</div>