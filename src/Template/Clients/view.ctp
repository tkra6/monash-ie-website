<?php
/**
  * @var \App\View\AppView $this
  */
$title = array('Mr'=> 'Mr', 'Ms' => 'Ms', 'Mrs'=> 'Mrs', 'Miss' => 'Miss', 'Mx'=> 'Mx', 'Dr' => 'Dr', 'Master'=> 'Master', 
             'Professor' => 'Professor', 'Sir' => 'Sir',  'Madam' => 'Madam',  'Lord' => 'Lord',  'Lady' => 'Lady',  'Dame' => 'Dame',  
             'Reverend' => 'Reverend',  'Father' => 'Father', 'Sister' => 'Sister',  'Rabbi' => 'Rabbi', 'Captain' => 'Captain',
             'King' => 'King', 'Queen' => 'Queen', 'Zombie Lord' => 'Zombie Lord');

$gender = array('Male'=> 'Male', 'Female' => 'Female', 'Not Applicable'=> 'Not Applicable');

$reminder = array('None'=> 'None', 'SMS' => 'SMS', 'Email'=> 'Email', 'SMS & Email'=> 'SMS & Email');


$concession = array('None'=> 'None', 'Seniors' => 'Seniors', 'Students'=> 'Students');

?>


  <div id="page-inner">
    <legend>
      <?= __('View Client') ?>
    </legend>
    <?= $this->Form->create($client) ?>
      <!-- /. ROW  -->
      <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">

            <div class="panel-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="panel panel-primary">
                    <div class="panel-heading">
                      Personal Detail
                    </div>
                    <div class="panel-body">
                      <?php echo $this->Form->control('title', array('class'=>'form-control', 'options' => $title , 'empty'=>'Choose','disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('firstName', array('class'=>'form-control' ,'placeholder'=>'Enter your first name','disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('lastName', array('class'=>'form-control','placeholder'=>'Enter your first name','disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('email', array('class'=>'form-control','placeholder'=>'Enter your Email','disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('gender', array('class'=>'form-control', 'options' => $gender , 'empty'=>'Choose','disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('DOB', array('class'=>'form-control' ,'disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('extraInfo', array('class'=>'form-control', 'type'=>'textarea','disabled' => 'disabled')); ?> <br/>
                      <?php echo $this->Form->control('comment', array('class'=>'form-control','type'=>'textarea','disabled' => 'disabled')); ?> <br/>
                    </div>
                  </div>
                  
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      Address Detail
                    </div>
                    <div class="panel-body">
                      <?php echo $this->Form->control('city', array('class'=>'form-control','placeholder'=>'Enter your City')); ?> <br/>
                      <?php echo $this->Form->control('state', array('class'=>'form-control' ,'placeholder'=>'Enter your State')); ?> <br/>
                      <?php echo $this->Form->control('country', array('class'=>'form-control','placeholder'=>'Enter your Country')); ?> <br/>
                      <?php echo $this->Form->control('postalCode', array('class'=>'form-control','placeholder'=>'Enter your Postal Code')); ?> <br/>
                      <?php echo $this->Form->control('street', array('class'=>'form-control','placeholder'=>'Enter your Street')); ?> <br/>
                    </div>
                  </div>

                    <div class="panel panel-success">
                    <div class="panel-heading">
                      Medical Detail
                    </div>
                    <div class="panel-body">
                  <?php echo $this->Form->control('occupation', array('class'=>'form-control','placeholder'=>'Enter your Occupation','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('emergency', array('class'=>'form-control', 'type'=>'textarea','placeholder'=>'Enter Contact Name: Contact Number','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('medicareNum', array('class'=>'form-control','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('referenceNum', array('class'=>'form-control','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('doctor', array('class'=>'form-control','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('note', array('class'=>'form-control', 'type'=>'textarea','disabled' => 'disabled')); ?> <br/>
                    </div>

                  </div>
                  
                  <div class="panel panel-warning">
                    <div class="panel-heading">
                      Communication preferences
                    </div>
                    <div class="panel-body">
                  <?php echo $this->Form->control('reminder', array('class'=>'form-control', 'options' => $reminder , 'empty'=>'Choose','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('smsMarketing', array('class'=>'checkbox', 'type'=>'checkbox', 'value' => 'Subscribe', 'hiddenField' => 'Unsubscribe','disabled' => 'disabled'));?>
                  <?php echo $this->Form->control('confirmation', array('class'=>'checkbox', 'type'=>'checkbox', 'value' => 'Subscribe', 'hiddenField' => 'Unsubscribe','disabled' => 'disabled'));?>
                    </div>

                  </div>

                  <div class="panel panel-danger">
                    <div class="panel-heading">
                      Billing Information
                    </div>
                    <div class="panel-body">
                  <?php echo $this->Form->control('concessionType', array('class'=>'form-control', 'options' => $concession , 'empty'=>'Choose','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('additionalEmail', array('class'=>'form-control','disabled' => 'disabled')); ?> <br/>
                  <?php echo $this->Form->control('invoiceInfo', array('class'=>'form-control', 'type'=>'textarea','disabled' => 'disabled')); ?> <br/>
                    </div>

                  </div>
                  
