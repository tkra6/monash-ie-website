<div class="templatemo-welcome" id="templatemo-service">
		<div class="container">
         
			 <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $patientName."'s Appointments";
																?>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
																	<?php if (sizeof($bookingDetails) == 0) {
																	echo "<br>No Bookings Found"; 
																} ?>
                                    <thead>
                                        <tr>
                                            <th>Appointment Type</th>
                                            <th>Practitioner</th>
																						<th>Date</th>
																						<th>Time</th>
																						<th>Duration</th>
																						<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
																			<?php if (is_array($bookingDetails) || is_object($bookingDetails)) {
                                        		foreach ($bookingDetails as $key => $appointment) {
																								echo '<tr>';
																								echo '<td>'.$appointment['appointment_type'].'</td>';
																								echo '<td>'.$appointment['practitioner_name'].'</td>';

																								$dateStart = explode("T", $appointment['start']);
																								$newDate = date("d-m-Y", strtotime($dateStart[0]));
																								echo '<td>'.$newDate.'</td>';

																							
																								$startTime = explode("Z", $dateStart[1]);
																								$start_date = new DateTime('2007-09-01 '.$startTime[0]);
																								$melb_time = new DateTimeZone('Australia/Melbourne');
																								$start_date->setTimezone($melb_time);
																								echo '<td>'.$start_date->format('H:i').'</td>';


																								$dateEnd = explode("T", $appointment['end']);
																								$endTime = explode("Z", $dateEnd[1]);
																								$since_start = $start_date->diff(new DateTime('2007-09-01 '.$endTime[0]));
																								echo '<td>'.$since_start->i.' minutes<br>'.'</td>';

																								echo '<td>';
																								echo $this->Html->link(__('Cancel'), ['action' => 'cancelbooking', $key], ['class' => 'btn btn-danger btn-xs']);
																								echo '</td>';
																								echo '</tr>';
																						}
																			}?>
                                    </tbody>
                                </table>
                            </div>
														<?php echo $this->Html->link("New appointment", array('controller' => 'pages', 'action'=> 'book'), array( 'class' => 'btn btn-default pull-left','div' => false, 'label' => false)); ?>
														<?php echo $this->Html->link("Return", array('controller' => 'clients', 'action'=> 'booklogin'), array( 'class' => 'btn btn-default pull-right','div' => false, 'label' => false)); ?>


                        </div>
                    </div>
			
			
      
      
  </div>
</div>