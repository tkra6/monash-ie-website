<div class="templatemo-welcome" id="templatemo-service">
		<div class="container">
         <div class="col-md-6">
					 
       <div class="panel panel-default">
					<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<?= $this->Form->create($loginForm) ?>
											<p class="help-block"><b>Enter the details you have used to make bookings, in order to view any appointments you've made.</b></p>
									<?php echo $this->Form->control('email', array('class'=>'form-control' ,'placeholder'=>'Enter your email address', 'type' => 'name')); ?> <br/>
									<?php echo $this->Form->control('phone', array('class'=>'form-control' ,'placeholder'=>'Enter your mobile or home phone', 'type' => 'phone')); ?> <br/>
            			<?= $this->Flash->render('invalidlogin') ?>





									<?php echo $this->Form->button(__('View your bookings'), array('class' => 'btn btn-default pull-left','div' => false, 'label' => false, 'type' => 'submit')); ?>
									</div>
							</div>
					 </div>
				</div>
			</div>

      
      
  </div>
</div>