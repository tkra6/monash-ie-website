  <?php
/**
  * @var \App\View\AppView $this
  */
$title = array('Mr'=> 'Mr', 'Ms' => 'Ms', 'Mrs'=> 'Mrs', 'Miss' => 'Miss', 'Mx'=> 'Mx', 'Dr' => 'Dr', 'Master'=> 'Master', 
             'Professor' => 'Professor', 'Sir' => 'Sir',  'Madam' => 'Madam',  'Lord' => 'Lord',  'Lady' => 'Lady',  'Dame' => 'Dame',  
             'Reverend' => 'Reverend',  'Father' => 'Father', 'Sister' => 'Sister',  'Rabbi' => 'Rabbi', 'Captain' => 'Captain',
             'King' => 'King', 'Queen' => 'Queen', 'Zombie Lord' => 'Zombie Lord');

$gender = array('Male'=> 'Male', 'Female' => 'Female', 'Not Applicable'=> 'Not Applicable');

$reminder = array('None'=> 'None', 'SMS' => 'SMS', 'Email'=> 'Email', 'SMS & Email'=> 'SMS & Email');


$concession = array('None'=> 'None', 'Seniors' => 'Seniors', 'Students'=> 'Students');

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Musclework Massage</title>
<style>
        * {
        margin: 0;
        padding: 0;
        }
        html {
        height: 100%;
        background: url('/img/form/bg.png');
        background: linear-gradient(rgba(196, 102, 0, 0.2), rgba(155, 89, 182, 0.2)),  url(/img/form/bg.png);
        }
        body {
        font-family: arial, verdana;
        }
</style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="/js/jquery-multi-step-form.js" type="text/javascript"></script>
    <link href="/css/jquery-multi-step-form.css" media="screen" rel="stylesheet" type="text/css"/>
  
</head>
<body>
    <h1 style="margin:150px auto 50px auto" align="center">New Client Form</h1>

    <div id="multistepform-example-container">
    <ul id="multistepform-progressbar">
        <li class="active">Personal Detail</li>
        <li>Address Detail</li>
        <li>Medical Detail</li>
        <li>Communication Preferences</li>
    </ul>
  
<div class="form">
<form action="">
<h2 class="fs-title">Personal Detail</h2>
<h3 class="fs-subtitle">Tell us about you</h3>
      <?= $this->Form->create($client) ?>

                      <?php echo $this->Form->control('title', array('class'=>'form-control', 'options' => $title , 'empty'=>'Choose')); ?> <br/>
                      <?php echo $this->Form->control('firstName', array('class'=>'form-control' ,'placeholder'=>'Enter your first name')); ?> <br/>
                      <?php echo $this->Form->control('lastName', array('class'=>'form-control','placeholder'=>'Enter your first name')); ?> <br/>
                      <?php echo $this->Form->control('email', array('class'=>'form-control','placeholder'=>'Enter your Email')); ?> <br/>
                      <?php echo $this->Form->control('gender', array('class'=>'form-control', 'options' => $gender , 'empty'=>'Choose')); ?> <br/>
                      <?php echo $this->Form->control('DOB', array('class'=>'form-control' )); ?> <br/>
                      <?php echo $this->Form->control('extraInfo', array('class'=>'form-control', 'type'=>'textarea')); ?> <br/>
                      <?php echo $this->Form->control('comment', array('class'=>'form-control','type'=>'textarea')); ?> <br/>
<input type="button" name="next" class="next button" value="Next">
</form>
</div>
  <div class="form">
  <form action="">
  <h2 class="fs-title">Address Detail</h2>
  <h3 class="fs-subtitle">We keep this record to mail you</h3>

                        <?php echo $this->Form->control('city', array('class'=>'form-control','placeholder'=>'Enter your City')); ?> <br/>
                        <?php echo $this->Form->control('state', array('class'=>'form-control' ,'placeholder'=>'Enter your State')); ?> <br/>
                        <?php echo $this->Form->control('country', array('class'=>'form-control','placeholder'=>'Enter your Country')); ?> <br/>
                        <?php echo $this->Form->control('postalCode', array('class'=>'form-control','placeholder'=>'Enter your Postal Code')); ?> <br/>
                        <?php echo $this->Form->control('street', array('class'=>'form-control','placeholder'=>'Enter your Street')); ?> <br/>

  <input type="button" name="previous" class="previous button" value="Previous">
  <input type="button" name="next" class="next button" value="Next">
  </form>
  </div>
      
  <div class="form">
  <form action="">
  <h2 class="fs-title">Medical Detail</h2>
  <h3 class="fs-subtitle">We are concern about your wellbeing</h3>
                    <?php echo $this->Form->control('occupation', array('class'=>'form-control','placeholder'=>'Enter your Occupation')); ?> <br/>
                    <?php echo $this->Form->control('emergency', array('class'=>'form-control', 'type'=>'textarea','placeholder'=>'Enter Contact Name: Contact Number')); ?> <br/>
                    <?php echo $this->Form->control('medicareNum', array('class'=>'form-control')); ?> <br/>
                    <?php echo $this->Form->control('referenceNum', array('class'=>'form-control')); ?> <br/>
                    <?php echo $this->Form->control('doctor', array('class'=>'form-control')); ?> <br/>
                    <?php echo $this->Form->control('note', array('class'=>'form-control', 'type'=>'textarea')); ?> <br/>
  <input type="button" name="previous" class="previous button" value="Previous">
  <input type="button" name="next" class="next button" value="Next">
  </form>
  </div>
      
  <div class="form">
  <form action="">
  <h2 class="fs-title">Communication Preferences</h2>
  <h3 class="fs-subtitle">How do you want us to contact you</h3>
                    <?php echo $this->Form->control('reminder', array('class'=>'form-control', 'options' => $reminder , 'empty'=>'Choose')); ?> <br/>
                    <?php echo $this->Form->control('smsMarketing', array('class'=>'checkbox', 'type'=>'checkbox', 'value' => 'Subscribe', 'hiddenField' => 'Unsubscribe'));?>
                    <?php echo $this->Form->control('confirmation', array('class'=>'checkbox', 'type'=>'checkbox', 'value' => 'Subscribe', 'hiddenField' => 'Unsubscribe'));?>
  <input type="button" name="previous" class="previous button" value="Previous">
  <?php echo $this->Form->button(__('Submit'), array('class' => 'next button','div' => false, 'label' => false, 'type' => 'submit')); ?>
        
              <!--   <input type="button" name="submit" class="next button" value="Finish"> -->

  </form>
  </div>
  </div>
  
<script>
$(document).ready(function(){
    $.multistepform({
        container:'multistepform-example-container',
        form_method:'GET',
    })
});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>