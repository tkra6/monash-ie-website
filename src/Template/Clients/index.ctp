<div id="page-inner">
  <div class="row">
    <div class="col-md-12">
      <h3>
        <?= __('Clients') ?>
      </h3>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Clients
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Clients', 'action' => 'add']); ?></a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">


                  <thead>
                    <tr>

                      <th>
                        <?php echo "Title";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "First Name"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Last Name"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Email"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Gender"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Date Of Birth"; ?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>                      
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                        <tbody>
            <?php foreach ($clients as $client): ?>
            <tr>
                <td><?= h($client->title) ?></td>
                <td><?= h($client->firstName) ?></td>
                <td><?= h($client->lastName) ?></td>
                <td><?= h($client->email) ?></td>
                <td><?= h($client->gender) ?></td>
                <td><?= h($client->DOB) ?></td>            
                <td class="actions">
                        <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Clients', 'action' => 'edit', $client->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>


                        <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                    array('controller' => 'Clients', 'action' => 'delete',$client->id), 
                    array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete Client: '. $client->firstName)));
                    ?>
                  
                     <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-eye']).' View',['controller' => 'Clients', 'action' => 'view', $client->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>
                </td>
            </tr>
            <?php endforeach; ?>
          <?php echo $this->fetch('postLink');?>
        </tbody>
                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
  </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>