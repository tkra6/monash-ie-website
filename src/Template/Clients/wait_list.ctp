<?php include_once("analyticstracking.php") ?>
<!--  <div class="progress progress-striped">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
    <span class="sr-only">40% Complete (success)</span>
  </div>
</div> -->
<?php 
	//	get all appointment types stored in $results

	foreach($jsonAppTypes['appointment_types'] as $question)
	{
			for ($i = 0; $i < 1; $i++)
			{
					$appresults[] = $question['name'];

			}
	}

foreach($jsonPractitioners['practitioners'] as $question)
	{
			for ($i = 0; $i < 1; $i++)
			{
					$pracresults1[] = $question['first_name'];
					$pracresults2[] = $question['last_name'];
				
			}
	}



?>


<?php echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); ?>
<br/>
<br/>
<div id="fh5co-testimonial" class="fh5co-bgwhite">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Wait List</h2>
						</div>
					</div>
					<div class="row" style="margin: 0px 10px 10px 10px;">
						    <?= $this->Form->create($waitForm) ?>
      <!-- /. ROW  -->
      <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">

            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
									
                  <div class="panel panel-primary"  style="border-color: #F79211;" >
                    <div class="panel-heading" style= "background-color:#F79211; border-color: #F79211;">
                      Personal Details
                    </div>
                    <div class="panel-body">
<!--                       <?php //echo $this->Form->control('firstName', array('class'=>'form-control' ,'placeholder'=>'Enter your first name', 'type' => 'name')); ?> <br/>
                      <?php //echo $this->Form->control('lastName', array('class'=>'form-control','placeholder'=>'Enter your first name', 'type' => 'name')); ?> <br/>
                      <?php //echo $this->Form->control('email', array('class'=>'form-control','placeholder'=>'Enter your Email', 'type' => 'email')); ?> <br/> -->
											
											<div class="form-group">
													<label for="validate-text">First Name</label>
											    <div class="input-group"data-validate="name">
												   <?php echo $this->Form->control('firstName', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Your First Name', 'type' => 'text', required)); ?>
												     <span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
											</div>
										</div>
											
											<div class="form-group">
													<label for="validate-text">Last Name</label>
											    <div class="input-group"data-validate="name">
												   <?php echo $this->Form->control('lastName', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Your Last Name', 'type' => 'text', required)); ?>
												     <span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
											</div>
										</div>
											
											<div class="form-group">
													<label for="validate-email">Email</label>
											<div class="input-group" data-validate="email">
												<?php echo $this->Form->control('fromEmail', array('class' => 'form-control', 'id' => 'validate-email', 'label' => false, 'placeholder' => 'Your Email', 'type' => 'text', required)); ?>
												<span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
											</div>
										</div>

                    </div>
                  </div>
                  
                  <div class="panel panel-info" style="border-color: #F79211;">
                    <div class="panel-heading" style= "background-color:#F79211; border-color: #F79211;  color:white;">
                      Booking Preferences
                    </div>
                    <div class="panel-body">	
<!-- 											<?php //echo $this->Form->control('appointmentType', array('class'=>'form-control', 'required'=>'required', 'options' => $appresults, 'value' => $appresults, 'empty'=>'Choose')); ?> <br/> -->
											<div class="form-group">
													<label for="validate-text">Appointment Type</label>
											    <div class="input-group"data-validate="name">
												   <?php echo $this->Form->control('appointmentType', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Appointment Type', 'type' => 'text', required)); ?>
												     <span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
											</div>
										</div>
											
											<div class="checkbox-group">
<!-- 												<h4>Preferred Practioner</h4> -->
												<br/><label for="validate-text">Preferred Practioner</label>
												
											<ul>	
											<?php $counter = 0;
												foreach ($pracresults1 as $key => $content) {
												$contentb = $pracresults2[$key];
	
											?> <li> <?php echo $this->Form->control('prac', array('class'=>'radio', 'label' => $content . ' ' . $contentb, 'type'=>'checkbox', 'value' => $content . ' ' . $contentb, 'hiddenField' => 'None'));?></li><br/><?php
											$counter = $counter + 1;
											} ?>
												</ul>
											<div class="checkbox-group">
<!-- 												<br/><h4>Availability</h4> -->
												<label for="validate-text">Availability</label>
											<ul>
												
												<li><?php echo $this->Form->control('Monday', array('class'=>'weekday', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?></li>
												<li><?php echo $this->Form->control('Tuesday', array('class'=>'weekday', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?></li>
												<li><?php echo $this->Form->control('Wednesday', array('class'=>'weekday', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?> </li>
												<li><?php echo $this->Form->control('Thursday', array('class'=>'weekday', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?></li>
												<li><?php echo $this->Form->control('Friday', array('class'=>'weekday', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?> </li>
												<li><?php echo $this->Form->control('Saturday', array('class'=>'weekday', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?> </li>
												</ul>
											</div>
																						
												<div class="checkbox-group"></br>
												<label for="validate-text">Additional Preferences</label>
<!-- 												<br/><h4>Additional Preferences</h4> -->
											<ul>
												<li><?php echo $this->Form->control('urgentAppointment', array('class'=>'additional_preferences', 'type'=>'checkbox', 'value' => 'Urgent', 'hiddenField' => 'Not Urgent'));?></li>
												<li><?php echo $this->Form->control('onlyOutsideBusinessHours', array('class'=>'additional_preferences', 'type'=>'checkbox', 'value' => 'Yes', 'hiddenField' => 'No'));?></li>
												</ul>
											</div>
											<br/>
											<div class="form-group">
													<label for="validate-text">Additional Requests</label>
											    <div class="input-group"data-validate="message">
												   <?php echo $this->Form->control('additionalRequests', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Additional Requests', 'type' => 'textarea', required)); ?>
												     <span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
											</div>
										</div>
<!-- 									<?php// echo $this->Form->control('additionalRequests', array('class'=>'form-control','type'=>'textarea', 'hidden' => 'None Given')); ?> <br/> -->
 									<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-right','div' => false, 'label' => false, 'type' => 'submit')); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
					</div>
	</div>
</div>

						
						
					
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<?php echo $this->Html->script('validationJava.js'); ?>
          