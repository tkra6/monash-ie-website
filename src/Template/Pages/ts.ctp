<?php include_once("analyticstracking.php") ?>
<!-- Page Content -->

<div class="container">
  <div class="row">
    <div class="col-md-4">
      <h2>
      FAQ
    </h2>
    <h4>
Q. Who should see a RMT practitioner?
      </h4>
     <p>
Anyone! Whether you are an athlete, office worker, parent or student – no matter what you do, if you are suffering soreness, tightness or pain in any muscle or joint, or if something is just 'not quite right', an RMT session is for you.      </p>
      <h4>
Q. Can I claim RMT on Private Health Insurance cover?
        </h4>
      <p>
In most cases, yes. However, this will depend on the type of cover you have, which insurer you are with and how often you have claimed previously. We recommend that you check with your health fund as to what you are covered for, to avoid any unexpected costs.      </p>
    <h4>Q. What is the cancellation policy?
      </h4>
      <p>Please provide a minimum of 24 hours notice for any alterations to appointment times by telephone. This allows other clients the opportunity to book the specified time. 100% cancellation fee applies to cancellations without sufficient notice. This also applies if you do not attend your appointment and have not told us.
GIFT VOUCHER CANCELLATION POLICY - There are no refunds for cancellation of treatments that have been pre-paid with a gift vouchers. We are happy to reschedule your apppointment before the expiry date of the gift voucher. If you do not attend your appointment and have not told us or do not give a minimum of 24hours cancellation notice your gift voucher will be void.
        
      </p>
      <h4>
       Q. What if I am running late for my appointment?

     </h4>
     <p>
       Please inform us as early as possible if you are running late for you appointment. You will be seen but your appointment will be kept to your booking length as we have to be mindfull of other client booking times and you will be charged the full amount of the booked treatment.
     </p>
    </div>
   <div class="col-md-4">
      <h2>
   </h2>
    <h4>
Q. What should I bring/wear to a RMT appointment?
      </h4>
     <p>
You should bring any information, referrals or medication that is relevant to your health and current musculoskeletal complaint to your appointment. Wearing firm fitting underwear is a must.
     <h4>
Q. Will my massage practitioner keep my information private?
        </h4>
      <p>
As regulated health professionals, massage practitioners are required as a part of the standards set by the Australian Association of Massage Therapists (AAMT) to maintain the information you provide, both verbally and in written form, in the strictest of confidence.
        Information that is collected about you may be collected only with consent, may only be disclosed with consent or to your immediate health providers (circle of care), and must be secured and maintained.</p>
     <h4>
       Q. What happens on the first visit?
     </h4>
     <p>
       Please arrive 10 minutes before your scheduled service and complete a confidential health history as part of your assessment. The massage practitioner will listen to your concerns, assess your individual needs as well as other factors that may be contributing to your injury (lifestyle, nutritional status, etc.). A treatment plan will be developed with you to ensure you receive appropriate treatment that will help you return, as much as possible, to your normal activities. Registered massage practitioners will also describe the treatments to be provided to ensure that you are comfortable with them. Your consent is sought before treatment is provided. 
     </p>
    </div>
     <div class="col-md-4">
      <h2>
    </h2>
    <h4>
Q. What are your payment policies?
      </h4>
     <p>
All payments are paid in Cash. Full payment is required on the day for all appointments including WorkCover. Private health insurance - you may receive a rebate once you submit your receipt.
       <h4>
Q. WorkCover
        </h4>
      <p>
        Please bring any information, case manager details and referrals that are relevant to your health and current musculoskeletal complaint to your appointment. Full payment is required on the day of all appointments. You will recieve a receipt and a copy is also emailed to your case manager to ensure you recieve a speedy reimbursement.</p>
       <h4>
       Q. How frequently should I come for massage treatments?
     </h4>
     <p>
       The frequency of visits for massage therapy depends on the condition being treated. Therefore, it is on a case specific basis. In general, specific injuries will require regular visits for a designated period of time to restore the normal function of the area. The massage practitioner will discuss a treatment schedule with you once the problem area has been properly assessed. Those people who do not have a specific injury per se however seek massage treatments for maintenance or preventative reasons generally will require 1-2 visits per month. This again is a guideline and may require modification depending on the patient.
 
We strive to maintain your optimum personal comfort levels so please let us know if you have any concerns or issues at the beginning of the session.
     </p>
    </div>
  </div>
</div>