<?php include_once("analyticstracking.php") ?>
<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'); ?>
<?php echo $this->Html->css('bootstrap.css'); ?>
<?php echo $this->Html->css('templatemo_style.css'); ?>	

<?php echo $this->Html->css('testimonialHome.css'); ?>
<?php echo $this->Html->script(array('jquery.min.js','bootstrap.min.js', 'stickUp.min.js', 'colorbox/jquery.colorbox-min.js', 'templatemo_script.js')); ?>
<?php echo $this->Html->script('testimonialHome.js'); ?>

								<?php
										function limit_words($string, $word_limit){
												$words = explode(" ",$string);
												return implode(" ",array_splice($words,0,$word_limit));
										} ?>

        <?php foreach ($pagecontents as $pagec): ?> 
<?php if ($pagec['section'] == 'welcome') { ?>
<!-- Carousel -->
		<div id="templatemo-carousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#templatemo-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#templatemo-carousel" data-slide-to="1"></li>
				<li data-target="#templatemo-carousel" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<div class="container">
						<div class="carousel-caption">
							<h1>Re-Creating Functional Pain Free Bodies</h1>
							<p>Discover how you can recover from injuries faster, relieve pain and aid in your body's muscle recovery time. Book your appointment today </p>
							<p><a class="btn btn-lg btn-green" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>" role="button" style="margin: 20px;">Book Now</a>
								<a class="btn btn-lg btn-orange" href="<?php echo $this->url->build(array('controller' => 'clients', 'action' => 'wait-list')) ?>" role="button">Wait List</a></p>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<div class="col-sm-6 col-md-6">
								<h1>Address</h1>
								<p>135 Gardenvale Road, Gardenvale Victoria 3185 (moved from Elsternwick location August 2016)</p>
							</div>
							<div class="col-sm-6 col-md-6">
								<h1>Transport</h1>
								<p>Conveniently located close to Gardenvale Train Station, parking is located in the surrounding streets to Muscleworks Massage clinic location and we always advice to arrive early to walk into clinic relaxed.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<h1>Gift Voucher</h1>
							<p>Massage is a gift that keeps on giving. By purchasing a gift voucher the intended recipient has the choice to select the service they prefer upon booking an appointment. </p>
							<p><a class="btn btn-lg btn-orange" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>" role="button">Buy Now</a></p>
						</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#templatemo-carousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#templatemo-carousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
		<!-- /#templatemo-carousel -->	
	


<div class="templatemo-welcome" id="templatemo-service">
		<div class="container">
			<div class="templatemo-slogan text-center">
				<span class="txt_darkgrey">Welcome to </span><span class="txt_orange">MUSCLEWORK MASSAGE</span>
				<p class="txt_slogan"><i>Re-Creating Functional Pain Free Bodies</i></p>
			</div>
		</div>
	</div>


<?php } elseif (($pagec['section'] == 'service')) { ?>	

	<div class="templatemo-service" id="templatemo-service">
		<div class="container">
			<div class="row">

				<div class="col-md-4">
					<div class="templatemo-service-item">
						<div>
							<img alt="icon" <?php echo $this->Html->image('leaf.png'); ?> <img/>
							<span class="templatemo-service-item-header">Remedial Massage</span>
						</div>
						<p>Remedial massage is the application of different techniques in the treatment of muscular pain and dysfunction that affect human movement. It is a form of treatment that is applied in the preventative, corrective and rehabilitative phases of treatment.
							Remedial Massage is concerned with the restoration and maintenance of the soft tissue structures of the body and can be used for relaxation purposes also</p>
						<div class="text-center">
							<a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'services')) ?>" class="templatemo-btn-read-more btn btn-orange">READ MORE</a>
						</div>
						<br class="clearfix" />
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-md-4">
					<div class="templatemo-service-item">
						<div>
							<img alt="icon" <?php echo $this->Html->image('mobile.png'); ?> <img/>
							<span class="templatemo-service-item-header">Pre & Postnatal Massage</span>
						</div>
						<p>Nausea, Fatigue, Anxiety and muscle soreness can put mums to be under added pressure and often lead to neck, shoulder, mid and low back pain. Massage during pregnancy can include specific light and deep pressure massage, myofascial techniques, circulatory
							work and acupressure. Postnatal massage can help relax, decrease stress, improve relaxation and unwind, aiding in improved sleep and easing muscles aches and pains.</p>
						<div class="text-center">
							<a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'services')) ?>" class="templatemo-btn-read-more btn btn-orange">READ MORE</a>
						</div>
<!-- 						<br class="clearfix" /> -->
					</div>
				</div>

				<div class="col-md-4">
					<div class="templatemo-service-item">
						<div>
							<img alt="icon" <?php echo $this->Html->image('battery.png'); ?> <img/>
							<span class="templatemo-service-item-header">Sports Massage</span>
						</div>
						<p>Prepare for your next major sporting event with an energising massage to revitalise tired muscles and minimise the risk of a sporting injury. Recovery massage is used to restore your muscles and release the build up of lactic acid. Sports massage
							ensures that the mobility and flexibility of muscles, tendons and joints are optimised pre and post event.</p>
						<div class="text-center">
							<a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'services')) ?>" class="templatemo-btn-read-more btn btn-orange">READ MORE</a>
						</div>
<!-- 						<br class="clearfix" /> -->
					</div>
				</div>
			</div>
		</div>
	</div>

<?php } elseif (($pagec['section'] == 'team')) { ?>	

	<div class="templatemo-team" id="templatemo-teaminfo">
		<div class="container">
			<div class="row">
				<div class="templatemo-line-header">
					<div class="text-center">
						<hr class="team_hr team_hr_left" /><span>MEET OUR TEAM</span>
						<hr class="team_hr team_hr_right" />
					</div>
				</div>
			</div>
			<ul class="row row_team">
				<?php foreach ($theTeam as $theTeams): ?>
				<li class="col-lg-3 col-md-3 col-sm-6 ">
					<div class="text-center">
						<div class="member-thumb">
							<img class="img-responsive" <?php echo ($theTeams->image);?> <img/>
							<div class="thumb-overlay">
								<a href="#"><span class="social-icon-fb"></span></a>
								<a href="#"><span class="social-icon-twitter"></span></a>
								<a href="#"><span class="social-icon-linkedin"></span></a>
							</div>
						</div>
						<div class="team-inner">
							<p class="team-inner-header"><?php echo ($theTeams->detail);?></p>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<!-- /.templatemo-team -->
<br class="clearfix" /><br/>
<br/>

<?php } elseif (($pagec['section'] == 'blog')) { ?>	

	<div id="templatemo-blog">
		<div class="container">
			<div class="row">
				<div class="templatemo-line-header" style="margin-top: 0px;">
					<div class="text-center">
						<hr class="team_hr team_hr_left hr_gray" /><span class="span_blog txt_darkgrey">BLOG POSTS</span>
						<hr class="team_hr team_hr_right hr_gray" />
					</div>
				</div>
				<br class="clearfix" />
			</div>

			<div class="blog_box">
				 <?php foreach ($blogPost as $blogPosts): ?>
				<div class="col-sm-5 col-md-6 blog_post">
					<ul class="list-inline">
						<li class="col-md-4">
							<a href="#">
								<img class="img-responsive" alt="gallery 1" <?php echo $this->Html->image('woman-stretching.jpg'); ?> <img/>
							</a>
						</li>
						<li class="col-md-8">
							<div class="pull-left">								
								<span class="blog_header"><?php echo $blogPosts->title; ?></span><br/>
								<span>Posted on : <a class="link_orange"><span class="txt_orange"><?php echo ($blogPosts->modified);?></span></a>
								</span>
							</div>
							<div class="pull-right">
								<a class="btn btn-orange" href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'blog')) ?>" role="button">Read More</a>
							</div>
							<div class="clearfix"> </div>
							<p class="blog_text">
								<?php $text = preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags(html_entity_decode($blogPosts->body)));
								//echo $clear;?>
								<?php
										$start = limit_words($text,25);
										$end = str_replace($start, '', $text);
										?>

							<?php echo $start; ?>
							</p>
						</li>
					</ul>
				</div>
				<?php endforeach; ?>
				<!-- /.blog_post 1 -->
			</div>
		</div>
	</div>

<br class="clearfix" /><br/>
<br/>

<?php } elseif (($pagec['section'] == 'testimonial')) { ?>	

	<div id="templatemo-testimonial">
		<div class="container">
			<div class="row">
				<div class="templatemo-line-header" style="margin-top: 0px;">
					<div class="text-center">
						<hr class="team_hr team_hr_left hr_gray" /><span class="txt_darkgrey">TESTIMONAL</span>
						<hr class="team_hr team_hr_right hr_gray" />
					</div>
				</div>


				<div class="col-md-8"> 
				 <!--Testimonials starting-->
            <div id="testimonials">                
                <h2 id="dev-snippet-title" class="text-center">Testimonials</h2>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-1">
                            <div class="testimonials-list">
                            <?php foreach ($theTestimonial as $theTestimonials): ?> 
                                 <!-- Single Testimonial -->  
                                <div class="single-testimonial">
                                    <div class="testimonial-holder">
                                        <div class="testimonial-content"> <?php echo ($theTestimonials->content);?>
                                        </div>
                                        <div class="row">
                                            <div class="testimonial-user clearfix">
                                                <div class="testimonial-user-image"><img <?php echo $this->Html->image('icon-newclient.png'); ?></img></div>
                                                <div class="testimonial-user-name"><?php echo ($theTestimonials->name);?><br>
																							  <?php echo $theTestimonials->has('service') ? ($theTestimonials->service->name) : '' ?>
																								</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
															<?php endforeach; ?>
                                <!-- End of Single Testimonial -->  
                                                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
				</div>
        <!--End of Testimonials -->

				
				<div class="col-md-4 contact_right">
					<p>Your feedback is important for us, feel free to add a testimonial by pressing the button below.</p>
					<p><a class="btn btn-lg btn-orange" href= "<?php echo $this->url->build(array('controller' => 'testimonials', 'action' => 'add1')) ?>" role="button">Add Testimonial</a></p>

				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</div>
<?php  } else { ?>
<div class="templatemo-welcome" id="templatemo-service">
		<div class="container">
			
						<div class="row">
				<div class="templatemo-line-header" style="margin-top: 0px;">
					<div class="text-center">
						<hr class="team_hr team_hr_left hr_gray" /><span class="txt_darkgrey"><?php echo $pagec['section']; ?></span>
						<hr class="team_hr team_hr_right hr_gray" />		
				
				<p class="txt_slogan"><i> 	
					<div class="col-md-8"> <h2>
						<?php
				echo $pagec['content']; ?> </i></p> </h2>
					 
        <!-- /.row -->
      </div>
				
			</div>
		</div>
</div>
</div>
</div>
<?php } endforeach; ?>

	<!-- /#templatemo-contact -->
<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us4.list-manage.com","uuid":"0c1143d5d9b13de1a538ee44b","lid":"189c70f228"}) })</script>