<?php include_once("analyticstracking.php") ?>
<?php echo $this->Html->css('aSpecialBar.css'); ?>

<?php $counter = 0; ?>
<?php foreach ($theService as $theServices): ?>
<?php $counter = $counter + 1; ?>
<?php endforeach;?>
<?php $width = 100/$counter; ?>
<?php echo $this->Html->script('ajquery.min.js'); ?>

<script>
	$(function() {
		$('a[title]').tooltip();
	});
</script>

<section class="fh5co-bg" class="fh5co-bg-sectionwhite">
<!-- 	<div id="fh5co-services" class="fh5co-bg-section"> -->
	<div class="container">
		<div class="row">
			<div class="board">
				<!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
				<div class="board-inner">
					<ul class="nav nav-tabs" id="myTab">
						
						<?php $count =0;?>
						<?php foreach ($theService as $theServices): ?>
						<?php
														$input = $theServices->image;
														preg_match("%src=\"([^\"])+(png|jpg|gif|jpeg)\"%i",$input,$result);
												?>
							<?php if ($count==0) { ?>

							<?php foreach ($pagecontents as $pagec): ?>
							<?php if (($pagec->section) == ($theServices->main)) { ?>

							<li class="active" style="width: <?php echo $width; ?>%;">
								<a href="#<?php echo preg_replace('/\s*/', '', strtolower($pagec->section)); ?>" data-toggle="tab" title="<?php echo $pagec->section; ?>">
											
                      <span class="round-tabs two">
												<i><img class="img-responsive" <?php echo $result[0]; ?> id = "mobileBar" style="display: block; margin: 0 auto; border-radius: 100px; height:70px; width:70px;"</img></i>
                      </span> 
                     </a>

							</li>
							<?php } endforeach; ?>
							<?php $count= $count +1;?>

							<?php } else { ?>
							<?php foreach ($pagecontents as $pagec): ?>
							<?php if (($pagec->section) == ($theServices->main)) { ?>
							<li style="width: <?php echo $width; ?>%;"><a href="#<?php echo preg_replace('/\s*/', '', strtolower($pagec->section)); ?>" data-toggle="tab" title="<?php echo $pagec->section; ?>">
                     <span class="round-tabs two">
											 <div id = "mobileBar1" >
                        	<i><img class="img-responsive" <?php echo $result[0]; ?> style="display: block; margin: 0 auto; border-radius: 100px; height:70px; width:70px;"</img></i>
											 </div>
												 </span> 
          			 </a>
							</li> 
							<?php } endforeach; ?>
							<?php } endforeach; ?>
					
					</ul>
				</div>

				<div class="tab-content" >
					<?php $myCount =0;?>
					<?php foreach ($theService as $theServices): ?>
					<?php if ($myCount == 0) { ?>
					<?php foreach ($pagecontents as $pagec): ?>
					<?php if (($pagec->section) == ($theServices->main)) { ?>
					<div class="tab-pane fade in active" id="<?php echo preg_replace('/\s*/', '', strtolower($pagec->section)); ?>">


						<p class="narrow">
							<?php echo ($pagec->content); ?>
						</p>

					</div>

					<?php } endforeach?>
					<?php $myCount = $myCount +1; ?>
					<?php } else { ?>
					<?php foreach ($pagecontents as $pagec): ?>
					<?php if (($pagec->section) == ($theServices->main)) { ?>
					<div class="tab-pane fade" id="<?php echo preg_replace('/\s*/', '', strtolower($pagec->section)); ?>">
						
						<p class="narrow">
							<?php echo $pagec->content; ?>
						</p>

					</div>

					<?php } endforeach; ?>
					<?php } endforeach; ?>

					
					<div class="clearfix"></div>
				</div>

			</div>
		</div>
	</div>
	<br/>
					<br/>
	<br/>
<!-- </section> -->
</div>
