<?php include_once("analyticstracking.php") ?>
		<?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800'); ?>
		<?php echo $this->Html->css('aanimate.css'); ?>
		<?php echo $this->Html->css('aicomoon.css'); ?>
		<?php echo $this->Html->css('abootstrap.css'); ?>
		<?php echo $this->Html->css('amagnific-popup.css'); ?>
		<?php echo $this->Html->css('aowl.carousel.min.css'); ?>
		<?php echo $this->Html->css('aowl.theme.default.min.css'); ?>
		<?php echo $this->Html->css('astyle.css'); ?>

		<?php echo $this->Html->script('amodernizr-2.6.2.min.js'); ?>

								<?php
										function limit_words($string, $word_limit){
												$words = explode(" ",$string);
												return implode(" ",array_splice($words,0,$word_limit));
										} ?>

        <?php foreach ($pagecontents as $pagec): ?> 
<?php if ($pagec['section'] == 'Main Slideshow') { ?>

<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->
  <div class="overlay"></div>

  <!-- Indicators -->
					
	
  <ol class="carousel-indicators" style = "bottom:40px;">
			<?php $countMain = 0; ?>
						<?php foreach ($theMain as $theMains): ?>
		<?php if ($countMain == 0  ) { ?>
    <li data-target="#bs-carousel" data-slide-to="<?php echo $countMain; ?>" class="active"></li>
  
	  <?php $countMain = $countMain +1;  ?>
	   <?php } else { ?>
		    <li data-target="#bs-carousel" data-slide-to="<?php echo $countMain; ?>"></li>
		<?php $countMain = $countMain +1; } ?>
    <?php endforeach; ?>
		</ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
		
		<?php $countMainContent = 0; ?>
						<?php foreach ($theMain as $theMains): ?>
						<?php
								$input = $theMains->image;
								preg_match("%src=\"([^\"])+(png|jpg|gif|jpeg|svg)\"%i",$input,$result);
						?>
			<?php $abcd = $result[0]; ?>
			<?php $test123 = str_replace('src="','', $abcd ); ?>
		<?php if ($countMainContent == 0  ) { ?>
		
    <div class="item slides active">
      <div class="slide-1" style = "<?php echo "background-image:url(".$test123.");";?>"</di>></div>
      <div class="hero">
        <hgroup>
            <h2><?php echo ($theMains->main); ?></h2>        
            <h4><?php echo ($theMains->detail); ?></h4>
        </hgroup>
      </div>
    </div>
		 <?php $countMainContent = $countMainContent +1;  ?>
	   <?php } else { ?>
		
    <div class="item slides">
      <div class="slide-1" style = "<?php echo "background-image:url(".$test123.");";?>"></div>
      <div class="hero">        
        <hgroup>
            <h2><?php echo ($theMains->main); ?></h2>        
            <h4><?php echo ($theMains->detail); ?></h4>
        </hgroup>       
        
      </div>
    </div>
		<?php $countMain = $countMain +1; } ?>
    <?php endforeach; ?>
						<a class="left carousel-control" href="#bs-carousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
					<a class="right carousel-control" href="#bs-carousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div> 
</div>



<?php } elseif (($pagec['section'] == 'Service')) { ?>	

			<div id="fh5co-services" class="fh5co-bg-section">
				<div class="container">
					<div class="row">
						<?php $countService = 1; ?>
						<?php foreach ($theService as $theServices): ?>
						<?php
								$input = $theServices->image;
								preg_match("%src=\"([^\"])+(png|jpg|gif|jpeg|svg)\"%i",$input,$result);
						?>
						
						<?php if ($countService < 4 ) { ?>
						<div class="col-md-4 text-center animate-box">
							<div class="services">
								<span><img class="img-responsive" <?php echo $result[0]; ?> style="display: block; margin: 0 auto; border-radius: 100px; height:70px; width:70px;"</img></span>
								<h3><?php echo $theServices->main; ?></h3>
								<p><?php echo $theServices->detail; ?></p>
								<p><a href="<?php echo $this->url->build(array('controller' => 'Pages', 'action' => 'services')) ?>" class="btn btn-primary btn-outline btn-sm">More <i class="icon-arrow-right"></i></a></p>
							</div>
						</div>
						<?php $countService = $countService +1; ?>
						<?php } ?>
						<?php endforeach; ?>
						</div>
					</div>
					</div>


<?php } elseif (($pagec['section'] == 'Team')) { ?>	

			<div id="fh5co-trainer">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							
							<h2><?php echo $pagec->heading; ?></h2>
						</div>
					</div>
					
					<div class="container">
                    <div class="row">
												<?php foreach ($theTeam as $theTeams): ?>
												<?php
														$input = $theTeams->image;
														preg_match("%src=\"([^\"])+(png|jpg|gif|jpeg)\"%i",$input,$result);

												?>
                        <div class="col-md-3 col-sm-3">
                            <div class="team-member">
                                <div class="team-img">
                                    <img class="img-responsive" <?php echo $result[0]; ?> style="display: block; margin: 0 auto; height:300px; width:300px;"</img>
                                </div>
                                <div class="team-hover">
                                    <div class="desk">
                                        <p>My Name is <?php echo ($theTeams->main);?>. <?php echo ($theTeams->detail);?></p>
                                    </div>
                                    <div class="s-link">
                                        <button type="button" class="btn btn-primary btn-outline btn-sm"><a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'team/#maya')) ?>">Read More</a></button>
                                    </div>
                                </div>
                            </div>
                            <div class="team-title">
                                <h5 style="text-align:center;"><?php echo ($theTeams->main);?></h5>
                            </div>
                        </div>
											<?php endforeach; ?>
                    </div>
                </div>
				</div>
			</div>

<?php } elseif (($pagec['section'] == 'Testimonial')) { ?>	

			<div id="fh5co-testimonial" class="fh5co-bg-sectionwhite">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2><?php echo $pagec->heading; ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="row animate-box">
								<div class="owl-carousel owl-carousel-fullwidth">
									<?php foreach ($theTestimonial as $theTestimonials): ?>
									<div class="item">
										<div class="testimony-slide active text-center">
											<figure>
												<img <?php echo $this->Html->image('icon-newclient.png'); ?></img>
											</figure>
											<span><?php echo ($theTestimonials->name);?></span>
											<span><a class="twitter">Service: <?php echo $theTestimonials->has('service') ? ($theTestimonials->service->name) : '' ?></a></span>
											<blockquote>
												<p>&ldquo;
													<?php echo ($theTestimonials->content);?>&rdquo;</p>
												<p><a href="<?php echo $this->url->build(array('controller' => 'testimonials', 'action' => 'add1')) ?>" class="btn btn-primary btn-outline btn-lg">Add Testimonial <i class="icon-arrow-right"></i></a></p>
											</blockquote>
										</div>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

<?php } elseif (($pagec['section'] == 'VourcherPrice')) { ?>


				<div id="fh5co-schedule" class="fh5co-bg" style="background-image: url(/img/graybg.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2><?php echo $pagec->heading; ?></h2>
						</div>
					</div>

					<div class="row animate-box">

						<div class="fh5co-tabs">
							<ul class="fh5co-tab-nav">
								<?php $lengthPrice = 0; ?>
								<?php foreach ($thePricetype as $thePricetypes): ?>
								<?php $lengthPrice = $lengthPrice + 1; ?>
								<?php endforeach; ?>
								<?php $priceSize = 100 / ($lengthPrice); ?>
								
								<?php $count =0; ?>
								<?php foreach ($thePricetype as $thePricetypes):  								
								$mobileHeading = $thePricetypes->name;
								$mobileHead = ucfirst(limit_words($mobileHeading, 1)); 
								//$mobileHead = substr($mobileHeading, 0, 3); ?>
								
							
								<?php if ($count == 0) { ?>
								<li class="active" style =" <?php echo "width:".$priceSize."%"; ?>" id = "<?php echo ($thePricetypes->id); ?>">
									<a href="#" data-tab="<?php echo ($thePricetypes->id); ?>"><span class="visible-xs" style = "font-size:2.5vw;"><?php echo $mobileHead; ?></span><span class="hidden-xs"><?php echo ucfirst($thePricetypes->name); ?></span></a></li>
 												
								
								<?php $count=$count+1; } else { ?> 
								<li style =" <?php echo "width:".$priceSize."%"; ?>" id = "<?php echo ($thePricetypes->id); ?>"><a href="#" data-tab="<?php echo ($thePricetypes->id); ?>"><span class="visible-xs"style = "font-size:2.5vw;"><?php echo $mobileHead; ?></span><span class="hidden-xs"><?php echo ucfirst($thePricetypes->name); ?></span></a></li>
								
								<?php } endforeach; ?>
							</ul>
							
							<?php $stack = array(); ?>
							<!-- Tabs -->
							<?php foreach ($thePrice as $thePrices): $type= $thePrices->pricetypes_id; array_push($stack, ($type)); $num=(array_unique($stack));?>
						  <?php endforeach; ?>

							<div class="fh5co-tab-content-wrap">
								<?php for($counter = 0; $counter < sizeof($num); $counter++) { ?>
								<?php	$value = array_slice($num, $counter, 1); ?>
												
								<div id = "test<?php echo current($value); ?>" class="fh5co-tab-content tab-content <?php if ($counter == 0) { echo 'active';} ?>" data-tab-content="<?php echo current($value); ?>">
									<ul class="class-schedule">
										<?php foreach ($thePrice as $thePrices): if ($thePrices->pricetypes_id == (current($value))) { ?>
										<li class="text-center">
											<div class="price"><sup class="currency">$</sup><?php echo ($thePrices->value); ?><small></small></div>
											<h4><?php echo "<br>".($thePrices->minutes); ?></h4>
<!-- 											<button type="button" class="btn btn-primary btn-outline btn-sm"><a href="<?php //echo $this->url->build(array('controller' => 'vouchers', 'action' => 'details', $thePrices->id)) ?>">Purchase</a></button> -->
										</li>
										<?php } endforeach?>
									</ul>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

<?php } elseif (($pagec['section'] == 'Blog')) { ?>	
	
			<div id="fh5co-blog" class="fh5co-bg-section">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2><?php echo $pagec->heading; ?></h2>
						</div>
					</div>
					<div class="row row-bottom-padded-md">

						<?php foreach ($blogPost as $blogPosts): ?>
						<div class="col-lg-4 col-md-4">
							<div class="fh5co-blog animate-box">
								<a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'blog')) ?>"><img class="img-responsive" <?php echo $this->Html->image('backgpic.jpg'); ?> <img/></a>
								<div class="blog-text">
									<h3><a href="#"><?php echo $blogPosts->title; ?></a></h3>
									<span class="posted_on"><?php echo ($blogPosts->modified);?></span>
									<!-- 							<span class="comment"><a href="">21<i class="icon-speech-bubble"></i></a></span> -->
									<p>
										<?php $text = preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags(html_entity_decode($blogPosts->body))); ?>
										<?php
										$start = limit_words($text,20);
										$end = str_replace($start, '', $text);
										?>

											<?php echo $start; ?>
									</p>
									<a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'blog')) ?>" class="btn btn-primary btn-outline btn-md">Read More</a>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			
<?php } elseif (($pagec['section'] == 'Ready')) { ?>	

					<div id="fh5co-started" class="fh5co-bg-section">
						<div class="overlay"></div>
						<div class="container">
							<div class="row animate-box">
								<div class="col-md-8 col-md-offset-2 text-center">
									<?php foreach ($theReady as $theReadys): ?>
									<h2><?php echo $theReadys->main; ?></h2>
									<p><?php echo $theReadys->detail; ?></p>
									<?php endforeach ?>
								</div>
							</div>
							<div class="row animate-box">
								<div class="col-md-8 col-md-offset-2 text-center">
									<p><a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>" class="btn btn-primary btn-outline btn-lg">Book Now</a></p>
								</div>
							</div>
						</div>
					</div>

<?php  } else { ?>
			<div id="fh5co-started" class="fh5co-bg" class="fh5co-bg-section">
				<div class="overlay"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2><?php echo $pagec->heading; ?></h2>
						</div>
					</div>
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center">
								<p><?php echo $pagec->content; ?></p>
						</div>
					</div>
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center">
<!-- 							<button type="button" class="btn btn-primary btn-outline btn-sm"><a href="<?php //echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>" >Book Now</a></button> -->
							<p><a href="<?php echo $this->url->build(array('controller' => 'pages', 'action' => 'book')) ?>" class="btn btn-default btn-lg">Book Now</a></p>
						</div>
					</div>
				</div>
			</div>
<?php } endforeach; ?>



	<!-- /#templatemo-contact -->
<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us4.list-manage.com","uuid":"0c1143d5d9b13de1a538ee44b","lid":"189c70f228"}) })</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

$(".fh5co-tab-nav li").click(function() {
    if($(this).parent().data("lastClicked")){
			var output = '#test' + ($(this).parent().data("lastClicked"));
        $(output).removeClass('active fadeIn');

    }
    $(this).parent().data("lastClicked", this.id);
});
	
</script>