<?php include_once("analyticstracking.php") ?>
<!-- Page Content -->
<br/>
<br/>
<br/>
<br/>
<br/>
<section class="fh5co-bg" style="background-image: url(/img/graybg.jpg);">
<?php foreach ($pagecontents as $pagec): ?>
<div class="container">
  <div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-8">

      <?php foreach ($articles as $article): ?>

      <!-- First Blog Post -->
      <h2>
        <a><?= ucfirst($article->title) ?></a>
      </h2>
      <p class="lead">

      </p>
      <p><i class="fa fa-clock-o" aria-hidden="true"></i>
        <?= h($article->created) ?>
      </p>

      
      <?php echo ($article->body); ?>
      <br/>
      <hr style="height:1px;border:none;color:#333;background-color:#333;" />
      <?php endforeach; ?>

    </div>

    <!-- Blog Sidebar Widgets Column -->
    <div class="col-md-4">

     <!-- Blog Categories Well -->
      <div class="well">
        <h4>Blog Categories</h4>
        <?php foreach ($categories as $category): ?>
        <tr>
          <td>
            <ul class="list-unstyled">
              <!--               <li><?php //echo $category ? $this->Html->link($category, ['controller' => 'Articles', 'action' => 'tags', $category->id]) ?> -->
              <!--               <li><a href="<?php //echo $category; ?>"><?php //echo $category; ?></a> -->
              <?php //echo $category; ?>
              <li>
                <?= $category->has('category') ? $this->Html->link($category->category->name, ['controller' => 'Articles', 'action' => 'tags', $category->category->name]) : '' ?>
                <?php //echo $this->Html->link($category, ['controller' => 'Articles', 'action' => 'tags', $category]); ?>
              </li>
              </li>
            </ul>
          </td>

        </tr>
        <?php endforeach; ?>
      </div>

      <!-- Side Widget Well -->
      <div class="well">
        <h4>Side Widget</h4>
        
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMonash.University&tabs=timeline&width=320&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500"
          style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        
      </div>
    </div>
  </div>

  <!-- /.row -->
</div>
<?php endforeach; ?>
</section>

<!-- /.container -->

<!-- jQuery -->
