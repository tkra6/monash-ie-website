<?php include_once("analyticstracking.php") ?>
<?php
// in your view file
$this->Html->script('dropdown.js', array('inline' => false));
$this->Html->css('globalBooking', null, array('inline' => false));
$this->layout="booking";
?>

<p>
  Are you a new client? Please provide us with extra detail about yourself. 
  <a class="btn btn-lg btn-green" href="<?php echo $this->url->build(array('controller' => 'clients', 'action' => 'clientform')) ?>" role="button" style="margin: 20px;">Click Here</a>
  
  If you can't find a suitable booking, add yourself to our Waiting List. We'll return back to you as soon as we find an opening that suits your preferences.
  <a class="btn btn-lg btn-green" href="<?php echo $this->url->build(array('controller' => 'clients', 'action' => 'wait_list')) ?>" role="button" style="margin: 20px;">Wait List</a>
</p>

<!-- <p><iframe src="https://muscleworksmassage.cliniko.com/bookings?embedded=true" align="left" frameborder="0" scrolling="auto" width="975" height="1150"></iframe></p>