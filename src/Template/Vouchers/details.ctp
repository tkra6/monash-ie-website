<!-- 			      <div id="templatemo-testimonial">
            <div class="container">
                <div class="row">
                    <div class="templatemo-line-header" style="margin-top: 0px;">
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="txt_darkgrey">Voucher Detail</span>
                            <hr class="team_hr team_hr_right hr_gray"/>
                        </div>
                    </div>
          <div class="col-md-4 contact_right">
          <p>Please Fill out the form below with your information.</p>

          <?php //echo $this->Form->create($voucher) ?>

          <?php
            //echo $this->Form->control('fromName', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Your Name', 'type' => 'name'));
                     // echo "<br/>";
            //echo $this->Form->control('fromEmail', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Your Email', 'type' => 'email'));
                     // echo "<br/>";
           // echo $this->Form->control('phoneNumber', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Your Phone Number')); 
                    //  echo "<br/>";
						//echo $this->Form->control('toName', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Recipient', 'type' => 'texts')); 
            				//	echo "<br/>";
						//echo $this->Form->control('message', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Message', 'type' => 'textarea'));

                      //echo "<br/>";
						
           //echo $this->Form->button(__('Continue'), array('class' => 'btn btn-orange pull-right','div' => false, 'label' => false, 'type' => 'submit')); 
            //echo $this->Form->end(); ?>
        </div> -->
<?php include_once("analyticstracking.php") ?>
<?php echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); ?>
<br/>
<br/>
<div id="fh5co-testimonial" class="fh5co-bgwhite">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Fill in your details</h2>
						</div>
					</div>
					<div class="row" style="margin: 0px 10px 10px 10px;">
						<div class="col-md-10 col-md-offset-1">
				
				<?php echo $this->Form->create($voucher) ?>
							
				<div class="form-group">
        			<label for="validate-text">Name</label>
					<div class="input-group"data-validate="name">
						<?php echo $this->Form->control('fromName', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Your Name', 'type' => 'text', required)); ?>
<!-- 						<input type="text" class="form-control" name="validate-text" id="validate-text" placeholder="Validate Text" required> -->
						<span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
					</div>
				</div>
				<div class="form-group">
        			<label for="validate-email">Email</label>
					<div class="input-group" data-validate="email">
						<?php echo $this->Form->control('fromEmail', array('class' => 'form-control', 'id' => 'validate-email', 'label' => false, 'placeholder' => 'Your Email', 'type' => 'text', required)); ?>
<!-- 						<input type="text" class="form-control" name="validate-email" id="validate-email" placeholder="Validate Email" required> -->
						<span class="input-group-addon danger"><i class="fa fa-ban"style="color:#ff920a;"></i></span>
					</div>
				</div>
    			<div class="form-group">
        			<label for="validate-phone">Phone Number</label>
					<div class="input-group" data-validate="phone">
						<?php echo $this->Form->control('phoneNum', array('class' => 'form-control', 'id' => 'validate-phone', 'label' => false, 'placeholder' => 'Phone Number (with state code)', 'type' => 'text', required)); ?>
<!-- 						<input type="text" class="form-control" name="validate-phone" id="validate-phone" placeholder="(814) 555-1234" required> -->
						<span class="input-group-addon danger"><i class="fa fa-ban"style="color:#ff920a;"></i></span>
					</div>
				</div>
				<div class="form-group">
        			<label for="validate-text">Recipient Name</label>
					<div class="input-group" data-validate="name">
						<?php echo $this->Form->control('toName', array('class' => 'form-control', 'id' => 'validate-text' , 'label' => false, 'placeholder' => 'Recipient', 'type' => 'text', required));  ?>
<!-- 						<input type="text" class="form-control" name="validate-text" id="validate-text" placeholder="Validate Text" required> -->
						<span class="input-group-addon danger"><i class="fa fa-ban"style="color:#ff920a;"></i></span>
					</div>
				</div>
        		<div class="form-group">
        			<label for="validate-length">Message</label>
					<div class="input-group" data-validate="message">
						<?php echo $this->Form->control('message', array('class' => 'form-control', 'id' => 'validate-length', 'label' => false, 'placeholder' => 'Message', 'type' => 'textarea')); ?>
<!-- 						<textarea type="text" class="form-control" name="validate-length" id="validate-length" placeholder="Validate Length" required></textarea> -->
						<span class="input-group-addon danger"><i class="fa fa-ban"style="color:#ff920a;"></i></span></span>
					</div>
				</div>

            <?php  
						 	echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-right','div' => false, 'label' => false, 'type' => 'submit'));
							//echo $this->Form->button(__('Continue'), array('class' => 'btn btn-primary col-xs-12', 'type' => 'submit'));
							echo $this->Form->end(); ?>
        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<?php echo $this->Html->script('validationJava.js'); ?>


