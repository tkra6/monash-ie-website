<div id="page-inner">

      <div class="form-group">
        <?= $this->Form->create() ?>
          <fieldset>
           <h2>
             <legend><?= __('Send Email to Customer') ?></legend>
            </h2> 

<?php echo $this->Form->control('email', array('class'=>'form-control', 'value' => h($voucher->fromEmail))); ?> </br>
<?php echo $this->Form->submit(__('Submit')); ?>
<?php echo $this->Form->end(); ?>