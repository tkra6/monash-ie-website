<?php
//$pdf = new TCPDF('P', 'mm', array(99, 210), true);



// create new PDF document
//tcpdf loaded via Composer from https://packagist.org/packages/tecnick.com/tcpdf
$pdf = new TCPDF('P', PDF_UNIT, "A4", true);

// set document header information. This appears at the top of each page of the PDF document
//$pdf->SetHeaderData('', '',"Muscleworks Massage Voucher", '');

// set header and footer fonts
$pdf->setPrintHeader(false);



//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// add a page
$pdf->AddPage();


// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
//$img_file = K_PATH_IMAGES.'image_demo.jpg';
$pdf->Image('img/VoucherTemplate.png', 00, 00, 210, 297, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
// $pdf->setPageMark();

$pdf->Ln();

//$pdf->SetFillColor(114, 155, 120);


// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set background image
$img_file = K_PATH_IMAGES.'Voucher.png';
// $pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


//$pdf->Image('img/VoucherTemplate.png', 00, 00, 00, 00, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

// set margins
// $pdf->SetMargins(DF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);







//$phoneNumber = '9530 6400';

foreach ($thePhone as $thePhones): 
$phoneNumber = ($thePhones->detail);
endforeach; 

foreach ($theAddress as $theAddresss): 
$address = ($theAddresss->detail);
endforeach; 

foreach ($modality as $modalities): 
$mod = ($modalities->name);
endforeach; 

$header = array($voucher['UUID'], $voucher['toName'], $voucher['fromName'],  $voucher['message'], $voucher['price']['value'], $voucher['id']);

$pdf->SetXY(30,100);
$html = '
        <th><font size="25">'.$mod.'</font><br><font size="16">'  .$voucher['price']['minutes'].'</font></th>';
$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();

$pdf->SetXY(145,95);
$html = '
        <th><font size="20">Valued at </font><font size="30"><br>    $'.$voucher['price']['value'].'</font></th>';
$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();

$pdf->SetXY(25,150);
$html = '
        <th><font size="14"><b>To: </b>'.$voucher['toName'].'<br><br><b>From: </b>'.$voucher['fromName'].'<br><br><b>Message: </b>'.$voucher['message'].'</font></th>';

$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();

$pdf->SetXY(80,255);
$html = '
        <th><font size="8">The voucher is valid for 3 months. Vouchers are non-refundable and cannot be honoured after the expiry date, nor redeemed for cash or credit.</font></th><br>';
$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();

$pdf->SetXY(80,265);
$html = '
        <th><font size="8">Address: '.$address.' Phone: '.$phoneNumber.'</font></th><br>';
$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();

$pdf->SetXY(80,250);
$html = '
        <th><font size="8">Voucher Expiry Date: '.$voucher['expiry'].'</font></th><br>';
$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();



//Close and output PDF document
// $filename = location_on_server."voucher.pdf";

// $fileatt = $pdf->Output($filename, 'F');

// $data = chunk_split( base64_encode(file_get_contents($filename)) );

// //$pdf->Output(__DIR__ . '/PDFVouchers/Voucher-'.$header[0].'.pdf', 'F');
$pdf->Output(__DIR__.'/PDFVouchers/Voucher-'.$header[0].'.pdf', 'F');

// ob_clean();


$pdf->Output('Voucher-'.$header[0].'.pdf', 'I');




//$pdf->Output('voucher_'.$voucher['id'].'.pdf', 'D');

//   $pdf = write(APP . 'webroot'. DS .'files' . DS . 'voucher_'.$voucher['id'].'.pdf');
//   $pdf = APP . 'webroot'. DS .'files' .  DS . 'voucher_'.$voucher['id'].'.pdf';
exit();
?>