<?php echo $this->Html->css('payment_options.css'); ?>
<?php echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); ?>
<?php include_once("analyticstracking.php") ?>

<div id="fh5co-testimonial" class="fh5co-bgwhite">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Choose your payment option</h2>
						</div>
					</div>
  
<div class="row" style="margin: 0px 10px 10px 10px;">    						<div class="col-md-3 col-md-offset-3">
                  <form name="_xclick" 
								action="https://www.sandbox.paypal.com/cgi-bin/webscr"
								method="post">
							<input type="hidden" name="cmd" value="_xclick">
							<input type="hidden" name="upload" value="1">
							<input type="hidden" name="lc" value="AU">
							<input type="hidden" name="business" value="sandboxemail@test.com"> <!-- muscleworksmassage@outlook.com is the actual business account-->
							<input type="hidden" name="currency_code" value="AUD">
							<input type="hidden" name="cancel_return" value="http://team16-aneo3300752.codeanyapp.com/vouchers/unsuccessful">
							<input type="hidden" name="item_name" value="GIFT VOUCHER">
							<input type="hidden" name="amount" value="<?php echo $value; ?>">
							<input type="hidden" name='no_shipping' value="0">
							<input type="hidden" name='rm' value="2">
							<input type="hidden" name="bn" value="PP-BuyNowBF">
							<input type="hidden" name="custom" value="mycustomvalue">
							<input type="hidden" name="return" value="http://team16-aneo3300752.codeanyapp.com/vouchers/view-pdf-download"/>
							<input type="image" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_200x51.png" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.sandbox.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">
					</form> 
    </div>
		<div class="col-md-3">
			Please note: Paypal accepts payments with credit card, an account is not needed to purchase through Paypal.
		</div>
					</div>
	</div>
								<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center">
							<br><h3>OR</h3>
						</div>
					</div>
<div class="row" style="margin: 0px 10px 10px 10px;">				
        						<div class="col-md-3 col-md-offset-3">
<a href="<?php echo $this->url->build(array('controller' => 'vouchers', 'action' => 'phonePayment')) ?>"><img class="img-responsive" src="/img/phonepaymentvs.png" alt="Phone Payment"></a>
																</div>
				<div class="col-md-3">
			
		</div>

    </div>
	</div>
</div>

  
