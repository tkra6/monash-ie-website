<?php echo $this->Html->css('payment_options.css'); ?>
<?php echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); ?>
<?php include_once("analyticstracking.php") ?>

<div id="fh5co-testimonial" class="fh5co-bgwhite">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Phone Payment</h2>
						</div>
					</div>
	</div>
  
  						<div class="row" style="margin: 0px 10px 10px 10px;">
              <div class="col-md-4 col-md-offset-4">
							Your voucher has been temporarily recorded, please contact us on
							<?php foreach ($phone as $thePhones): ?>
							<a href="tel://<?php echo ($thePhones->detail);?>"><?php echo ($thePhones->detail);?></a>
							<?php endforeach; ?> to pay and receive your voucher.
								</div>
  </div>
</div>

