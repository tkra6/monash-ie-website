       
<?php

$urlPDF = array('controller' => 'Vouchers', 'action' => 'viewPdf');

?>
                
 <div id="page-inner">
  <div class="row">
    <div class="col-md-12">
     <h3><?= __('Vouchers') ?></h3>
    </div>
  </div>
  <hr/>


  <div class="row">
    <div class="col-md-12">
      <!-- Advanced Tables -->
      <div class="panel panel-default">
        <div class="panel-heading">
          List of Vouchers
        </div>

        <div class="panel-body">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#list" data-toggle="tab">List</a>
            </li>
            <li class=""><a <?php echo $this->Html->link('Add',['controller' => 'Vouchers', 'action' => 'add']); ?></a>
            </li>
            <li class=""><a href="/vouchers/archive" >Archives</a>
            </li>
          </ul>
          <br/>
          <div class="tab-content">
            <div class="tab-pane fade active in" id="list">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                  
                  <thead>
                    <tr>

                      <th>
                        <?php echo "From Name";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "From Email";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Phone Number";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "To Name";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Date Purchase";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Expiry";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Payment Status";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Voucher Status";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th>
                        <?php echo "Price";?>
                        <i class="fa fa-fw fa-sort"></i>
                      </th>
                      <th class="actions">
                        <?= __('Actions') ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                        $clickMe = "PDF";
                        $urlPDF = array('controller' => 'Vouchers', 'action' => 'viewPdf');
                        foreach ($vouchers as $voucher): ?>
                    <tr>
                        <td><?= h($voucher->fromName) ?></td>
                        <td><?= h($voucher->fromEmail) ?></td>
                        <td><?= h($voucher->phoneNum) ?></td>
                        <td><?= h($voucher->toName) ?></td>
                        <td><?= h($voucher->datePurchase) ?></td>
                        <td><?= h($voucher->expiry) ?></td>
                        <td><?php if(($voucher->paymentStatus) == 0) { echo "Unpaid"; } else if (($voucher->paymentStatus) == 1) { echo "Paypal Payment";} else if (($voucher->paymentStatus) == 2) {echo "Phone Payment";} else { echo "Cash"; } ?> </td>              
                        <td><?= $voucher->has('status') ? $this->Html->link($voucher->status->name, ['controller' => 'Status', 'action' => 'view', $voucher->status->id]) : '' ?></td>
                        <td><?= $voucher->has('price') ? $this->Html->link($voucher->price->value, ['controller' => 'Prices', 'action' => 'view', $voucher->price->id]) : '' ?></td>
                        <td class="actions">
                          
                          <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-edit']).' Edit',['controller' => 'Vouchers', 'action' => 'edit', $voucher->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>

                          <?php echo $this->Form->postLink('<i class="fa fa-pencil"></i> ' . __('Delete'), 
                          array('controller' => 'Vouchers', 'action' => 'delete',$voucher->id), 
                          array('class' => 'btn btn-danger', 'escape'=>false, 'confirm' =>('Are you sure you want to delete Voucher of Customer:{0}? '. $voucher->fromName)));
                          ?>
                          
                          <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-file']).' PDF',['controller' => 'Vouchers', 'action' => 'viewPdf', $voucher->id],
                                                 ['class' => 'btn btn-primary', 'role' => 'button' , 'escape' => false]);?>
                          
                          <?php echo $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-envelope']).' Email',['controller' => 'Vouchers', 'action' => 'email', $voucher->id],
                                                 ['class' => 'btn btn-danger', 'role' => 'button' , 'escape' => false]);?>


                        </td> 
                    </tr>
                    <?php endforeach; ?>
                  <?php echo $this->fetch('postLink');?>
                </tbody>

                </table>

              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
        </div>
</div>


    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery-1.10.2.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/jquery.dataTables.js') ?>
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/dataTables/dataTables.bootstrap.js') ?>

    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>
    <!-- CUSTOM SCRIPTS -->
    <?php echo $this->Html->script('/bs-binary-admin/assets/js/custom.js') ?>

                
