  <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800'); ?>
	<?php echo $this->Html->css('aanimate.css'); ?>
	<?php echo $this->Html->css('aicomoon.css'); ?>
	<?php echo $this->Html->css('abootstrap.css'); ?>
	<?php echo $this->Html->css('amagnific-popup.css'); ?>
	<?php echo $this->Html->css('aowl.carousel.min.css'); ?>
	<?php echo $this->Html->css('aowl.theme.default.min.css'); ?>
	<?php echo $this->Html->css('astyle.css'); ?>

  <?php echo $this->Html->script('amodernizr-2.6.2.min.js'); ?>

</head>
<?php										
  function limit_words($string, $word_limit){												
	$words = explode(" ",$string);
	return implode(" ",array_splice($words,0,$word_limit));
} ?>

<div id="fh5co-schedule" class="fh5co-bg" style="background-image: url(/img/graybg.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2>Pricing</h2>
						</div>
					</div>

					<div class="row animate-box">

						<div class="fh5co-tabs">
							<ul class="fh5co-tab-nav">
								<?php $lengthPrice = 0; ?>
								<?php foreach ($thePricetype as $thePricetypes): ?>
								<?php $lengthPrice = $lengthPrice + 1; ?>
								<?php endforeach; ?>
								<?php $priceSize = 100 / ($lengthPrice); ?>
								<?php $count =0; ?>
								<?php foreach ($thePricetype as $thePricetypes):  								
								$mobileHeading = $thePricetypes->name;
								$mobileHead = limit_words($mobileHeading, 1); ?>
								
								<?php if ($count == 0) { ?>
                <li class="active" style =" <?php echo "width:".$priceSize."%"; ?>" id = "<?php echo ($thePricetypes->id); ?>">
									<a href="#" data-tab="<?php echo ($thePricetypes->id); ?>"><span class="visible-xs"><?php echo $mobileHead; ?></span><span class="hidden-xs"><?php echo ($thePricetypes->name); ?></span></a></li>				
								
								<?php $count=$count+1; } else { ?> 
								<li style =" <?php echo "width:".$priceSize."%"; ?>" id = "<?php echo ($thePricetypes->id); ?>"><a href="#" data-tab="<?php echo ($thePricetypes->id); ?>"><span class="visible-xs"><?php echo $mobileHead; ?></span><span class="hidden-xs"><?php echo ($thePricetypes->name); ?></span></a></li>
								
								<?php $count=$count+1; } endforeach; ?>
							</ul>
							
							<?php $stack = array(); ?>
							<!-- Tabs -->
							<?php foreach ($thePrice as $thePrices): $type= $thePrices->pricetypes_id; array_push($stack, ($type)); $num=(array_unique($stack));?>
						  <?php endforeach; ?>

						<div class="fh5co-tab-content-wrap">
								<?php for ($counter = 0; $counter < sizeof($num); $counter++) { ?>
								<?php	$value = array_slice($num, $counter, 1); ?>
												
								<div id = "test<?php echo current($value); ?>" class="fh5co-tab-content tab-content <?php if ($counter == 0) { echo 'active';} ?>" data-tab-content="<?php echo current($value); ?>">
									<ul class="class-schedule">
										<?php foreach ($thePrice as $thePrices): if ($thePrices->pricetypes_id == (current($value))) { ?>
										<li class="text-center">
											<div class="price"><sup class="currency">$</sup><?php echo ($thePrices->value); ?><small></small></div>
											<h4><?php echo "<br>".($thePrices->minutes); ?></h4>
											<button type="button" class="btn btn-primary btn-outline btn-sm"><a href="<?php echo $this->url->build(array('controller' => 'vouchers', 'action' => 'details', $thePrices->id)) ?>">Purchase</a></button>
										</li>
										<?php } endforeach?>
									</ul>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

$(".fh5co-tab-nav li").click(function() {
    if($(this).parent().data("lastClicked")){
			var output = '#test' + ($(this).parent().data("lastClicked"));
        $(output).removeClass('active fadeIn');

    }
    $(this).parent().data("lastClicked", this.id);
});
	
</script>