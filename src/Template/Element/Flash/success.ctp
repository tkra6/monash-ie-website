<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success alert-dismissable fade in">
    <a class="close" style="z-index:99999999;" data-dismiss="alert" aria-label="close" onclick="this.classList.add('hidden')">&times;</a>
    <strong>Success!</strong> <?= $message ?>
  </div>
<!-- <div class="message success" onclick="this.classList.add('hidden')"><?php //echo $message ?></div>
 -->