<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="<?php //echo h($class) ?>" onclick="this.classList.add('hidden');"><?php //echo $message ?></div>
 -->
<div class="alert alert-info alert-dismissable fade in">
    <a class="close" style="z-index:99999999;" data-dismiss="alert" aria-label="close" onclick="this.classList.add('hidden')">&times;</a>
    <strong>Result!</strong> <?= $message ?>
  </div>