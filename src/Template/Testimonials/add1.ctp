<?php include_once("analyticstracking.php") ?> 
<?php $array_of_options = array('Yes', 'No');?>

<?php echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); ?>
	<div id="fh5co-testimonial" class="fh5co-bg-sectionwhite">
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Happy Clients</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 col-md-offset-1">
							<div class="row animate-box">
								<div class="owl-carousel owl-carousel-fullwidth">
									<?php foreach ($theTestimonial as $theTestimonials): ?>
									<div class="item">
										<div class="testimony-slide active text-center">
											<figure>
												<img <?php echo $this->Html->image('icon-newclient.png'); ?></img>
											</figure>
											<span><?php echo ($theTestimonials->name);?></span>
											<span><a class="twitter">Service: <?php echo $theTestimonials->has('service') ? ($theTestimonials->service->name) : '' ?></a></span>
											<blockquote>
												<p>&ldquo;
													<?php echo ($theTestimonials->content);?>&rdquo;</p>
												<p><a href="<?php echo $this->url->build(array('controller' => 'testimonials', 'action' => 'add1')) ?>" class="btn btn-primary btn-outline btn-sm">Add Testimonial <i class="icon-arrow-right"></i></a></p>
											</blockquote>
										</div>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-md-offset-1">
							          <p>Your feedback is important for us.</p>

          <?php echo $this->Form->create($testimonial) ?>
					
							<div class="form-group">
													<label for="validate-text">Name</label>
											    <div class="input-group"data-validate="name">
												   <?php echo $this->Form->control('name', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Your First Name', 'type' => 'text') , required); ?>
												     <span class="input-group-addon danger"><i class="fa fa-ban" style="color:#ff920a;"></i></span>
											</div>
										</div>
          							
							<div class="form-group">
													<label for="validate-text">Content</label>
											    <div class="input-group" data-validate="message">
												   <?php echo $this->Form->control('content', array('class' => 'form-control', 'id' => 'validate-text', 'label' => false, 'placeholder' => 'Write your testimonial', 'type' => 'textarea') , required); ?>
												     <span class="input-group-addon danger"><i class="fa fa-ban"style="color:#ff920a;"></i></span>
											</div>
										</div>
														
							
           <?php echo $this->Form->control('status', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Status', 'type' => 'hidden')); 
                      echo "<br/>"; ?>
							

							<div class="form-group">
													<label for="validate-text">Select Service</label>
											    <div class="input-group">
												   <?php echo $this->Form->select('services_id', $services, ['class' => 'form-control', 'id' => 'validate-select', 'label' => false], required); ?>

												     <span class="input-group-addon success"><i class="fa fa-check" style="color:#ff920a;"></i></span>
											</div>
										</div>
							
<!-- 							<div class="form-group">
            		<label for="validate-select">Validate Select</label>
					<div class="input-group">
                        <select class="form-control" name="validate-select" id="validate-select" placeholder="Validate Select" required>
                            <option value="">Select an item</option>
                            <option value="item_1">Item 1</option>
                            <option value="item_2">Item 2</option>
                            <option value="item_3">Item 3</option>
                        </select>
						<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
					</div>
				</div> -->
														
                      
            <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-default pull-right','div' => false, 'label' => false, 'type' => 'submit')); ?>
           <?php	 echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>

					
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<?php echo $this->Html->script('validationJava.js'); ?>