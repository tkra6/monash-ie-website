  <?php
/**
  * @var \App\View\AppView $this
  */
$status = array('Unpublished' => 'Unpublished', 'Published'=> 'Published');

?>
<div id="page-inner">
        <div class="row">
          <div class="col-md-12">
           <h2><?= __('Testimonials') ?></h2>
          </div>
        </div>
        <hr />
      <div class="form-group">
        <?= $this->Form->create($testimonial) ?>
           <h2>
             <legend><?= __('Add Testimonials') ?></legend>
            </h2> 
					
						<?php echo $this->Form->control('name', array('class'=>'form-control'));?> </br>
           <?php echo $this->Form->control('content', array('class'=>'form-control', 'type'=>'textarea'));?> </br>
           <?php echo $this->Form->control('status', array('class'=>'form-control', 'options' => $status , 'empty'=>'Choose'));?> </br>
					 <?php echo $this->Form->control('services_id', array('class'=>'form-control'  ,'options' => $services));?> </br>
 <?=  $this->Form->button('Submit', array('class' => 'btn btn-default')); ?>
</div>
</div>
    