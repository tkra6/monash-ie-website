<div id="page-inner">
          <div class="row">
          <div class="col-md-12">
           <h2><?= __('Testimonials') ?></h2>
          </div>
        </div>
        <hr />
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
         <?= h($testimonial->name) ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
             
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($testimonial->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Content') ?></th>
            <td><?= h($testimonial->content) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($testimonial->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Service') ?></th>
            <td><?= $testimonial->has('service') ? $this->Html->link($testimonial->service->name, ['controller' => 'Services', 'action' => 'view', $testimonial->service->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($testimonial->created) ?></td>
        </tr>
    </table>
</div>
