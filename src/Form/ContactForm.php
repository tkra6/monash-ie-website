<?php 
// in src/Form/ContactForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ContactForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('title', 'string')
            ->addField('firstName', ['type' => 'string'])
            ->addField('lastName', ['type' => 'string'])
            ->addField('email', ['type' => 'string'])
            ->addField('gender', ['type' => 'string'])
            ->addField('DOB', ['type' => 'date'])
            ->addField('extraInfo', ['type' => 'text'])
            ->addField('comment', ['type' => 'text'])
            ->addField('occupation', ['type' => 'string'])
            ->addField('emergency', ['type' => 'text'])
            ->addField('medicareNum', ['type' => 'integer'])
            ->addField('referenceNum', ['type' => 'integer'])
            ->addField('doctor', ['type' => 'text'])
            ->addField('note', ['type' => 'text'])
            ->addField('reminder', ['type' => 'string'])
            ->addField('smsMarketing', ['type' => 'string'])
            ->addField('confirmation', ['type' => 'string']);
    }


  
    protected function _buildValidator(Validator $validator)
    {
        return $validator->add('title', 'length', [
                          'rule' => ['minLength', 2],
                          'message' => 'A name is required'])
          
                          ->add('email', 'format', [
                          'rule' => 'email',
                          'message' => 'A valid email address is required',])
          
                          ->add('firstName', 'length', [
                          'rule' => ['minLength', 1],
                          'message' => 'A valid first name is required',])
      
                          ->add('lastName', 'length', [
                          'rule' => ['minLength', 1],
                          'message' => 'A valid last name is required',])
      
                          ->add('firstName', 'format', [
                          'rule' => ['ascii'],
                          'message' => 'No special characters allowed',]);
    }

    protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
  public function setErrors($errors)
{
    $this->_errors = $errors;
}
}
?>