<?php 
// in src/Form/LoginForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class LoginForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('email', ['type' => 'string'])
                  ->addField('phone', ['type' => 'integer']);
    }


  
    protected function _buildValidator(Validator $validator)
    {
        return $validator->add('email', 'format', [
                          'rule' => 'email',
                          'message' => 'A valid email address is required',])
          
                          ->add('phone', [
                            'minLength' => [
                                'rule' => ['minLength', 8],
                                'last' => true,
                                'message' => 'Phone must be between 8-10 digits'
                            ],
                            'maxLength' => [
                                'rule' => ['maxLength', 10],
                                'message' => 'Phone must be between 8-10 digits'
                            ]])
      
                            ->add('email', [
                            'maxLength' => [
                                'rule' => ['maxLength', 40],
                                'last' => true,
                                'message' => 'Email must be under 40 characters'
                            ]]);

    }

    protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
  public function setErrors($errors)
{
    $this->_errors = $errors;
}
}
?>