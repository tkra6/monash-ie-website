<?php 
// in src/Form/LoginForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class CancelForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('email', ['type' => 'string'])
                  ->addField('mobile', ['type' => 'integer']);
    }


  
    protected function _buildValidator(Validator $validator)
    {
        return $validator;
    }

    protected function _execute(array $data)
    {
        // Send an email.
        return true;
    }
  public function setErrors($errors)
{
    $this->_errors = $errors;
}
}
?>