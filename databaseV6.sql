-- --------------------------------------------------------
--
-- Table structure for table `pricetypes`
--

CREATE TABLE pricetypes (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `name` VARCHAR(50) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some pricetypes for testing: */
INSERT INTO pricetypes (id, name)
    VALUES (1, 'Remedial');

    