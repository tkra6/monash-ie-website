-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 01, 2017 at 12:51 AM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 7.1.3-3+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `team16`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `body` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `categories_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `created`, `modified`, `categories_id`, `users_id`) VALUES
(1, 'First post', '<p><iframe height=\"314\" src=\"//www.youtube.com/embed/fLVFrf3uD3w\" width=\"560\"></iframe></p>\r\n\r\n<p><strong>Hello</strong><strong><span style=\"color:#ff0000\"> Everyone! my name is lalala</span></strong></p>\r\n', '2017-05-16 03:11:20', '2017-08-21 11:40:15', 3, 6),
(2, 'second post', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/b1cc20ac4e7984b9878f4bb70295007c.jpg\" style=\"height:283px; width:200px\" /><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/005-instagram-logo.png\" style=\"height:24px; width:24px\" /></p>\r\n\r\n<p>Just an idea&nbsp;</p>\r\n', '2017-05-16 10:21:26', '2017-08-24 17:31:15', 4, 6);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rght`, `name`, `description`, `created`, `modified`) VALUES
(1, 7, 2, 11, 'Massages', 'Different type of Massage', '2017-05-16 06:15:58', '2017-08-09 09:03:49'),
(3, 5, 6, 7, 'Sports Massage', 'Sports massage is a deep vigorous massage designed to prepare your body to function optimally before an event or to relax & recover after a strenuous workout.', '2017-05-16 06:20:29', '2017-08-08 15:03:10'),
(4, 5, 4, 5, 'Remedial Massage', 'Remedial massage is the application of different techniques in the treatment of muscular pain and dysfunction that affect human movement.', '2017-05-16 06:21:40', '2017-08-08 14:25:58'),
(5, 1, 3, 10, 'Well-being', 'This category will talk about eating habits', '2017-08-08 04:22:42', '2017-08-08 13:47:12'),
(7, 0, 1, 12, 'helloo', 'heloooooo', '2017-08-08 15:39:40', '2017-08-08 15:42:54'),
(8, 5, 8, 9, 'aa', 'aa', '2017-08-31 16:19:05', '2017-08-31 16:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `DOB` date NOT NULL,
  `extraInfo` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `occupation` varchar(50) NOT NULL,
  `emergency` varchar(255) NOT NULL,
  `medicareNum` int(11) DEFAULT NULL,
  `referenceNum` int(2) DEFAULT NULL,
  `doctor` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `reminder` varchar(10) NOT NULL,
  `smsMarketing` varchar(25) NOT NULL,
  `confirmation` varchar(25) NOT NULL,
  `concessionType` varchar(25) NOT NULL,
  `additionalEmail` varchar(255) DEFAULT NULL,
  `invoiceInfo` varchar(255) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `postalCode` int(10) NOT NULL,
  `street` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `title`, `firstName`, `lastName`, `email`, `gender`, `DOB`, `extraInfo`, `comment`, `occupation`, `emergency`, `medicareNum`, `referenceNum`, `doctor`, `note`, `reminder`, `smsMarketing`, `confirmation`, `concessionType`, `additionalEmail`, `invoiceInfo`, `city`, `state`, `country`, `postalCode`, `street`) VALUES
(1, 'Miss', 'Adeline', 'Neo', 'adenehh@gmail.com', 'Female', '1995-07-11', 'something', 'easy to book', 'Student', 'Janet: 999999999999', NULL, NULL, 'DR peter', 'back problem', 'None', 'Unsubscribe', 'Subscribe', 'None', '', '', 'notting hill', 'victoria', 'australia', 3168, '68 kanooka grove'),
(2, 'Mr', 'Rhys', 'Matthew', 'rhys@gmail.com.', 'Male', '1996-01-01', 'something', 'easy to book', 'Student', 'Livia: 88888888888', 2147483647, 1, '', '', 'SMS', 'Subscribe', 'Subscribe', 'Student', '', '', 'notting hill', 'victoria', 'australia', 3168, '21 bingley avenue');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `phoneNum` varchar(10) NOT NULL,
  `clients_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `type`, `phoneNum`, `clients_id`) VALUES
(1, 'Mobile', '406999111', 1),
(2, 'Home', '40699222', 1),
(3, 'Home', '40699333', 2);

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `detail` varchar(300) NOT NULL,
  `image` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `type`, `detail`, `image`) VALUES
(1, 'Address', '135 Gardenvale Road, Gardenvale Victoria 3185', ''),
(2, 'Phone Number', '(03) 9530 6400', ''),
(3, 'Quote', 'I take a massage each week. This isn\'t an indulgence, it\'s an investment in your full creative expression/productivity/passion and sustained good health.', ''),
(4, 'Partner', 'Partner 1', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/p1%282%29.gif\" style=\"height:128px; width:168px\" /><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/p1.gif\" style=\"height:152px; width:200px\" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n'),
(5, 'Partner', 'Partner 2', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/partner-gardosteo.jpg\" style=\"height:93px; width:200px\" />&nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n'),
(6, 'Partner', 'Partner 3', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/partner-sandra.jpg\" style=\"height:162px; width:200px\" />&nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n'),
(7, 'Team', 'NICOLLETTE', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/M1.jpg\" style=\"height:333px; width:250px\" /></p>\r\n'),
(8, 'Team', 'Maya', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/M2.jpg\" style=\"height:323px; width:250px\" /></p>\r\n'),
(9, 'Team', 'ELISSA', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/M3.jpg\" style=\"height:292px; width:250px\" /></p>\r\n'),
(10, 'Team', 'MEGAN', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/M4.jpg\" style=\"height:291px; width:250px\" /></p>\r\n'),
(12, 'testing', 'test', '');

-- --------------------------------------------------------

--
-- Table structure for table `pagecontents`
--

CREATE TABLE `pagecontents` (
  `id` int(11) NOT NULL,
  `page` varchar(30) NOT NULL,
  `section` varchar(30) NOT NULL,
  `content` longtext NOT NULL,
  `block` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pagecontents`
--

INSERT INTO `pagecontents` (`id`, `page`, `section`, `content`, `block`) VALUES
(1, 'home', 'welcome', '<h3><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/Night-Time-Photography1.jpg\" style=\"height:633px; width:950px\" /></h3>\r\n\r\n<p>HELLO</p>\r\n', 1),
(2, 'home', 'sevices', '<p>Services<strong> <span style=\"color:#1abc9c\">content</span></strong></p>\r\n', 1),
(3, 'home', 'teams', '<p>team content</p>\r\n', 1),
(4, 'home', 'blog', '<p>blog content</p>\r\n', 1),
(5, 'home', 'testimonial', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/motivational-wallpaper-4%281%29.jpg\" style=\"height:113px; width:200px\" />testimonial content</p>\r\n', 0),
(6, 'testing', 'test', '<p><img alt=\"\" src=\"http://team16-aneo3300752.codeanyapp.com/webroot/js/kcfinder/upload/images/milky-way%281%29.jpg\" style=\"height:150px; width:300px\" /></p>\r\n\r\n<p>Hello everyone!&nbsp;</p>\r\n', 0),
(7, 'testing', 'testtest', '<p><strong>hello <span style=\"color:#c0392b\">world&nbsp;</span></strong></p>\r\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(11) NOT NULL,
  `minutes` varchar(255) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `pricetypes_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `minutes`, `value`, `pricetypes_id`) VALUES
(1, '30 Minutes', '71.50', 1),
(2, '45 Minutes', '88.00', 1),
(3, '60 Minutes', '104.50', 1),
(4, '75 Minutes', '126.50', 1),
(5, '25 Minutes', '50.00', 2),
(6, '45 Minutes', '76.50', 2),
(7, '45 Minutes', '50.50', 3),
(8, '10 Minutes', '10.10', 3),
(9, '180 Minutes', '250.00', 4),
(10, '80 Minutes', '80.40', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pricetypes`
--

CREATE TABLE `pricetypes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pricetypes`
--

INSERT INTO `pricetypes` (`id`, `name`) VALUES
(1, 'Remedial'),
(2, 'Pregnancy'),
(3, 'Myotherapy'),
(4, 'Packages'),
(5, 'Myotherapy');

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `clients_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `firstName`, `lastName`, `status`, `clients_id`) VALUES
(1, 'Rhys', 'Matthew', 'Relative', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`) VALUES
(1, 'Remedial Massage'),
(2, 'Pre and Postinatal Massage'),
(3, 'Sports  Massage'),
(4, 'Relaxation Massage'),
(5, 'Corporate Massage'),
(6, 'Event Massage'),
(7, 'Others Massage');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Redeemed'),
(2, 'Unredeemed'),
(3, 'Expired');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT 'Unpublished',
  `created` datetime DEFAULT NULL,
  `consent` tinyint(1) DEFAULT NULL,
  `services_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `content`, `status`, `created`, `consent`, `services_id`) VALUES
(1, 'Adeline a', 'Good place, Nice Environment', 'Unpublished', '2017-02-23 22:48:52', 1, 1),
(2, 'Thomas', 'Love this place', 'Unpublished', '2017-02-23 22:48:55', 0, 3),
(3, 'Rhys', 'Great job', 'Unpublished', '2017-02-23 22:49:52', 0, 4),
(4, 'Frank', 'Highly recommended', 'Unpublished', '2017-02-23 22:55:52', 0, 2),
(35, 'Nomi', 'Lets check this out.', 'Unpublished', '2017-07-28 05:29:40', 0, 1),
(54, 'Adeline Neo', 'So good!', 'Unpublished', '2017-08-07 15:16:39', 0, 1),
(55, 'ss', 'ss', 'Unpublished', '2017-08-22 02:28:53', 0, 2),
(56, 'ss', 'ss', 'Unpublished', '2017-08-22 02:29:03', 0, 1),
(57, 'ss', 'ss', 'Unpublished', '2017-08-22 02:29:37', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(80) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `created`, `modified`) VALUES
(6, 'admin', '$2y$10$zlgwDFK6Llx7q1QY4KQpCuhDEMQE.WI.tWA5Is3WYi6nPcmpR2StG', 'admin', 'megan', 'am@gm.com', '2017-04-26 15:49:22', '2017-08-22 03:26:05'),
(26, 'aneo3', '$2y$10$13uBjdTHF2Z435FxnSOSh.P4eC3aXRn0SZK9wmEhhlu8MC8BUgz0S', 'Adeline', 'neo', 'adenehh@gmail.com', '2017-08-06 16:27:31', '2017-08-06 16:27:31'),
(27, 'haha', '$2y$10$Lwnzbj6C2//0uo6MGm5BmehKGX/7dVzFmos32TaSgyh12iI7O5y/q', 'ade', 'neooo', 'adenehh@gmail.come', '2017-08-22 02:20:51', '2017-08-22 02:20:51'),
(28, 'helloworld11', '$2y$10$hs71wVWQPFJVzBl/3k7GMeehLAw8izTXs/CL9AUbqf.Q6y6zfECh2', 'aaaa', 'aaaaa', 'haha@gm.comq', '2017-08-22 03:13:55', '2017-08-22 03:30:34');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `fromName` varchar(255) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  `phoneNumber` varchar(10) NOT NULL,
  `toName` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `datePurchase` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry` date DEFAULT NULL,
  `paymentStatus` int(1) DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '2',
  `prices_id` int(11) NOT NULL,
  `UUID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `fromName`, `fromEmail`, `phoneNumber`, `toName`, `message`, `datePurchase`, `expiry`, `paymentStatus`, `status_id`, `prices_id`, `UUID`) VALUES
(8, 'adeline', 'adenehh@gmail.com', '0406960722', 'hahahah', 'a', '2017-05-17 14:45:00', '2018-05-17', 0, 2, 1, '53ec6a5d-1481-41a5-99f1-99fd4456251f'),
(30, 'asdasd', 'adenehh@gmail.com', '0406960733', 'sasa', 'saaas', '2017-08-09 12:35:00', '2018-08-29', 1, 2, 1, 'e9cc140d-f179-4ca0-9f00-2f08b3872976'),
(31, 'Adeline', 'adenehh@gmail.com', '0406960722', 'Janet', 'GOODDAY!', '2017-08-24 16:19:00', '2018-08-29', 1, 2, 1, 'c229a664-566c-494f-a0dc-ea36fb8025e1'),
(32, 'Ade neo', 'adenehh@gmail.com', '0406960755', 'Megan', 'Relax!', '2017-08-29 08:24:00', '2018-08-29', 0, 2, 1, '88843717-bdee-4de8-adde-2b1ef6b88d1d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_id` (`categories_id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clients_id` (`clients_id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagecontents`
--
ALTER TABLE `pagecontents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pricetypes_id` (`pricetypes_id`);

--
-- Indexes for table `pricetypes`
--
ALTER TABLE `pricetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clients_id` (`clients_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_id` (`services_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prices_id` (`prices_id`),
  ADD KEY `status_id` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pagecontents`
--
ALTER TABLE `pagecontents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pricetypes`
--
ALTER TABLE `pricetypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `articles_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `pricetypes_id` FOREIGN KEY (`pricetypes_id`) REFERENCES `pricetypes` (`id`);

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `relations_ibfk_1` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD CONSTRAINT `testimonials_ibfk_1` FOREIGN KEY (`services_id`) REFERENCES `services` (`id`);

--
-- Constraints for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD CONSTRAINT `vouchers_ibfk_1` FOREIGN KEY (`prices_id`) REFERENCES `prices` (`id`),
  ADD CONSTRAINT `vouchers_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
