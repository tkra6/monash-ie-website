SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
--
--
-- --------------------------------------------------------
--
-- Table structure for table `categories`
--

CREATE TABLE categories (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `parent_id` INT(11) NOT NULL, 
    `lft` INT(11) NOT NULL, 
    `rght` INT(11) NOT NULL, 
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `created` DATETIME DEFAULT NULL,
    `modified` DATETIME DEFAULT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some articles for testing: */
INSERT INTO categories (id, parent_id, lft, rght, name, description, created)
    VALUES (1, 1, 1, 1, 'food.', 'curry' ,NOW());
    
INSERT INTO categories (id, parent_id, lft, rght, name, description, created)
    VALUES (2, 1, 2, 2,'drinks', 'cola', NOW());
    
INSERT INTO categories(id, parent_id, lft,rght, name, description, created)
    VALUES (3, 2, 1, 1,'bakery', 'bread', NOW());
-- --------------------------------------------------------
--
-- Table structure for table `articles`
--

CREATE TABLE articles (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `title` VARCHAR(50) NOT NULL,
    `body` TEXT NOT NULL,
    `created` DATETIME DEFAULT NULL,
    `modified` DATETIME DEFAULT NULL,
    `categories_id` INT NOT NULL,
    `users_id` INT NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`categories_id`) REFERENCES categories(`id`),
    FOREIGN KEY(`users_id`) REFERENCES users(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some articles for testing: */
INSERT INTO articles (id, title, body, created, categories_id, users_id)
    VALUES (1, 'The title', 'This is the article body.', NOW(), 1, 6);
INSERT INTO articles (id, title, body, created, categories_id, users_id)
    VALUES (2, 'A title once again', 'And the article body follows.', NOW(), 2 ,2);
INSERT INTO articles (id, title, body, created, categories_id, users_id)
    VALUES (3, 'Title strikes back', 'This is really exciting! Not.', NOW(),3,3);
    
    
    
    