CREATE TABLE IF NOT EXISTS `archives`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `fromName` VARCHAR(255) NOT NULL,
    `fromEmail` VARCHAR(255) NOT NULL,
    `phoneNumber` VARCHAR(10) NOT NULL,
    `toName` VARCHAR(255) NOT NULL,
    `message` VARCHAR(255) NOT NULL,
    `datePurchase` DATETIME DEFAULT NULL,
    `emailSent` BIT DEFAULT 0,
    `paymentStatus` BIT DEFAULT 0,
    `status_id` INT NOT NULL,
    `prices_id` INT NOT NULL,
    `UUID` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`prices_id`) REFERENCES prices(`id`),
    FOREIGN KEY(`status_id`) REFERENCES status(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;