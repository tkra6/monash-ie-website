    CREATE TABLE contents (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `type` VARCHAR(50) NOT NULL, 
    `detail` VARCHAR(200) NOT NULL, 
    `image` VARCHAR(50) NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some contacts for testing: */
INSERT INTO contents (id, type, detail, image)
    VALUES (1, 'Address', '135 Gardenvale Road, Gardenvale Victoria 3185', '');
INSERT INTO contents (id, type, detail, image)
    VALUES (2, 'Phone Number', '(03) 9530 6400', '');                      
