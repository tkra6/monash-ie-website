<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VouchersFixture
 *
 */
class VouchersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'fromName' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'fromEmail' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'phoneNumber' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'toName' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'message' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'datePurchase' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'emailSent' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => 'b\'0\'', 'collate' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'paymentStatus' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => 'b\'0\'', 'collate' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'prices_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'UUID' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'prices_id' => ['type' => 'index', 'columns' => ['prices_id'], 'length' => []],
            'status_id' => ['type' => 'index', 'columns' => ['status_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'vouchers_ibfk_1' => ['type' => 'foreign', 'columns' => ['prices_id'], 'references' => ['prices', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'vouchers_ibfk_2' => ['type' => 'foreign', 'columns' => ['status_id'], 'references' => ['status', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'fromName' => 'Lorem ipsum dolor sit amet',
            'fromEmail' => 'Lorem ipsum dolor sit amet',
            'phoneNumber' => 1,
            'toName' => 'Lorem ipsum dolor sit amet',
            'message' => 'Lorem ipsum dolor sit amet',
            'datePurchase' => '2017-05-09 06:17:27',
            'emailSent' => 'Lorem ipsum dolor sit amet',
            'paymentStatus' => 'Lorem ipsum dolor sit amet',
            'status_id' => 1,
            'prices_id' => 1,
            'UUID' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
