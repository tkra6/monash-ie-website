<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'); ?>
<!-- Bootstrap core CSS <link href="css/bootstrap.css" rel='stylesheet' type='text/css'> -->
<?php echo $this->Html->css('pricing.css'); ?>
<?php echo $this->Html->script(array('jquery.min.js','bootstrap.min.js', 'stickUp.min.js', 'colorbox/jquery.colorbox-min.js', 'templatemo_script.js')); ?>




<div id="templatemo-portfolio">
	<div class="container">
			<div class="row">
				<div class="templatemo-line-header head_contact">
					<div class="text-center">
						<hr class="team_hr team_hr_left hr_gray" /><span class="txt_darkgrey">VOUCHERS</span>
						<hr class="team_hr team_hr_right hr_gray" />
					</div>
				</div>
		</div>
	</div>
		<div class="container">
			<div class="row">
				<div class="clearfix"></div>
				<div class="templatemo-gallery-category" style="font-size:30px; margin-top:5px;">
					<div class="text-center">
						<a href=".remedial">Remedial </a> | <a href=".pregnancy">Pregnancy Message</a> | <a href=".myotherapy">Myotherapy</a> | <a href=".packages">Packages</a>
					</div>
				</div>
				
				
				
				</div>
			<!-- /.row -->


			<div class="clearfix"></div>
			<div class="text-center">
			<ul class="templatemo-project-gallery">

				<!-- Each set of vouchers goes here -->
				<li class="gallery remedial">	
					<div class="templatemo-service-item">
						<div>
							<div class="col-sm-4">
							<span><img alt="icon" <?php echo $this->Html->image('leaf.png'); ?> <img/> </span>
							<span class="templatemo-service-item-header">Remedial Massage</span>
							</div>
						</div>
						<p>Remedial massage is the application of different techniques in the treatment of muscular pain and dysfunction that affect human movement. It is a form of treatment that is applied in the preventative, corrective and rehabilitative phases of treatment.
									Remedial Massage is concerned with the restoration and maintenance of the soft tissue structures of the body and can be used for relaxation purposes also</p>
					</div>				
					<div id="pricing-table" class="clear">
						<div class="col-sm-1">
						</div>
     				<?php foreach ($prices as $price): if ($price->pricetypes_id == 1) { ?>
							<div class="plan">
								<h3><?= h($price->minutes)?><span>$<?= h($price->value)?></span></h3>
								<a class="signup" <?= $this->Html->link(__('Purchase'), ['action' => 'details', $price->id]) ?></a>
							</div>
						<?php } endforeach; ?>
					</div>
				</li>
				
				<!-- Each set of vouchers goes here -->
				<li class="gallery pregnancy">	
					<div class="templatemo-service-item">
						<div>
							<div class="col-sm-4">
							<span><img alt="icon" <?php echo $this->Html->image('leaf.png'); ?> <img/> </span>
							<span class="templatemo-service-item-header">Pregnancy Massage</span>
							</div>
						</div>
						<p>Pregnancy messages consist of: <span class=""></span></p>
					</div>				
					<div id="pricing-table" class="clear">
						<div class="col-sm-1">
						</div>
     				<?php foreach ($prices as $price): if ($price->pricetypes_id == 2) { ?>
							<div class="plan">
								<h3><?= h($price->minutes)?><span>$<?= h($price->value)?></span></h3>
								<a class="signup" <?= $this->Html->link(__('Purchase'), ['action' => 'details', $price->id]) ?></a>
							</div>
						<?php } endforeach; ?>
					</div>
				</li>
				
				<!-- Each set of vouchers goes here -->
				<li class="gallery myotherapy">	
					<div class="templatemo-service-item">
						<div>
							<div class="col-sm-4">
							<span><img alt="icon" <?php echo $this->Html->image('leaf.png'); ?> <img/> </span>
							<span class="templatemo-service-item-header">Myotherapy Massage</span>
							</div>
						</div>
						<p>Myotherapy message is ...</p>
					</div>				
					<div id="pricing-table" class="clear">
						<div class="col-sm-1">
						</div>
     				<?php foreach ($prices as $price): if ($price->pricetypes_id == 3) { ?>
							<div class="plan">
								<h3><?= h($price->minutes)?><span>$<?= h($price->value)?></span></h3>
								<a class="signup" <?= $this->Html->link(__('Purchase'), ['action' => 'details', $price->id]) ?></a>
							</div>
						<?php } endforeach; ?>
					</div>
				</li>
				
				<!-- Each set of vouchers goes here -->
				<li class="gallery packages">	
					<div class="templatemo-service-item">
						<div>
							<div class="col-sm-4">
							<span><img alt="icon" <?php echo $this->Html->image('leaf.png'); ?> <img/> </span>
							<span class="templatemo-service-item-header">Packages</span>
							</div>
						</div>
						<p>Vouchers can be purchased as a package of various types of massages. The current assortment of massages are:</p>
					</div>				
					<div id="pricing-table" class="clear">
						<div class="col-sm-1">
						</div>
     				<?php foreach ($prices as $price): if ($price->pricetypes_id == 4) { ?>
							<div class="plan">
								<h3><?= h($price->minutes)?><span>$<?= h($price->value)?></span></h3>
								<a class="signup" <?= $this->Html->link(__('Purchase'), ['action' => 'details', $price->id]) ?></a>
							</div>
						<?php } endforeach; ?>
					</div>
				</li>
			
			
			</div>
			</ul>
							


			<div class="clearfix"></div>
			
		</div>
		<!-- /.container -->
	</div>
	<!-- /.templatemo-portfolio -->
