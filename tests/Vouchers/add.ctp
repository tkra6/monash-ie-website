<div id="page-inner">

      <div class="form-group">
        <?= $this->Form->create($voucher) ?>
          <fieldset>
           <h2>
             <legend><?= __('Add Voucher') ?></legend>
            </h2> 
            
          <?php  echo $this->Form->control('fromName', array('class'=>'form-control')); ?> </br>
          <?php  echo $this->Form->control('fromEmail', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('phoneNumber', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('toName', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('message', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('datePurchase', array('class'=>'form-control','empty' => true)); ?></br>
          <?php  echo $this->Form->control('emailSent', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('paymentStatus', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('voucherStatus_id', array('class'=>'form-control')); ?></br>
          <?php  echo $this->Form->control('status_id', array('class'=>'form-control','options' => $status)); ?></br>
          <?php  echo $this->Form->control('prices_id', array('class'=>'form-control','options' => $prices)); ?></br>
          <?php  echo $this->Form->control('UUID', array('class'=>'form-control')); ?></br>
        
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
