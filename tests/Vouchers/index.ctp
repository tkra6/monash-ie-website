<?php

$urlPDF = array('controller' => 'Vouchers', 'action' => 'viewPdf');

?>

      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
              <h2><?= __('Vouchers') ?></h2>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-md-12 col-md-12 col-xs-12">
<!--           <div class="vouchers index large-9 medium-8 columns content"> -->
            <div class="panel panel-default">
              <div class="panel-heading">
               <?= __('Vouchers Table') ?>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('fromName') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fromEmail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phoneNumber') ?></th>
                <th scope="col"><?= $this->Paginator->sort('toName') ?></th>
                <th scope="col"><?= $this->Paginator->sort('datePurchase') ?></th>
                <th scope="col"><?= $this->Paginator->sort('emailSent') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paymentStatus') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prices_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>

        <tbody>
            <?php 
                $clickMe = "PDF";
                $urlPDF = array('controller' => 'Vouchers', 'action' => 'viewPdf');
                foreach ($vouchers as $voucher): ?>
            <tr>
                <td><?= h($voucher->fromName) ?></td>
                <td><?= h($voucher->fromEmail) ?></td>
                <td><?= $this->Number->format($voucher->phoneNumber) ?></td>
                <td><?= h($voucher->toName) ?></td>
                <td><?= h($voucher->datePurchase) ?></td>
                <td><?= h($voucher->emailSent) ?></td>
                <td><?= h($voucher->paymentStatus) ?></td>
                <td><?= $voucher->has('status') ? $this->Html->link($voucher->status->name, ['controller' => 'Status', 'action' => 'view', $voucher->status->id]) : '' ?></td>
                <td><?= $voucher->has('price') ? $this->Html->link($voucher->price->id, ['controller' => 'Prices', 'action' => 'view', $voucher->price->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $voucher->id],['class' => 'btn btn-primary btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $voucher->id],['class' => 'btn btn-success btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $voucher->id],['class' => 'btn btn-danger btn-xs'], ['confirm' => __('Are you sure you want to delete # {0}?', $voucher->id)]) ?>
                    <?= $this->Html->link(__('PDF Button'), ['controller' => 'Vouchers', 'action' => 'viewPdf', $voucher->id],['class' => 'btn btn-warning btn-xs']) ?>
                    <?= $this->Html->link(__('Send Email'), ['controller' => 'Vouchers', 'action' => 'email', $voucher->id],['class' => 'btn btn-info btn-xs']) ?>


                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
