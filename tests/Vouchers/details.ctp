			      <div id="templatemo-testimonial">
            <div class="container">
                <div class="row">
                    <div class="templatemo-line-header" style="margin-top: 0px;">
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="txt_darkgrey">Voucher Detail</span>
                            <hr class="team_hr team_hr_right hr_gray"/>
                        </div>
                    </div>
          <div class="col-md-4 contact_right">
          <p>Please Fill out the form below with your information.</p>

          <?php echo $this->Form->create($voucher) ?>

          <?php
            echo $this->Form->control('fromName', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Your Name', 'type' => 'name'));
                      echo "<br/>";
            echo $this->Form->control('fromEmail', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Your Email', 'type' => 'email'));
                      echo "<br/>";
            echo $this->Form->control('phoneNumber', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Your Phone Number')); 
                      echo "<br/>";
						echo $this->Form->control('toName', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Recipient', 'type' => 'texts')); 
            					echo "<br/>";
						echo $this->Form->control('message', array('class' => 'form-control', 'div' => false, 'label' => false, 'placeholder' => 'Message', 'type' => 'textarea'));

                      echo "<br/>";
						
            echo $this->Form->button(__('Continue'), array('class' => 'btn btn-orange pull-right','div' => false, 'label' => false, 'type' => 'submit')); 
            echo $this->Form->end(); ?>
        </div>
