<?php

// create new PDF document
//tcpdf loaded via Composer from https://packagist.org/packages/tecnick.com/tcpdf
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true);

// set document header information. This appears at the top of each page of the PDF document
//$pdf->SetHeaderData('', '',"Muscleworks Massage Voucher", '');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 20));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', 10));
$pdf->setPrintHeader(false);







//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// add a page
$pdf->AddPage();

$pdf->Ln();

$pdf->SetFillColor(114, 155, 120);

$pdf->SetXY(0,50);

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set background image
$img_file = K_PATH_IMAGES.'Voucher.png';
$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();

$phoneNumber = '9530 6400';

$header = array($voucher['UUID'], $voucher['toName'], $voucher['fromName'],  $voucher['message'], $voucher['price']['value'], $voucher['id']);
  
$html = '
<table border="1" cellspacing="3" cellpadding="4">
    <tr>
        <th></th>
        <th align="centre"><b>voucherID: </b></th>
        <th align="left align">'.$header[0].'</th>
        <th></th>
    </tr>
    
     <tr>
        <th></th>
        <th align="centre"><b>To:  </b></th>
        <th align="left align">'.$header[1].'</th>
        <th></th>
    </tr>
    
     <tr>
        <th></th>
        <th align="centre"><b>Gifted From: </b></th>
        <th align="left align">'.$header[2].'</th>
        <th></th>
    </tr>

         <tr>
        <th></th>
        <th align="centre"><b>Message: </b></th>
        <th align="left align">'.$header[3].'</th>
        <th></th>
    </tr>
    
    <tr>
        <th></th>
        <th align="centre"><b>Value:  </b></th>
        <th align="left align">$'.$header[4].'</th>
        <th></th>
    </tr>
    
    ';


$pdf->writeHTML($html, true, false, false, false, '');

ob_clean();


$pdf->SetXY(0,150);

$html = '





<body>
<p align="center"> Thank you for using muscle works service </p>
<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0">


<tbody>
    <tr>
    <th></th>
    <th></th>
        <th align="center align" bgcolor=" #ff7800" ><b>phone:  </b></th>
        <th align="center align"bgcolor="#ff7800">'.$phoneNumber.'</th>
     <th></th>
     <th></th>
      </tr>
     <tr>
    <th></th>
    <th></th>
        <th align="right align" bgcolor=" #ff7800" ><b></b></th>
        <th align="center align"bgcolor="#ff7800"></th>
     <th></th>
     <th></th>
    </tr>
    <tr>
    <th></th>
    <th></th>
        <th align="right align" bgcolor=" #ff7800" ><b></b></th>
        <th align="center align"bgcolor="#ff7800"></th>
     
     <th></th><th></th>
     </tr>
   <tbody>
    </table>
    
   </body>
    ';


$pdf_name = "Voucher.$header[0]..pdf";
$pdf_name2= "Voucher.$header[0].";

$pdf->writeHTML($html, true, false, false, false, '');
ob_clean();

$pdf->Image('img/banner.jpg', 0, 0, 220, 40, 'JPG', '', 'T', false, 300, '', false, false, 1, false, false, false);
// $pdf->Image('img/VoucherBot.jpg', 30, 105, 150, 150, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);


//Close and output PDF document
// $filename = location_on_server."voucher.pdf";

// $fileatt = $pdf->Output($filename, 'F');

// $data = chunk_split( base64_encode(file_get_contents($filename)) );

$pdf->Output(__DIR__ . '/PDFVouchers/Voucher-'.$header[0].'.pdf', 'F');
ob_clean();
$pdf->Output("Voucher-'$header[0].'pdf", 'D');

//$pdf->Output('voucher_'.$voucher['id'].'.pdf', 'D');

//   $pdf = write(APP . 'webroot'. DS .'files' . DS . 'voucher_'.$voucher['id'].'.pdf');
//   $pdf = APP . 'webroot'. DS .'files' .  DS . 'voucher_'.$voucher['id'].'.pdf';
exit();
?>