
<div id="page-inner">
  <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">

      <div class="panel panel-default">
        <div class="panel-heading">
         <?= h($voucher->id) ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
        <tr>
            <th scope="row"><?= __('FromName') ?></th>
            <td><?= h($voucher->fromName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FromEmail') ?></th>
            <td><?= h($voucher->fromEmail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ToName') ?></th>
            <td><?= h($voucher->toName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Message') ?></th>
            <td><?= h($voucher->message) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('EmailSent') ?></th>
            <td><?= h($voucher->emailSent) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PaymentStatus') ?></th>
            <td><?= h($voucher->paymentStatus) ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('Status') ?></th>
            <td><?= $voucher->has('status') ? $this->Html->link($voucher->status->name, ['controller' => 'Status', 'action' => 'view', $voucher->status->id]) : '' ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('Price') ?></th>
            <td><?= $voucher->has('price') ? $this->Html->link($voucher->price->id, ['controller' => 'Prices', 'action' => 'view', $voucher->price->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('UUID') ?></th>
            <td><?= h($voucher->UUID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($voucher->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PhoneNumber') ?></th>
            <td><?= $this->Number->format($voucher->phoneNumber) ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('VoucherStatus Id') ?></th>
            <td><?= $this->Number->format($voucher->voucherStatus_id) ?></td>
        </tr>
        <tr>

            <th scope="row"><?= __('DatePurchase') ?></th>
            <td><?= h($voucher->datePurchase) ?></td>
        </tr>
    </table>
</div>
