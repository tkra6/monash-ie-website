<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RedirectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RedirectsTable Test Case
 */
class RedirectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RedirectsTable
     */
    public $Redirects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.redirects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Redirects') ? [] : ['className' => 'App\Model\Table\RedirectsTable'];
        $this->Redirects = TableRegistry::get('Redirects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Redirects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
