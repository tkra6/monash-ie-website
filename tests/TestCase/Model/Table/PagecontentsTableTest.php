<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PagecontentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PagecontentsTable Test Case
 */
class PagecontentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PagecontentsTable
     */
    public $Pagecontents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pagecontents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pagecontents') ? [] : ['className' => 'App\Model\Table\PagecontentsTable'];
        $this->Pagecontents = TableRegistry::get('Pagecontents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pagecontents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
