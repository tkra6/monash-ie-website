<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PagecontentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PagecontentTable Test Case
 */
class PagecontentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PagecontentTable
     */
    public $Pagecontent;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pagecontent'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pagecontent') ? [] : ['className' => 'App\Model\Table\PagecontentTable'];
        $this->Pagecontent = TableRegistry::get('Pagecontent', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pagecontent);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
