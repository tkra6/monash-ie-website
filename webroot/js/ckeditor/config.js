/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {

	//Change skin by downloading the content and config the name below
	config.extraPlugins = 'tableresize';
	config.extraPlugins = 'simplebutton';
// 	config.extraPlugins = 'image2';
	config.skin = 'icy_orange';
	//config.extraPlugins = 'uploadimage';

	// Define changes to default configuration here. F or example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.filebrowserBrowseUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = 'http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload.php?type=flash';
	
	

};

// CKEDITOR.editorConfig = function(config) {
// 	config.skin = 'icy_orange';
// // ...
//    config.filebrowserBrowseUrl = '/kcfinder/browse.php?opener=ckeditor&type=files'; http://team16-aneo3300752.codeanyapp.com/js/kcfinder/upload/images/browse.php?opener=ckeditor&type=files
//    config.filebrowserImageBrowseUrl = '/kcfinder/browse.php?opener=ckeditor&type=images';
//    config.filebrowserFlashBrowseUrl = '/kcfinder/browse.php?opener=ckeditor&type=flash';
//    config.filebrowserUploadUrl = '/kcfinder/upload.php?opener=ckeditor&type=files';
//    config.filebrowserImageUploadUrl = '/kcfinder/upload.php?opener=ckeditor&type=images';
//    config.filebrowserFlashUploadUrl = '/kcfinder/upload.php?opener=ckeditor&type=flash';
// // ...
// };