SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
--
--

-- --------------------------------------------------------
--
-- Table structure for table `voucher_prices`
--

CREATE TABLE IF NOT EXISTS `prices`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `minutes` VARCHAR(255) NOT NULL,
    `value` DECIMAL(10,2) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


--
-- Dumping data for table ``
--

INSERT INTO `prices` (`id`, `minutes`, `value`) VALUES
(NULL, '30 Minutes', '71.50');

INSERT INTO `prices` (`id`, `minutes`, `value`) VALUES
(NULL, '45 Minutes', '88.00');

INSERT INTO `prices` (`id`, `minutes`, `value`) VALUES
(NULL, '60 Minutes', '104.50');

INSERT INTO `prices` (`id`, `minutes`, `value`) VALUES
(NULL, '75 Minutes', '126.50');


-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Table structure for table `paymentStatus`
--
CREATE TABLE IF NOT EXISTS `status`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

INSERT INTO `status` (`id`, `name`) VALUES
(NULL, 'Redeemed');

INSERT INTO `status` (`id`, `name`) VALUES
(NULL, 'Unredeemed');

INSERT INTO `status` (`id`, `name`) VALUES
(NULL, 'Expired');




--
-- Table structure for table `vouchers`
--
CREATE TABLE IF NOT EXISTS `vouchers`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `fromName` VARCHAR(255) NOT NULL,
    `fromEmail` VARCHAR(255) NOT NULL,
    `phoneNumber` INT NOT NULL,
    `toName` VARCHAR(255) NOT NULL,
    `message` VARCHAR(255) NOT NULL,
    `datePurchase` DATETIME DEFAULT NULL,
    `emailSent` BIT DEFAULT 0,
    `paymentStatus` BIT DEFAULT 0,
    `status_id` INT NOT NULL,
    `prices_id` INT NOT NULL,
    `UUID` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`prices_id`) REFERENCES prices(`id`),
    FOREIGN KEY(`status_id`) REFERENCES status(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

--
-- Dumping data for table ``
--

INSERT INTO `vouchers` (`id`, `fromName`, `fromEmail`, `phoneNumber`, `toName`, `message`, `datePurchase`, `emailSent`, `paymentStatus`, `status_id`, `prices_id`, `UUID`) VALUES
(NULL, 'Adeline', 'ade@gm.com', 0406960777, 'megan', 'Enjoy your day', '2017-05-02 04:00:33', 0, 0, 2, 1, 'acswwq214356376UHGFDFASDa');

INSERT INTO `vouchers` (`id`, `fromName`, `fromEmail`, `phoneNumber`, `toName`, `message`, `datePurchase`, `emailSent`, `paymentStatus`, `status_id`, `prices_id`, `UUID`) VALUES
(NULL, 'Rhys', 'ry@gm.com', 0406960778, 'janel', 'relax yourself', '2017-05-02 04:00:39', 0, 0, 2, 3, 'sfcfdsfrgye5ftwev');

INSERT INTO `vouchers` (`id`, `fromName`, `fromEmail`, `phoneNumber`, `toName`, `message`, `datePurchase`, `emailSent`, `paymentStatus`, `status_id`, `prices_id`, `UUID`) VALUES
(NULL, 'Thomas', 'th@gm.com', 0406960779, 'livia', 'take a break', '2017-05-02 04:05:39', 0, 0, 2, 2, 'sf43hdsfghjgfdsadf');

INSERT INTO `vouchers` (`id`, `fromName`, `fromEmail`, `phoneNumber`, `toName`, `message`, `datePurchase`, `emailSent`, `paymentStatus`, `status_id`, `prices_id`, `UUID`) VALUES
(NULL, 'Frank', 'fr@gm.com', 0406960788, 'pod', 'good job', '2017-05-02 04:06:39', 0, 0, 2, 3, '67huyhbgvfer3');

-- --------------------------------------------------------
