--
--
--
-- --------------------------------------------------------
--
-- Table structure for table `address`
--

/* CREATE TABLE address (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `city` VARCHAR(50) NOT NULL, 
    `state` VARCHAR(50) NOT NULL, 
    `country` VARCHAR(50) NOT NULL, 
    `postalCode` int(10) NOT NULL,
    `street` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

/* Then insert some address for testing: */
--INSERT INTO address (id, city, state, country, postalCode, street)
--    VALUES (1, 'clayton', 'victoria', 'australia', 3168, '68 kanooka grove');
    
--INSERTINSERT INTO address (id, city, state, country, postalCode, street)
    --VALUES (2, 'notting hill', 'victoria', 'australia', 3168, '21 bingley avenue');
 --*/

--
--
--
-- --------------------------------------------------------
--
-- Table structure for table `clients`
--

CREATE TABLE clients (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `title` VARCHAR(25) NOT NULL, 
    `firstName` VARCHAR(50) NOT NULL, 
    `lastName` VARCHAR(50) NOT NULL, 
    `email` VARCHAR(255) NOT NULL,
    `gender` VARCHAR(20) NOT NULL,
    `DOB` DATE NOT NULL,
    `extraInfo` VARCHAR(255)  NULL,
    `comment` VARCHAR(255)  NULL,
    `occupation` VARCHAR(50) NOT NULL,
    `emergency` VARCHAR(255) NOT NULL,
    `medicareNum` int(10) NULL,
    `referenceNum` int(2) NULL,
    `doctor` VARCHAR(50) NULL,
    `note` VARCHAR(255) NULL,
    `reminder` VARCHAR(10) NOT NULL,
    `smsMarketing` VARCHAR(25) NOT NULL,
    `confirmation` VARCHAR(25) NOT NULL,
    `concessionType` VARCHAR(25) NOT NULL,
    `additionalEmail` VARCHAR(255) NULL,
    `invoiceInfo` VARCHAR(255) NULL,
    `city` VARCHAR(50) NOT NULL, 
    `state` VARCHAR(50) NOT NULL, 
    `country` VARCHAR(50) NOT NULL, 
    `postalCode` int(10) NOT NULL,
    `street` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some clients for testing: */
INSERT INTO clients (id, title, firstName, lastName, email, gender, DOB, extraInfo, comment, 
                    occupation, emergency, medicareNum, referenceNum, doctor, note, reminder, smsMarketing, confirmation, concessionType, additionalEmail, invoiceInfo,city, state, country, postalCode, street)
            VALUES (1, 'Miss', 'Adeline', 'Neo', 'adenehh@gmail.com.', 'Female' , '1995-07-11', 'something', 'easy to book', 
                    'Student' , 'Janet: 999999999999', 1234567891, 1, 'DR peter', 'back problem', 'SMS and Email' , 'Unsubscribe', 'Subscribe', 'Student', '', '', 'clayton', 'victoria', 'australia', 3168, '68 kanooka grove' );
    
INSERT INTO clients (id, title, firstName, lastName, email, gender, DOB, extraInfo, comment, 
                    occupation, emergency, medicareNum, referenceNum, doctor, note, reminder, smsMarketing, confirmation, concessionType, additionalEmail, invoiceInfo,city, state, country, postalCode, street)
            VALUES (2, 'Mr', 'Rhys', 'Matthew', 'rhys@gmail.com.', 'Male' , '1996-01-01', 'something', 'easy to book', 
                    'Student' , 'Livia: 88888888888', 9876543211, 1, '', '', 'SMS' , 'Subscribe', 'Subscribe' , 'Student', '', '' , 'notting hill', 'victoria', 'australia', 3168, '21 bingley avenue');
    
    
    
    
-- --------------------------------------------------------
--
-- Table structure for table `relations`
--

CREATE TABLE relations (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `firstName` VARCHAR(50) NOT NULL, 
    `lastName` VARCHAR(50) NOT NULL, 
    `status` VARCHAR(10) NOT NULL,
    `clients_id` INT NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`clients_id`) REFERENCES clients(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some relations for testing: */
INSERT INTO relations (id, firstName, lastName, status, clients_id)
    VALUES (1, 'Rhys', 'Matthew', 'Relative', 1);

                    
-- --------------------------------------------------------
--
-- Table structure for table `contacts`
--

CREATE TABLE contacts (
    `id` INT(11) NOT NULL AUTO_INCREMENT, 
    `type` VARCHAR(50) NOT NULL, 
    `phoneNum` int(10) NOT NULL, 
    `clients_id` INT NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`clients_id`) REFERENCES clients(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;


/* Then insert some contacts for testing: */
INSERT INTO contacts (id, type, phoneNum, clients_id)
    VALUES (1, 'Mobile', 0406999111, 1);
INSERT INTO contacts (id, type, phoneNum, clients_id)
    VALUES (2, 'Home', 040699222, 1);
INSERT INTO contacts (id, type, phoneNum, clients_id)
    VALUES (3, 'Home', 040699333, 2);                    
    
