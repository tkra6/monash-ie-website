SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

-- --------------------------------------------------------

INSERT INTO `services` (`id`, `name`) VALUES
(1,'Remedial Massage' ),
(2,'Pre & Postinatal Massage' ),
(3,'Sports  Massage' ),
(4,'Relaxation Massage' ),
(5,'Corporate Massage' ),
(6,'Event Massage' ),
(7,'Others Massage' );

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `content` VARCHAR(255) NOT NULL,
    `status` VARCHAR(255) NOT NULL,
    `created` DATETIME DEFAULT NULL,
    `consent` BIT DEFAULT 0,
    `services_id` INT NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`services_id`) REFERENCES services(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

-- --------------------------------------------------------


INSERT INTO `testimonials` (`id`, `name`, `content`, `status`, `created`, `consent`, `services_id`) VALUES
(1,'Adeline', 'Good place, Nice Environment', 'Unpublished' , '2017-02-23 22:48:52' , 0 , 1 ),
(2,'Thomas', 'Love this place', 'Unpublished' , '2017-02-23 22:48:55' , 0 , 1 ),
(3,'Rhys', 'Great job', 'Unpublished' , '2017-02-23 22:49:52' , 0 , 1 ),
(4,'Frank', 'Highly recommended', 'Unpublished' , '2017-02-23 22:55:52' , 0 , 1 );

